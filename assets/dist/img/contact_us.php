<div class="page-header"></div>
<div class="container-fluid d-flex justify-content-center vh-100 pt-5 mb-auto" id="Contact_Us">
<div class="container d-flex justify-content-center bg-secondary">
    <div class="row my-1 mx-auto">
        <div class="col-sm-6"> <img src="<?= base_url(); ?>assets/img/contact_us/bg_contact_us2.png" alt="IMG"> </div>
        <!--col-->
        <div class="col-md-6">
            <h2 class="form-title">Contact us</h2>
            <p class="justify text-muted">Have an enquiry or would like to give us feedback?<br>Fill out the form below to contact our team.</p>
            <form>
                <div class="form-group pt-2 pl-1"> <label for="exampleInputName">Your name</label> <input type="text" class="form-control" id="exampleInputName"> </div>
                <div class="form-group pl-1"> <label for="exampleInputEmail1">Your email address</label> <input type="email" class="form-control" id="exampleInputEmail1"> </div>
                <div class="form-group pl-1"> <label for="exampleFormControlTextarea1">Your message</label> <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea> </div>
                <div class="row">
                    <div class="col-lg-2 offset-md-auto"><button type="submit" class="btn btn-primary">Send</button></div>
                </div>
            </form>
        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
</div>