-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Nov 2021 pada 11.19
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wearhadid`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `IDAdmin` varchar(10) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Username` varchar(25) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Address` text NOT NULL,
  `Phone` varchar(12) NOT NULL,
  `Gender` char(1) NOT NULL,
  `Email` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`IDAdmin`, `Name`, `Username`, `Password`, `Address`, `Phone`, `Gender`, `Email`) VALUES
('A001', 'Satrio Ananda S', 'satrio14', '405e8a9472142c344e434f0db9e0061e', 'Komp. Nusa Indah No.2', '085797335768', 'L', 'satriosetiawan99@gmail.co'),
('A002', 'Nursilva', 'Nursilva22', 'd41d8cd98f00b204e9800998ecf8427e', 'Batujajar No.5', '085797335768', 'P', 'pockyid48@gmail.com'),
('A003', 'Erik Fadli', 'Erik123478', 'd41d8cd98f00b204e9800998ecf8427e', 'Komp. Nusa Hijau no 1', '082129878720', 'L', 'satrioananda807@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `IDCart` int(8) NOT NULL,
  `IDProduct` varchar(10) NOT NULL,
  `IDMember` varchar(10) NOT NULL,
  `Qty` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`IDCart`, `IDProduct`, `IDMember`, `Qty`) VALUES
(78, 'P010', 'M001', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `IDCategory` varchar(15) NOT NULL,
  `Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`IDCategory`, `Name`) VALUES
('C001', 'Kaos'),
('C002', 'Kemeja'),
('C003', 'Sarung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_product`
--

CREATE TABLE `list_product` (
  `No` int(11) NOT NULL,
  `IDReseller` varchar(25) NOT NULL,
  `IRProduct` varchar(10) NOT NULL,
  `ProductName` varchar(25) NOT NULL,
  `IRCategory` varchar(25) NOT NULL,
  `Stock` int(6) NOT NULL,
  `Price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `list_product`
--

INSERT INTO `list_product` (`No`, `IDReseller`, `IRProduct`, `ProductName`, `IRCategory`, `Stock`, `Price`) VALUES
(10, 'R002', 'P002', 'Basic Washed Tee Blue', 'C002', 32, 200000),
(42, 'R003', 'P003', 'Basic Washed Tee Green', 'C001', 10, 150000),
(43, 'R003', 'P001', 'Basic Plain Tee Black', 'C001', 10, 150000),
(44, 'R003', 'P004', 'Basic Washed Tee Red', 'C001', 10, 150002),
(65, 'R001', 'P002', 'Basic Plain Tee Green (M)', 'C001', 34, 149000),
(66, 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'C001', 4, 149000),
(67, 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'C001', 10, 150000),
(68, 'R001', 'P001', 'Basic Plain Tee Green (S)', 'C001', 12, 151000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `IDMember` varchar(10) NOT NULL,
  `Username` varchar(15) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Gender` enum('L','P') NOT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `Address` text DEFAULT NULL,
  `Postal_Code` int(5) DEFAULT NULL,
  `Detail_Address` text DEFAULT NULL,
  `Email` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`IDMember`, `Username`, `Password`, `Name`, `Gender`, `Phone`, `Address`, `Postal_Code`, `Detail_Address`, `Email`) VALUES
('M001', 'satrio08', '405e8a9472142c344e434f0db9e0061e', 'Satrio Ananda S', 'L', '081395085146', 'Komplek nusa hijau permai blok r12a', 40512, NULL, 'pockyid48@gmail.com'),
('M002', 'Sarah_19', '398397d56b091c9e676400346180827f', 'Sarah Dwi', 'L', '082217212174', 'Jalan Cipageran no.5', 40521, 'Rumah Putih, Pager Hitam', 'sarahanika97@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification`
--

CREATE TABLE `notification` (
  `No` int(11) NOT NULL,
  `ID` varchar(25) NOT NULL,
  `Value_Notification_Order` longtext NOT NULL,
  `Status` varchar(10) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `notification`
--

INSERT INTO `notification` (`No`, `ID`, `Value_Notification_Order`, `Status`, `Date`) VALUES
(1, 'TM001202110251', 'Terdapat Pesanan Baru Dengan ID TM001202110251 Mohon Periksa Pesanan Tersebut', 'seen', '2021-10-25'),
(12, 'TM001202110258', 'Terdapat Pesanan Baru Dengan ID TM001202110258 Mohon Periksa Pesanan Tersebut', 'seen', '2021-10-25'),
(13, 'TM001202110261', 'Terdapat Pesanan Baru Dengan ID TM001202110261 Mohon Periksa Pesanan Tersebut', 'seen', '2021-10-26'),
(14, 'TM001202110281', 'Terdapat Pesanan Baru Dengan ID TM001202110281 Mohon Periksa Pesanan Tersebut', 'seen', '2021-10-28'),
(15, 'R003', 'Terdapat Pendaftar Reseller Baru Dengan ID R003 Mohon Periksa Account Tersebut', 'seen', '2021-10-29'),
(20, 'R003', 'Terdapat Pendaftar Reseller Baru Dengan ID R003 Mohon Periksa Account Tersebut', 'seen', '2021-10-31'),
(21, 'R004', 'Terdapat Pendaftar Reseller Baru Dengan ID R004 Mohon Periksa Account Tersebut', 'seen', '2021-10-31'),
(22, 'R005', 'Terdapat Pendaftar Reseller Baru Dengan ID R005 Mohon Periksa Account Tersebut', 'seen', '2021-10-31'),
(25, 'TM002202111011', 'Terdapat Pesanan Baru Dengan ID TM002202111011 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-01'),
(28, 'TM001202111012', 'Terdapat Pesanan Baru Dengan ID TM001202111012 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-01'),
(51, 'TM003202111011', 'Terdapat Pesanan Baru Dengan ID TM003202111011 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-01'),
(52, 'R006', 'Terdapat Pendaftar Reseller Baru Dengan ID R006 Mohon Periksa Account Tersebut', 'seen', '2021-11-02'),
(53, 'TM001202111021', 'Terdapat Pesanan Baru Dengan ID TM001202111021 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-02'),
(54, 'TM001202111041', 'Terdapat Pesanan Baru Dengan ID TM001202111041 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-04'),
(55, 'TM001202111042', 'Terdapat Pesanan Baru Dengan ID TM001202111042 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-04'),
(56, 'TM001202111171', 'Terdapat Pesanan Baru Dengan ID TM001202111171 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-17'),
(57, 'TM001202111172', 'Terdapat Pesanan Baru Dengan ID TM001202111172 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-17'),
(58, 'TM001202111173', 'Terdapat Pesanan Baru Dengan ID TM001202111173 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-17'),
(59, 'TM001202111174', 'Terdapat Pesanan Baru Dengan ID TM001202111174 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-17'),
(60, 'R006', 'Terdapat Pendaftar Reseller Baru Dengan ID R006 Mohon Periksa Account Tersebut', 'seen', '2021-11-17'),
(61, 'TM001202111175', 'Terdapat Pesanan Baru Dengan ID TM001202111175 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-17'),
(62, 'TM001202111176', 'Terdapat Pesanan Baru Dengan ID TM001202111176 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-17'),
(63, 'TM001202111181', 'Terdapat Pesanan Baru Dengan ID TM001202111181 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-18'),
(64, 'TM001202111182', 'Terdapat Pesanan Baru Dengan ID TM001202111182 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-18'),
(65, 'TM001202111241', 'Terdapat Pesanan Baru Dengan ID TM001202111241 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-24'),
(66, 'TM001202111242', 'Terdapat Pesanan Baru Dengan ID TM001202111242 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-24'),
(67, 'TM001202111242', 'Terdapat Pesanan Baru Dengan ID TM001202111242 Mohon Periksa Pesanan Tersebut', 'seen', '2021-11-24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order`
--

CREATE TABLE `order` (
  `No` int(10) NOT NULL,
  `IDOrder` varchar(15) NOT NULL,
  `IDProduct` varchar(10) NOT NULL,
  `IDMember` varchar(10) NOT NULL,
  `Province` text NOT NULL,
  `City` text NOT NULL,
  `Districts` text NOT NULL,
  `Address` text NOT NULL,
  `Detail_Address` text NOT NULL,
  `Postal_Code` varchar(6) NOT NULL,
  `Qty` int(2) NOT NULL,
  `Shipping_Cost` int(6) NOT NULL,
  `Total_Price` int(7) NOT NULL,
  `Status` varchar(10) NOT NULL,
  `Date` date NOT NULL,
  `Expedition` varchar(25) NOT NULL,
  `ImageSource` varchar(25) NOT NULL,
  `Ulasan` text NOT NULL,
  `ImageUlasan` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order`
--

INSERT INTO `order` (`No`, `IDOrder`, `IDProduct`, `IDMember`, `Province`, `City`, `Districts`, `Address`, `Detail_Address`, `Postal_Code`, `Qty`, `Shipping_Cost`, `Total_Price`, `Status`, `Date`, `Expedition`, `ImageSource`, `Ulasan`, `ImageUlasan`) VALUES
(49, 'TM0012021110113', 'P002', 'M001', 'Jawa Timur', 'Malang', 'Majalengka', 'Komplek nusa hijau permai blok r12a', 'Rumah Kuning-Orange, Pagar merah, Sebelah Warung Biasa', '40512', 1, 0, 150200, 'Order', '2021-11-01', '', '', '', ''),
(50, 'TM0012021110114', 'P002', 'M001', 'Jawa Barat', 'Bandung', 'Pajajaran', 'Komplek nusa hijau permai blok r12a', 'Rumah Kuning-Orange, Pagar merah, Sebelah Warung Biasa', '40512', 3, 0, 450600, 'Order', '2021-11-01', '', '', '', ''),
(51, 'TM0012021110114', 'P001', 'M001', 'Jawa Barat', 'Bandung', 'Pajajaran', 'Komplek nusa hijau permai blok r12a', 'Rumah Kuning-Orange, Pagar merah, Sebelah Warung Biasa', '40512', 2, 0, 300000, 'Order', '2021-11-01', '', '', '', ''),
(52, 'TM0012021110115', 'P003', 'M001', 'Jakarta', 'Jakarta', 'Jatinegara', 'Komplek nusa hijau permai blok r12a', 'Rumah Kuning-Orange, Pagar merah, Sebelah Warung Biasa', '40512', 1, 0, 120000, 'Order', '2021-11-01', '', '', '', '0'),
(77, 'TM001202111182', 'P009', 'M001', 'Jakarta', 'Jakarta', 'Jatinegara', 'Komplek nusa hijau permai blok r12a', 'Blok R, RT.06 RW.12', '40512', 2, 0, 298000, 'Deliver', '2021-11-18', 'JNE -  3414134214321', 'Tanti_(38).jpeg', '', '0'),
(79, 'TM001202111241', 'P009', 'M001', 'Jawab Barat', 'Kota Cimahi', 'Cimahi Utara', 'Komplek nusa hijau permai blok r12a', 'Blok R', '40512', 2, 20000, 298000, 'Confir', '2021-11-24', '', '', '', '0'),
(80, 'TM001202111241', 'P008', 'M001', 'Jawab Barat', 'Kota Cimahi', 'Cimahi Utara', 'Komplek nusa hijau permai blok r12a', 'Blok R', '40512', 1, 20000, 149000, 'Confir', '2021-11-24', '', '', '', '0'),
(81, 'TM001202111241', 'P007', 'M001', 'Jawab Barat', 'Kota Cimahi', 'Cimahi Utara', 'Komplek nusa hijau permai blok r12a', 'Blok R', '40512', 1, 20000, 149000, 'Confir', '2021-11-24', '', '', '', '0'),
(83, 'TM001202111242', 'P013', 'M001', 'Jawa Barat', 'Kota Bekasi', 'Bekasi Timur', 'Komplek nusa hijau permai blok r12a', 'Blok R, RT.04, RW.12', '40512', 1, 12000, 149000, 'Deliver', '2021-11-24', 'JNE - 12471280572108', 'Mohammad_isnan_(62).jpeg', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `IDProduct` varchar(10) NOT NULL,
  `IDAdmin` varchar(10) NOT NULL,
  `ProductName` varchar(25) NOT NULL,
  `IDCategory` varchar(15) NOT NULL,
  `Description` longtext NOT NULL,
  `ItemStock` int(5) NOT NULL,
  `ImageSource` varchar(50) DEFAULT NULL,
  `Price` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`IDProduct`, `IDAdmin`, `ProductName`, `IDCategory`, `Description`, `ItemStock`, `ImageSource`, `Price`) VALUES
('P001', 'A001', 'Basic Plain Tee Green (S)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cmSize details (Chest Width x Shirt Length) : S : 52 x 78 cm ', 90, 'A1.jpeg', 149000),
('P002', 'A001', 'Basic Plain Tee Green (M)', 'C001', 'Photos and real color similarity : 90%Real size may be vary, approxiamately between1-1.5cmSize details (Chest Width x Shirt Length): M : 54 x 80 cm ', 495, 'A21.jpeg', 149000),
('P003', 'A001', 'Basic Plain Tee Green (L)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): L : 56 x 82 cm ', 492, 'A3.jpg', 120000),
('P004', 'A001', 'Basic Plain Tee Green(XL)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): XL : 58 x 82 cm', 499, 'A4.jpg', 149000),
('P005', 'A001', 'Basic Washed Tee Blue (S)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length):   S : 52 x 78 cm ', 499, 'B1.jpg', 149000),
('P006', 'A001', 'Basic Washed Tee Blue (M)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): M : 54 x 80 cm ', 498, 'B21.jpg', 149000),
('P007', 'A001', 'Basic Washed Tee Blue (L)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length):  L : 56 x 82 cm ', 249, 'B3.jpg', 149000),
('P008', 'A001', 'Basic Washed Tee Blue(XL)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): XL : 58 x 82 cm', 100, 'B41.jpg', 149000),
('P009', 'A001', 'Basic Plain Tee Black (S)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length):  S : 52 x 78 cm ', 293, 'C1.jpg', 149000),
('P010', 'A001', 'Basic Plain Tee Black (M)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): M : 54 x 80 cm ', 250, 'C2.jpg', 149000),
('P011', 'A001', 'Basic Plain Tee Black (L)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): L : 56 x 82 cm', 428, 'C3.jpg', 149000),
('P012', 'A001', 'Basic Plain Tee Black XL', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): XL : 58 x 82 cm', 317, 'C4.jpg', 149000),
('P013', 'A001', 'Plain Tee Misty Grey (S)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): S : 52 x 78 cm', 319, 'D1.jpg', 149000),
('P014', 'A001', 'Plain Tee Misty Grey (M)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): M : 54 x 80 cm', 650, 'D2.jpg', 149000),
('P015', 'A001', 'Plain Tee Misty Grey (L)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): L : 56 x 82 cm ', 520, 'D3.jpg', 149000),
('P016', 'A001', 'Plain Tee Misty Grey (XL)', 'C001', 'Photos and real color similarity : 90% Real size may be vary, approxiamately between1-1.5cm Size details (Chest Width x Shirt Length): XL : 58 x 82 cm', 575, 'D4.jpg', 149000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reseller`
--

CREATE TABLE `reseller` (
  `IDReseller` varchar(10) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Username` varchar(25) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Address` text NOT NULL,
  `Phone` varchar(12) NOT NULL,
  `Gender` varchar(1) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reseller`
--

INSERT INTO `reseller` (`IDReseller`, `Name`, `Username`, `Password`, `Address`, `Phone`, `Gender`, `Email`, `Status`) VALUES
('R001', 'Satrio Ananda S', 'Satrio27', '398397d56b091c9e676400346180827f', 'Komplek nusa hijau permai blok r12a', '081395085146', 'L', 'satriosetiawan99@gmail.co', 'Aktif'),
('R002', 'Nursilva', 'Nursilva21', '398397d56b091c9e676400346180827f', 'Komplek nusa hijau permai blok r12a', '085797335768', 'L', 'NurSilva00@gmail.com', 'Aktif'),
('R004', 'Satrio ananda setiawan', 'Farhan123', '854862761135e326e92cfb0ff977d090', 'Komplek nusa hijau permai blok r12a', '085797335768', 'L', 'satrio.ananda807@gmail.com', 'Non Aktif'),
('R005', 'Nursilva Aulianisa Putri', 'Silva2204', 'e337d72bf329c6de43a58fe7a001f6e8', 'Pangauban RT.01 RW.05 No.035 Kodepos : 40761', '089663651710', 'P', 'nursilvaaulia@gmail.com', 'Aktif'),
('R006', 'Satrio Ananda S', 'Satrio45', '405e8a9472142c344e434f0db9e0061e', 'Komp. Nusa Hijau Permai Blok R 12 ', '085797335768', 'L', 'satrioananda807@gmail.com', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE `slider` (
  `No` int(11) NOT NULL,
  `ImageSource` varchar(50) NOT NULL,
  `Status` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slider`
--

INSERT INTO `slider` (`No`, `ImageSource`, `Status`) VALUES
(2, 'S2.png', 'Lainnya'),
(7, 'S52.jpg', 'Utama');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_reseller`
--

CREATE TABLE `transaction_reseller` (
  `ID` int(10) NOT NULL,
  `IDTransaction` varchar(15) NOT NULL,
  `IDReseller` varchar(25) NOT NULL,
  `IDProduct` varchar(25) NOT NULL,
  `ProductName` varchar(30) NOT NULL,
  `NameBuyer` varchar(25) NOT NULL,
  `Expedition` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `Price` int(10) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Total_Price` int(20) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaction_reseller`
--

INSERT INTO `transaction_reseller` (`ID`, `IDTransaction`, `IDReseller`, `IDProduct`, `ProductName`, `NameBuyer`, `Expedition`, `Address`, `Price`, `Quantity`, `Total_Price`, `Date`) VALUES
(137, 'TR001202110171', 'R001', 'P003', 'Basic Washed Tee Green', 'Farhan Renaldy Purnomo', 'JNE', 'dahlah', 200000, 3, 600000, '2021-10-17'),
(138, 'TR001202110172', 'R001', 'P003', 'Basic Washed Tee Green', 'Farhan Renaldy Purnomo', 'POS', 'kurang jauh', 200000, 2, 400000, '2021-10-17'),
(139, 'TR001202110173', 'R001', 'P003', 'Basic Washed Tee Green', 'Nur Silva', 'POS', 'hiya', 200000, 1, 200000, '2021-10-17'),
(148, 'TR002202110201', 'R002', 'P002', 'Basic Washed Tee Blue', 'Farhan Renaldy Purnomo', 'POS', 'Jalan Kopo no 3', 200000, 10, 2000000, '2021-10-20'),
(149, 'TR002202110202', 'R002', 'P002', 'Basic Washed Tee Blue', 'Eprian Junan', 'POS', 'hiya', 200000, 5, 1000000, '2021-10-20'),
(150, 'TR002202109081', 'R002', 'P002', 'Basic Washed Tee Blue', 'Satrio Ananda S', 'JNE', 'Jalan Kopo no 3', 200000, 2, 400000, '2021-09-08'),
(152, 'TR002202109091', 'R002', 'P002', 'Basic Washed Tee Blue', 'Nur Silva', 'POS', 'Nusa Kambangan', 200000, 4, 800000, '2021-09-09'),
(154, 'TR002202109101', 'R002', 'P002', 'Basic Washed Tee Blue', 'Nur Silva', 'POS', 'kurang jauh', 200000, 1, 200000, '2021-09-10'),
(155, 'TR002202109111', 'R002', 'P002', 'Basic Washed Tee Blue', 'Eprian Junan', 'POS', 'dahlah', 100000, 1, 100000, '2021-09-11'),
(159, 'TR001202110281', 'R001', 'P002', 'Basic Washed Tee Blue', 'Ananda', 'JnT -  100', 'Kurang jauh', 201000, 1, 201000, '2021-10-28'),
(176, 'TR001202110282', 'R001', 'P003', 'Basic Washed Tee Grey', 'Dani', 'JnT -  100', 'Kurang jauh', 200000, 2, 400000, '2021-10-28'),
(184, 'TR001202110312', 'R001', 'P001', 'Basic Plain Tee Black', 'Ahmad R', 'JNE ', 'Jalan Kopo Asri No 4 B', 160000, 1, 160000, '2021-10-31'),
(185, 'TR001202110283', 'R001', 'P002', 'Basic Washed Tee Blue', 'Satrio Ananda S', ' J&T - 100', 'Jalan Pasospati No 5-45', 150000, 4, 600000, '2021-10-28'),
(188, 'TR001202111042', 'R001', 'P002', 'Basic Washed Tee Blue', 'Ananda Setiawan', 'J&T â€“ 1000', 'Jalan Terusan No.56', 100000, 1, 100000, '2021-11-04'),
(190, 'TR001202111043', 'R001', 'P002', 'Basic Washed Tee Blue', 'Ananda Setiawan', 'J&T â€“ 1000', 'Jalan Terusan No 4', 150000, 4, 600000, '2021-11-04'),
(191, 'TR001202111043', 'R001', 'P003', 'Basic Washed Tee Grey', 'Ananda Setiawan', 'J&T â€“ 1000', 'Jalan Terusan No 4', 150000, 1, 150000, '2021-11-04'),
(192, 'TR001202111131', 'R001', 'P003', 'Basic Washed Tee Grey', 'Satrio Ananda ', 'J&T - 09808490180951', 'Jauh', 150000, 2, 300000, '2021-11-13'),
(193, 'TR001202111021', 'R001', 'P003', 'Basic Washed Tee Grey', 'Indrawan', 'J&T - 341121312412', 'Jauh Banget', 150000, 3, 450000, '2021-11-02'),
(194, 'TR001202111171', 'R001', 'P003', 'Basic Washed Tee Grey', 'Endah Cipti', 'JNE - 3414151351312', 'Jalan Pasopati No.5 - 45', 150000, 4, 600000, '2021-11-17'),
(195, 'TR001202101131', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Yuliah', 'JNE - 120040042107018', 'Jl. Kebantenan Indah No.21, Bekasi Timur, 40512', 149000, 4, 596000, '2021-01-13'),
(196, 'TR001202101211', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Ananda Purnomo', 'JNE - 120040041207114', 'Jl. Pondok Indah No. 4, Jakarta Utara', 149000, 2, 298000, '2021-01-21'),
(197, 'TR001202101311', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Aditya Rahmadi', 'JnT -  10002585403621', 'Komp. Kota Mas No 213, Cimahi Tengah', 149000, 3, 447000, '2021-01-31'),
(198, 'TR001202102141', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Dani Udin Setiawan', 'JnT -  10002585403123', 'Jl. Gatot Subroto No.41, Cimahi Tengah', 149000, 2, 298000, '2021-02-14'),
(199, 'TR001202102281', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Juniah ', 'JNE - 120040042107111', 'Komp. Nusa Sari No.17, Cimahi Utara', 149000, 1, 149000, '2021-02-28'),
(200, 'TR001202102281', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Juniah ', 'JNE - 120040042107111', 'Komp. Nusa Sari No.17, Cimahi Utara', 150000, 1, 150000, '2021-02-28'),
(201, 'TR001202102281', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Juniah ', 'JNE - 120040042107111', 'Komp. Nusa Sari No.17, Cimahi Utara', 149000, 2, 298000, '2021-02-28'),
(202, 'TR00120210341', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Ananda Wirjawan', 'JNE - 120040042107012', 'Jl. Encep katawirja No.46, Cimahi Utara', 149000, 3, 447000, '2021-03-04'),
(203, 'TR00120210341', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Ananda Wirjawan', 'JNE - 120040042107012', 'Jl. Encep katawirja No.46, Cimahi Utara', 150000, 2, 300000, '2021-03-04'),
(204, 'TR001202103171', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Anugrah Raditya ', 'JNE - 120040042101342', 'Jl. Melong Raya No.141, Cimahi Selatan', 149000, 1, 149000, '2021-03-17'),
(205, 'TR001202103171', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Anugrah Raditya ', 'JNE - 120040042101342', 'Jl. Melong Raya No.141, Cimahi Selatan', 151000, 2, 302000, '2021-03-17'),
(206, 'TR001202103271', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Dimas Hamdan', 'JnT -  1000258540931', 'Jl. Pajajaran No.45, Kota Bandung', 150000, 1, 150000, '2021-03-27'),
(207, 'TR001202103271', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Dimas Hamdan', 'JnT -  1000258540931', 'Jl. Pajajaran No.45, Kota Bandung', 151000, 3, 453000, '2021-03-27'),
(208, 'TR001202104031', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Muhammad Ikhsan Alwan', 'JNE - 120040042107012', 'Jl. Cilember No.4', 149000, 3, 447000, '2021-04-03'),
(209, 'TR001202104131', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Brian Asep ', 'JNE - 120040042107112', 'Komp. Armed No.14, Cimahi Tengah', 149000, 5, 745000, '2021-04-13'),
(210, 'TR001202105021', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Erik Fadliansyah', 'COD', 'Jl. Ahmir Mahmud No.12', 149000, 3, 447000, '2021-05-02'),
(211, 'TR001202105021', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Erik Fadliansyah', 'COD', 'Jl. Ahmir Mahmud No.12', 151000, 4, 604000, '2021-05-02'),
(212, 'TR001202105111', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Dhanis Nugraha', 'COD', 'Jl. Ciawitali No.3', 149000, 4, 596000, '2021-05-11'),
(213, 'TR001202105111', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Dhanis Nugraha', 'COD', 'Jl. Ciawitali No.3', 150000, 3, 450000, '2021-05-11'),
(214, 'TR001202105041', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Diyas Islahudin', 'COD', 'Jl. Cimindi No.56', 151000, 3, 453000, '2021-05-04'),
(215, 'TR001202105041', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Diyas Islahudin', 'COD', 'Jl. Cimindi No.56', 149000, 2, 298000, '2021-05-04'),
(216, 'TR001202106021', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Agya Ananda', 'JNE - 120040041207223', 'Komp. Nusa Hijau Permai Blok. R 12 A', 151000, 1, 151000, '2021-06-02'),
(217, 'TR001202106021', 'R001', 'P004', 'Basic Plain Tee Green(XL)', 'Agya Ananda', 'JNE - 120040041207223', 'Komp. Nusa Hijau Permai Blok. R 12 A', 149000, 1, 149000, '2021-06-02'),
(218, 'TR001202106101', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Samid ', 'JNE - 120040042101233', 'Jalan Merdeka No.15, Kota Solo', 149000, 2, 298000, '2021-06-10'),
(219, 'TR001202106101', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Samid ', 'JNE - 120040042101233', 'Jalan Merdeka No.15, Kota Solo', 150000, 1, 150000, '2021-06-10'),
(220, 'TR001202106111', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Ahmad Solihin', 'JNE - 12004004210123', 'Jl. Pasopati No.6', 149000, 2, 298000, '2021-06-11'),
(221, 'TR001202106111', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Ahmad Solihin', 'JNE - 12004004210123', 'Jl. Pasopati No.6', 150000, 4, 600000, '2021-06-11'),
(222, 'TR001202107141', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Jessica Nur', 'JNE - 120040042101234', 'Jl. Nusa Kambangan No.3', 149000, 2, 298000, '2021-07-14'),
(223, 'TR001202107141', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Jessica Nur', 'JNE - 120040042101234', 'Jl. Nusa Kambangan No.3', 151000, 2, 302000, '2021-07-14'),
(224, 'TR001202107141', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Jessica Nur', 'JNE - 120040042101234', 'Jl. Nusa Kambangan No.3', 150000, 1, 150000, '2021-07-14'),
(225, 'TR001202108141', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Satrio Ananda Daarul', 'JNE - 120040041207223', 'Jl. Pacinan No.4 ', 200000, 2, 400000, '2021-08-14'),
(226, 'TR001202108141', 'R001', 'P001', 'Basic Plain Tee Green (S)', 'Satrio Ananda Daarul', 'JNE - 120040041207223', 'Jl. Pacinan No.4 ', 200000, 3, 600000, '2021-08-14'),
(227, 'TR001202109161', 'R001', 'P005', 'Basic Washed Tee Blue (S)', 'Adolf Hitler', 'JNE - 120040042101442', 'Jl. Budiluhur No.14', 150000, 10, 1500000, '2021-09-16'),
(230, 'TR001202111181', 'R001', 'P002', 'Basic Plain Tee Green (M)', 'Ananda S', 'JNE - 123421421512', 'Komp. Pasospati No 3', 149000, 1, 149000, '2021-11-18');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`IDAdmin`);

--
-- Indeks untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`IDCart`),
  ADD KEY `IDProduct` (`IDProduct`,`IDMember`),
  ADD KEY `IDMember` (`IDMember`);

--
-- Indeks untuk tabel `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`IDCategory`);

--
-- Indeks untuk tabel `list_product`
--
ALTER TABLE `list_product`
  ADD PRIMARY KEY (`No`);

--
-- Indeks untuk tabel `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`IDMember`);

--
-- Indeks untuk tabel `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`No`);

--
-- Indeks untuk tabel `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`No`) USING BTREE,
  ADD KEY `IDProduct` (`IDProduct`,`IDMember`),
  ADD KEY `IDOrder` (`IDOrder`) USING BTREE;

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`IDProduct`),
  ADD KEY `IDAdmin` (`IDAdmin`),
  ADD KEY `ProductType` (`IDCategory`);

--
-- Indeks untuk tabel `reseller`
--
ALTER TABLE `reseller`
  ADD PRIMARY KEY (`IDReseller`);

--
-- Indeks untuk tabel `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`No`);

--
-- Indeks untuk tabel `transaction_reseller`
--
ALTER TABLE `transaction_reseller`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `cart`
--
ALTER TABLE `cart`
  MODIFY `IDCart` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT untuk tabel `list_product`
--
ALTER TABLE `list_product`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT untuk tabel `notification`
--
ALTER TABLE `notification`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `order`
--
ALTER TABLE `order`
  MODIFY `No` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT untuk tabel `slider`
--
ALTER TABLE `slider`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `transaction_reseller`
--
ALTER TABLE `transaction_reseller`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`IDProduct`) REFERENCES `product` (`IDProduct`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`IDAdmin`) REFERENCES `admin` (`IDAdmin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`IDCategory`) REFERENCES `category` (`IDCategory`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
reseller