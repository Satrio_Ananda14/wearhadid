<?php
    class Produk extends CI_Model{
        #region tampil seluruh data
        function tampil_data_Produk(){
            $this->db->select('product.*, category.Name as category');
            $this->db->join('category','category.IDCategory= product.IDCategory', 'left');
            return $this->db->get('product');
        }

        function tampil_data_Kategori(){
            return $this->db->get('category');
        }

        function tampil_data_Daftarproduk($ID){
            $this->db->select('list_product.*, category.Name as category');
            $this->db->join('category','category.IDCategory= list_product.IRCategory', 'left');
            $this->db->where("IDReseller",$ID);
            return $this->db->get('list_product');
        }

        function tampil_data_Slider(){
            return $this->db->get('slider');
        }
        
        #endregion

        function insertproduk($produk){ 
            return $this->db->insert("product",$produk); 
        } 

        function insertkategori($id){
            $kategori = array( 
                "IDCategory" => $id,
                "Name" => $this->input->post("nama")
            );
            return $this->db->insert('Category',$kategori);
        }

        function insertdaftarproduk($daftar_produk){
            return $this->db->insert('list_product',$daftar_produk);
        }

        function insertSlider($slider){
            return $this->db->insert("slider",$slider);
        }

        function Detail_Produk($id){
            $this->db->select('product.*, Admin.Name as Admin');
            $this->db->select('product.*, category.Name as category');
            $this->db->join('Admin','Admin.IDAdmin = product.IDAdmin', 'left');
            $this->db->join('category','category.IDCategory= product.IDCategory', 'left');
            $this->db->where("IDProduct",$id);
            return $this->db->get('Product');
        }

        function Detail_Produk_Dasar($id){
            $this->db->where("IDProduct",$id);
            return $this->db->get('Product');
        }

        function Detail_Kategori($id){
            $this->db->where("IDCategory",$id);		
            return $this->db->get('Category');
        }

        function Detail_Daftar_Produk($ID){
            $this->db->select('list_product.*, category.Name as category');
            $this->db->join('category','category.IDCategory= list_product.IRCategory', 'left');
            $this->db->where("No",$ID);
            return $this->db->get('list_product');
        }

        function Detail_Bantuan_Daftar_Produk($ID, $Product){
            $this->db->where("IDReseller",$ID);
            $this->db->where("IRProduct",$Product);
            return $this->db->get('list_product');
        }

        function Detail_Slider($id){
            $this->db->where("No",$id);
            return $this->db->get('slider');
        }
        
        function Edit_Produk($id,$post){

            $harga=$this->input->post("harga");
            $y= strlen($harga);
            $arr1 = str_split($harga);
            $hargafix="";
            for($x=0;$x<$y;$x++){
                if($arr1[$x]!="."){
                    $hargafix = $hargafix.$arr1[$x];
                }
            }

            $produk = array(
                "IDAdmin" => $_SESSION["id"],
                "ProductName" => $this->input->post("nama"), 
                "IDCategory" => $this->input->post("kategori"), 
                "Description" => $this->input->post("deskripsi"),
                "ItemStock" => $this->input->post("stok"),
                "Price" => $hargafix
                );
            if($post['gambar'] !== null){
                $produk['ImageSource'] = $post['gambar'];
            } 
           
            $this->db->where("IDProduct",$id);
            return $this->db->update("product",$produk);
        }

        function Edit_Kategori($id){
            $Category = array( 
                "Name" => $this->input->post("nama"),
                ); 
            $this->db->where("IDCategory",$id);
            return $this->db->update("Category",$Category);
        }

        function Edit_Daftar_Produk($id,$daftar_produk){
            $this->db->where("No",$id);
            return $this->db->update("list_product",$daftar_produk);
        }

        function Edit_Stok_Daftar_Produk($id,$daftar_produk){
            $this->db->where("No",$id);
            return $this->db->update("list_product",$daftar_produk);
        }

        function Edit_Stok_Produk($id,$stock){
            $this->db->where("IDProduct",$id);
            return $this->db->update("product",$stock);
        }

        function Edit_Slider($id,$post){

            $slider = array(
                "Status" =>$this->input->post("Status")
                );
            if($post['gambar'] != null){
                $slider['ImageSource'] = $post['gambar'];
            } 
            $this->db->where("No",$id);
            return $this->db->update("slider",$slider);
        }

        function Hapus_Produk($id){ 
            $this->db->where("IDProduct",$id); 
            return $this->db->delete("Product"); 
        }

        function Hapus_Kategori($id){ 
            $this->db->where("IDCategory",$id); 
            return $this->db->delete("Category"); 
        }

        function Hapus_Daftar_Produk($ID){
            $this->db->where("No",$ID); 
            return $this->db->delete("list_product");
        }

        function Hapus_Slider($ID){
            $this->db->where("No",$ID); 
            return $this->db->delete("slider");
        }

        function tampil_data_product(){
            return $this->db->get('product');
        }

        function daftar_belum_dijual($id, $product){
            $this->db->select('product.*, category.Name as category');
            $this->db->join('category','category.IDCategory= product.IDCategory', 'left');
            if ($product){
                $this->db->where_not_in('IDProduct', $product);
            }
            return $this->db->get('product');
        }

        function daftar_belum_dijual_tanggal($id, $product){
            $this->db->select('list_product.*, category.Name as category');
            $this->db->join('category','category.IDCategory= list_product.IRCategory', 'left');
            $this->db->where("IDReseller",$id);
            if ($product){
                $this->db->where_not_in('IRProduct', $product);
            }
            return $this->db->get('list_product');
        }
    }
?>