<?php
    class Transaksi extends CI_Model{
        function tampil_data_transaksi($ID){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("IDReseller",$ID);
            $this->db->order_by("Date", "DESC");
            return $this->db->get('transaction_reseller');
        }

        function tampil_data_cart($ID){
            $this->db->select('cart.*, Product.ProductName as Name');
            $this->db->select('cart.*, Product.ImageSource as img');
            $this->db->select('cart.*, Product.Price as Price');
            $this->db->join('Product','Product.IDProduct = Cart.IDProduct', 'left');
            $this->db->where("IDMember",$ID);
            return $this->db->get('cart');
        }

        function tampil_data_cart_id($ID){
            $this->db->select('cart.*, Product.ProductName as Name');
            $this->db->select('cart.*, Product.ImageSource as img');
            $this->db->select('cart.*, Product.Price as Price');
            $this->db->join('Product','Product.IDProduct = Cart.IDProduct', 'left');
            $this->db->where("IDCart",$ID);
            return $this->db->get('cart');
        }

        function tampil_data_order(){
            $this->db->select('Order.*, Product.ProductName as Name');
            $this->db->select('Order.*, Member.Name as MName');
            $this->db->join('Member','Member.IDMember = Order.IDMember', 'left');
            $this->db->join('Product','Product.IDProduct = Order.IDProduct', 'left');
            $this->db->order_by("No", "DESC");
            return $this->db->get('Order');
        }

        function tampil_data_Order_Detail($ID){
            $this->db->select('Order.*, Product.ProductName as Name');
            $this->db->select('Order.*, Product.ImageSource as img');
            $this->db->select('Order.*, Product.Price as PriceProduct');
            $this->db->select('Order.*, Member.Name as MName');
            $this->db->select('Order.*, Member.Email as Email');
            $this->db->join('Member','Member.IDMember = Order.IDMember', 'left');
            $this->db->join('Product','Product.IDProduct = Order.IDProduct', 'left');
            $this->db->where('IDOrder',$ID);
            return $this->db->get('Order');
        }

        function tampil_data_transaksi_tgl($ID,$tgl){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("IDReseller",$ID);
            $this->db->where("Date",$tgl);
            return $this->db->get('transaction_reseller');
        }

        function tampil_data_transaksi_id($no,$ID){
            // $this->db->select('transaction_reseller.*, list_product.ProductName as Name');
            // $this->db->join('list_product','list_product.IRProduct = transaction_reseller.IDProduct', 'left');
            // $this->db->where("transaction_reseller.IDReseller",$ID);
            // $this->db->where("list_product.IDReseller",$ID);
            $this->db->where("IDTransaction",$no);
            return $this->db->get('transaction_reseller');
        }

        function insertdatapenjualan($penjualan){ 
            return $this->db->insert("transaction_reseller",$penjualan); 
        } 

        function Detail_Data_Penjualan($id){
            $this->db->select('transaction_reseller.*, Product.ProductName as ProductName');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("ID",$id);
            return $this->db->get('transaction_reseller');
        }

        function Edit_Data_Penjualan($id,$data_penjualan){
            $this->db->where("ID",$id);
            return $this->db->update("transaction_reseller",$data_penjualan);
        }

        function Hapus_Data_Penjualan($ID){
            $this->db->where("ID",$ID); 
            return $this->db->delete("transaction_reseller");
        }

        function tampil_data_alltransaksi(){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            return $this->db->get('transaction_reseller');
        }

        function Periksa_Tanggal($tanggal, $ID){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("Date", $tanggal);
            $this->db->where("IDReseller", $ID); 
            return $this->db->get('transaction_reseller');
        }

        function tampil_banyak_produk($ID){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("IDReseller",$ID);
            return $this->db->get('transaction_reseller');
        }

        function tampil_data_tgl($bln, $thn){
            $this->db->select('transaction_reseller.*, Reseller.Name as Name');
            $this->db->join('Reseller','Reseller.IDReseller = transaction_reseller.IDReseller', 'left');
            $this->db->where('MONTH(transaction_reseller.Date)',$bln);
            $this->db->where('YEAR(transaction_reseller.Date)',$thn);
            $this->db->order_by("IDReseller", "ASC");
            return $this->db->get('transaction_reseller');
        }

        function tampil_data_tgl_admin($bln, $thn){
            $this->db->select('Order.*, Member.Name as Name');
            $this->db->join('Member','Member.IDMember = Order.IDMember', 'left');
            $this->db->where('MONTH(Order.Date)',$bln);
            $this->db->where('YEAR(Order.Date)',$thn);
            $this->db->order_by("IDOrder", "ASC");
            return $this->db->get('Order');
        }

        function tampil_data_hari($hari){
            $this->db->select('transaction_reseller.*, Reseller.Name as Name');
            $this->db->join('Reseller','Reseller.IDReseller = transaction_reseller.IDReseller', 'left');
            $this->db->select('transaction_reseller.*, Product.ProductName as PName');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where('Date',$hari);
            $this->db->order_by("IDReseller", "ASC");
            return $this->db->get('transaction_reseller');
        }

        function tampil_data_hari_admin($hari){
            $this->db->select('Order.*, Member.Name as Name');
            $this->db->join('Member','Member.IDMember = Order.IDMember', 'left');
            $this->db->select('Order.*, Product.ProductName as PName');
            $this->db->join('Product','Product.IDProduct= Order.IDProduct', 'left');
            $this->db->where('Date',$hari);
            $this->db->order_by("IDOrder", "ASC");
            return $this->db->get('Order');
        }
        
        function tampil_data_bln_reseller($bln, $thn, $id)  {
            $this->db->where('MONTH(transaction_reseller.Date)',$bln);
            $this->db->where('YEAR(transaction_reseller.Date)',$thn);
            $this->db->where('IDReseller', $id);
            $this->db->order_by("Date", "ASC");
            return $this->db->get('transaction_reseller');
        }

        function tampil_data_hari_reseller($hari, $id){
            $this->db->where('Date',$hari);
            $this->db->where("IDReseller", $id);
            return $this->db->get('transaction_reseller');
        }

        function grafik_data_hari($id){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("IDReseller",$id);
            $this->db->order_by("Date", "DESC");
            $this->db->limit(6, 0);
            return $this->db->get('transaction_reseller');
        }

        function CheckTanggal($id,$tgl){
            $this->db->select('transaction_reseller.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= transaction_reseller.IDProduct', 'left');
            $this->db->where("IDReseller",$id);
            $this->db->where("Date",$tgl);
            return $this->db->get('transaction_reseller');
        }

        function insertcart($data){
            return $this->db->insert("cart",$data); 
        }

        function deletecart($ID){
            $this->db->where("IDCart",$ID); 
            return $this->db->delete("cart");
        }

        function updatecart($ID,$data){
            $this->db->where("IDCart",$ID);
            return $this->db->update("cart",$data);
        }

        function CheckTanggalOrder($id,$tgl){
            $this->db->select('order.*, Product.ProductName as Name');
            $this->db->join('Product','Product.IDProduct= order.IDProduct', 'left');
            $this->db->where("IDMember",$id);
            $this->db->where("Date",$tgl);
            return $this->db->get('order');
        }

        function insertdataorder($penjualan){ 
            return $this->db->insert("order",$penjualan); 
        } 

        function tampil_data_bulanan($ID,$i){
            $this->db->select('SUM(Total_Price) as Total_Akhir');
            $this->db->where('MONTH(transaction_reseller.Date)',$i);
            $this->db->where("transaction_reseller.IDReseller",$ID);
            $this->db->from('transaction_reseller');
            return $this->db->get();
        }

        function tampil_data_bulanan_admin($i){
            $this->db->select('SUM(Total_Price) as Total_Akhir');
            $this->db->where('MONTH(Order.Date)',$i);
            $this->db->where("Status",'Deliver');
            $this->db->from('Order');
            return $this->db->get();
        }

        function deleteorder($ID){
            $this->db->where("IDOrder",$ID); 
            return $this->db->delete("order");
        }

        function order_to_deliver($id,$update){
            $this->db->where("IDOrder",$id);
            return $this->db->update("Order",$update);
        }

        function checkcart($IDM, $ID){
            $this->db->where("IDMember",$IDM);
            $this->db->where("IDProduct",$ID);
            return $this->db->get('cart');
        }

        function Update_Bukti_Trans($id,$bukti){
            $this->db->where("IDOrder",$id);
            return $this->db->update("Order",$bukti);
        }
    }
?>
