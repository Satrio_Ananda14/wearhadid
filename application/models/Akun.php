<?php
    class Akun extends CI_Model{
        #region proses login
        function Admin(){
            $username = $this->input->post("username");
            $pass = $this->input->post("password");
            $password = md5($pass);
            $this->db->where("username",$username);
            $this->db->where("password",$password);
            return $this->db->get("Admin");
        } 

        function Reseller(){
            $username = $this->input->post("username");
            $pass = $this->input->post("password");
            $password = md5($pass);
            $this->db->where("username",$username);
            $this->db->where("password",$password);
            return $this->db->get("Reseller");
        }

        function Member(){
            $username = $this->input->post("username");
            $pass = $this->input->post("password");
            $password = md5($pass);
            $this->db->where("username",$username);
            $this->db->where("password",$password);
            return $this->db->get("Member");
        } 
        #endregion
        #region tampil data akun
        function tampil_data_admin(){
            return $this->db->get('Admin');
        }

        function tampil_data_reseller(){
            return $this->db->get('Reseller');
        }

        function tampil_data_Member(){
            return $this->db->get('Member');
        }
        #endregion
        #region proses tambah akun
        function insertAdmin($id){
            $password_toencript = $this->input->post("password");
            $password = md5($password_toencript);
            $Admin = array( 
                "IDAdmin" =>$id,
                "Name" => $this->input->post("nama"),
			    "Username" => $this->input->post("usernama"),
			    "Password" => $password,
			    "Address" => $this->input->post("alamat"),
			    "Phone" => $this->input->post("telfon"),
			    "Gender" => $this->input->post("jeniskelamin"),
			    "Email" => $this->input->post("email")
            );
            return $this->db->insert('Admin',$Admin);
        }

        function insertReseller($id,$status){
            $password_toencript = $this->input->post("password");
            $password = md5($password_toencript);
            $Reseller = array( 
                "IDReseller" => $id,
                "Name" => $this->input->post("nama"),
			    "Username" => $this->input->post("usernama"),
			    "Password" => $password,
			    "Address" => $this->input->post("alamat"),
			    "Phone" => $this->input->post("telfon"),
			    "Gender" => $this->input->post("jeniskelamin"),
			    "Email" => $this->input->post("email"),
                "Status" => $status
            );
            return $this->db->insert('Reseller',$Reseller);
        }

        function insertMember($id){
            $password_toencript = $this->input->post("password");
            $password = md5($password_toencript);
            $Member = array( 
                "IDMember" => $id,
                "Username" => $this->input->post("usernama"),
			    "Password" => $password,
                "Name" => $this->input->post("nama"),
			    "Gender" => $this->input->post("jeniskelamin"),
                "Phone" => $this->input->post("telfon"),
                "Address" => $this->input->post("alamat"),
			    "Postal_Code" => $this->input->post("kodepost"),
                "Detail_Address" => $this->input->post("dtlalamat"),
			    "Email" => $this->input->post("email"),
            );
            return $this->db->insert('Member',$Member);
        }
        #endregion
        #region tampil Detail satu akun
        function Detail_Admin($id){
            $this->db->where("IDAdmin",$id);		
            return $this->db->get('Admin');
        }

        function Detail_Reseller($id){
            $this->db->where("IDReseller",$id);		
            return $this->db->get('Reseller');
        }

        function Detail_Member($id){
            $this->db->where("IDMember",$id);		
            return $this->db->get('Member');
        }
        #endregion
        #region edit akun
        function Edit_Admin($id){
            $password_toencript = $this->input->post("password");
            $password = md5($password_toencript);
            $Admin = array( 
                "Name" => $this->input->post("nama"),
			    "Username" => $this->input->post("usernama"),
			    "Password" => $password,
			    "Address" => $this->input->post("alamat"),
			    "Phone" => $this->input->post("telfon"),
			    "Gender" => $this->input->post("jeniskelamin"),
			    "Email" => $this->input->post("email")
                // "Status" => $this->input->post("status")
                ); 
            $this->db->where("IDAdmin",$id);
            return $this->db->update("Admin",$Admin);
        }

        function Edit_Reseller($id){
            $password_toencript = $this->input->post("password");
            $password = md5($password_toencript);
            $Reseller = array( 
                "Name" => $this->input->post("nama"),
			    "Username" => $this->input->post("usernama"),
			    "Password" => $password,
			    "Address" => $this->input->post("alamat"),
			    "Phone" => $this->input->post("telfon"),
			    "Gender" => $this->input->post("jeniskelamin"),
			    "Email" => $this->input->post("email"),  
                "Status" => $this->input->post("status")
                ); 
            $this->db->where("IDReseller",$id);
            return $this->db->update("Reseller",$Reseller);
        }

        function Edit_Reseller_Accept($ID, $Reseller){
            $this->db->where("IDReseller",$ID);
            return $this->db->update("Reseller",$Reseller);
        }

        function Edit_Member($id){
            $password_toencript = $this->input->post("password");
            $password = md5($password_toencript);
            $Member = array( 
                "Username" => $this->input->post("usernama"),
			    "Password" => $password,
                "Name" => $this->input->post("nama"),
			    "Gender" => $this->input->post("jeniskelamin"),
                "Phone" => $this->input->post("telfon"),
                "Address" => $this->input->post("alamat"),
			    "Postal_Code" => $this->input->post("kodepost"),
                "Detail_Address" => $this->input->post("dltalamat"),
			    "Email" => $this->input->post("email")
                ); 
            $this->db->where("IDMember",$id);
            return $this->db->update("Member",$Member);
        }
        #endregion
        #region hapus akun
        function Hapus_Admin($id){ 
            $this->db->where("IDAdmin",$id); 
            return $this->db->delete("Admin"); 
        }

        function Hapus_Reseller($id){ 
            $this->db->where("IDReseller",$id); 
            return $this->db->delete("Reseller"); 
        }

        function Hapus_Member($id){ 
            $this->db->where("IDMember",$id); 
            return $this->db->delete("Member"); 
        }
        #endregion
    }
?> 