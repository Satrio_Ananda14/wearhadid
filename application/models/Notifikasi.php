<?php 
    class Notifikasi extends CI_Model{
        function tampil_data_Notifikasi(){		
            $this->db->order_by("Date", "ASC");
            $this->db->limit(5, 0);
            $this->db->where("Status", "not seen");
            return $this->db->get("notification");
        }

        function insertnotifikasi($data){
            $this->db->insert("notification",$data);
        }
    
        function Edit_Notifikasi($No,$data){
            $this->db->where("No",$No);
            return $this->db->update("notification",$data);
        }

        function Edit_Notifikasi_Order_Reseller($No,$data){
            $this->db->where("ID",$No);
            return $this->db->update("notification",$data);
        }

        function total_notifikasi()
        {   
            $this->db->select('*');
            $this->db->from('notification');
            $this->db->where("Status", "not seen" );
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                return $query->num_rows();
            }
            else
            {
                return 0;
            }
        }
}
?>