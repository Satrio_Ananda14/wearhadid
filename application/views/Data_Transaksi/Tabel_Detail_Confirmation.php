<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Confirmation</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Transaction</a></li>
              <li class="breadcrumb-item active">Confirmation</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Produk</h3>
                  <a href="<?php echo site_url('Kelola_Produk/TambahProduk');?>" type="button" class="btn btn-primary btn-sm" style="float: right;"><i class="fas fa fa-plus"></i> Tambah Product </a>
                </div> -->
              <div class="card-body">
                    <?php foreach($order as $u){
                        $IDTransaksi = $u->IDOrder;
                        $Tanggal = $u->Date;
                        $Email = $u->Email;
                        $Nama = $u->MName;
                        $Address = $u->Address;
                        $Detail_Address = $u->Detail_Address;
                        $Postal_Code= $u->Postal_Code;
                        $Province = $u->Province;
                        $City = $u->City;
                        $District = $u->Districts;
                    }?>

                    <table width="100%">
                        <tr>
                            <th> ID Transaksi </th>
                            <th> : </th>
                            <th> <?php Echo $IDTransaksi ?></th>
                            <th> Nama </th>
                            <th> : </th>
                            <th> <?php Echo $Nama ?></th>
                            <th> </th>
                            <th> </th>
                        </tr>
                        <tr>
                            <td> Alamat </td>
                            <td> : </td>
                            <td> <?php Echo $Address ?></td>
                            <td> Detail Alamat </td>
                            <td> : </td>
                            <td> <?php Echo $Detail_Address ?></td>
                        </tr>
                        <tr>
                            <td> KodePos </td>
                            <td> : </td>
                            <td> <?php Echo $Postal_Code ?></td>
                            <td> Email </td>
                            <td> : </td>
                            <td> <?php Echo $Email ?></td>
                        </tr>
                        <tr>
                            <td> Provinsi </td>
                            <td> : </td>
                            <td> <?php Echo $Province ?></td>
                            <td> Kota </td>
                            <td> : </td>
                            <td> <?php Echo $City ?></td>
                        </tr>
                        <tr>
                            <td> Kecamatan </td>
                            <td> : </td>
                            <td> <?php Echo $District ?></td>
                        </tr>
                    </table>

                    <table width=100% border=1 class="table table-bordered table-striped">
                    <tr class="text-center">
                        <td> Nama Product </td>
                        <td> Quantity </td>
                        <td> Total Harga </td>
                    </tr>
                    <?php
                    $Total_Harga=0;
                    foreach($order as $u){?>
                    <tr>
                        <td > <?php echo $u->Name?> </td>
                        <td class="text-center"> <?php echo $u->Qty?> </td>
                        <td>Rp. <?php echo number_format($u->Total_Price,0,',','.')?> </td>
                    </tr>
                    <?php 
                        $Total_Harga = $Total_Harga + $u->Total_Price;
                        } ?>
                    <tr>
                    <th colspan="2" style="text-align:center">Total Ongkos Kirim</th>
                        <th style="text-align:left">Rp. <?php echo number_format($u->Shipping_Cost,0,',','.') ?></th>
                    </tr>
                    <tr>
                    <th colspan="2" style="text-align:center">Total Harga</th>
                        <th style="text-align:left">Rp. <?php echo number_format($Total_Harga+$u->Shipping_Cost,0,',','.') ?></th>
                    </tr>
                    </table>

                    <table width=100%>
                        <tr>
                            <td> BUKTI PEMBAYARAN</td>
                        </tr>

                        <tr align=center>
                            <?php if($u->ImageSource != ""){?>
                                <td><img src="<?php echo base_url('assets/dist/img/'),$u->ImageSource ?>" width=50% height=50% class="text-center"/></td>
                            <?php }else{?>
                                <td> Orde Belum Dibayar</td>
                            <?php }?>
                        </tr>

                        <tr height=30>
                            <td> </td>
                        </tr>
                    <table>

                    <table align="center" class="md-2">
                    <?php if($u->ImageSource) { ?>
                        <tr>
                            <td colspan=3> 
                                <form action="<?php echo site_url('Data_Transaksi/ProsesKirim/'.$IDTransaksi);?>" method="post" id="form">
                                    <div class="form-group">
                                    <label>Expedition : </label>
                                    <input type="text" name="nama" class="form-control" maxlength="25" size="10" required>
                                    </div>
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <td><button type="submit" form="form" class="btn btn-success" class="text-center" onclick="return confirm('Sudah yakin Order Sudah Benar ?')">Accept</button> </td>
                            <!-- <td><button type="submit" form="form" class="btn btn-success" >Accept</button> </td> -->
                            <td width=50> </td>
                    <?php } ?>
                            <td><a type="button" href="<?php echo site_url('Data_Transaksi/ProsesHapusOrder/'.$IDTransaksi);?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin Order akan Ditolak ?')">Reject</button> </a>
                        </tr>
                    </table>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>