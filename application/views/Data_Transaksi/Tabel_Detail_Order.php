<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Transaction</a></li>
              <li class="breadcrumb-item active">Order</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Produk</h3>
                  <a href="<?php echo site_url('Kelola_Produk/TambahProduk');?>" type="button" class="btn btn-primary btn-sm" style="float: right;"><i class="fas fa fa-plus"></i> Tambah Product </a>
                </div> -->
              <div class="card-body">
                    <?php foreach($order as $u){
                        $IDTransaksi = $u->IDOrder;
                        $Tanggal = $u->Date;
                        $Email = $u->Email;
                        $Nama = $u->MName;
                        $Address = $u->Address;
                        $Detail_Address = $u->Detail_Address;
                        $Postal_Code= $u->Postal_Code;
                        $Province = $u->Province;
                        $City = $u->City;
                        $District = $u->Districts;
                    }?>

                    <table width="100%">
                        <tr>
                            <th> ID Transaksi </th>
                            <th> : </th>
                            <th> <?php Echo $IDTransaksi ?></th>
                            <th> Nama </th>
                            <th> : </th>
                            <th> <?php Echo $Nama ?></th>
                            <th> </th>
                            <th> </th>
                        </tr>
                        <tr>
                            <td> Alamat </td>
                            <td> : </td>
                            <td> <?php Echo $Address ?></td>
                            <td> Detail Alamat </td>
                            <td> : </td>
                            <td> <?php Echo $Detail_Address ?></td>
                        </tr>
                        <tr>
                            <td> KodePos </td>
                            <td> : </td>
                            <td> <?php Echo $Postal_Code ?></td>
                            <td> Email </td>
                            <td> : </td>
                            <td> <?php Echo $Email ?></td>
                        </tr>
                        <tr>
                            <td> Provinsi </td>
                            <td> : </td>
                            <td> <?php Echo $Province ?></td>
                            <td> Kota </td>
                            <td> : </td>
                            <td> <?php Echo $City ?></td>
                        </tr>
                        <tr>
                            <td> Kecamatan </td>
                            <td> : </td>
                            <td> <?php Echo $District ?></td>
                        </tr>
                        
                    </table>

                    <table width=100% border=1 class="table table-bordered table-striped">
                    <tr class="text-center">
                        <td> Nama Product </td>
                        <td> Quantity </td>
                        <td> Total Harga </td>
                    </tr>
                    <?php
                    $Total_Harga=0;
                    foreach($order as $u){?>
                    <tr>
                        <td > <?php echo $u->Name?> </td>
                        <td class="text-center"> <?php echo $u->Qty?> </td>
                        <td>Rp. <?php echo number_format($u->Total_Price,0,',','.')?> </td>
                    </tr>
                    <?php 
                        $Total_Harga = $Total_Harga + $u->Total_Price;
                        } ?>
                    <tr>
                    <th colspan="2" style="text-align:center">Total Harga</th>
                        <th style="text-align:left">Rp. <?php echo number_format($Total_Harga,0,',','.') ?></th>
                    </tr>
                    </table>
                    <table align="center" class="md-2">
                        <tr>
                            <td colspan=3> 
                                <form action="<?php echo site_url('Data_Transaksi/ProsesKonfirmasi/'.$IDTransaksi);?>" method="post" id="form">
                                    <div class="form-group">
                                    <label>Biaya Ongkos Kirim : </label>
                                      <input type="number" name="ongkir" id="masking1" max="200" min="0" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control" required>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td><button type="submit" form="form" class="btn btn-success" class="text-center" onclick="return confirm('Sudah yakin Order Sudah Benar ?')">Tambah Ongkos</button> </td>
                            <!-- <td><button type="submit" form="form" class="btn btn-success" >Accept</button> </td> -->
                            <td width=50> </td>
                    
                            <td><a type="button" href="<?php echo site_url('Data_Transaksi/ProsesHapusOrder/'.$IDTransaksi);?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin Order akan Ditolak ?')">Reject</button> </a>
                        </tr>
                    </table>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>