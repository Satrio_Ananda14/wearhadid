<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Transaction</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="max-width:90%;margin-left:3rem;">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Order</h3>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th class="text-center">ID Transaction</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    $no = 1;
                    $bantuan_NoPenjualan="";
                    $Total_Penjualan=0;
                    foreach($order as $u){
                      if($u->Status == 'Order'){
                      $Penjualan = $u->IDOrder;
                      if($bantuan_NoPenjualan != $Penjualan){
                        $bantuan_NoPenjualan= $Penjualan;
                        $Total_Penjualan=0;
                          foreach($order as $s){
                            if($Penjualan==$s->IDOrder){
                              $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                            }
                          }
                  ?>
                  <tr>
                    <th><?php  echo $no++?></th>
                    <th><?php  echo $u->MName?></th> 
                    <th class="text-center"><?php  echo $u->IDOrder?></th>
                    <th class="text-center">
                      <a href="<?php echo site_url('Data_Transaksi/Detail_Order/'.$u->IDOrder);?>" type="button" id="<?php echo $u->IDOrder ?>" class="detail btn btn-primary btn-sm justify-content-center"><i class="fas fa fa-eye"></i> Proses</a>
                    </th>
                  <?php }
                  }} ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="max-width:90%;margin-left:3rem;">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Confirmation </h3>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>ID Transaction</th>
                    <th>Confirmation Payment</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    $no = 1;
                    $bantuan_NoPenjualan="";
                    $Total_Penjualan=0;
                    foreach($order as $u){
                      if($u->Status == 'Confir'){
                      $Penjualan = $u->IDOrder;
                      if($bantuan_NoPenjualan != $Penjualan){
                        $bantuan_NoPenjualan= $Penjualan;
                        $Total_Penjualan=0;
                          foreach($order as $s){
                            if($Penjualan==$s->IDOrder){
                              $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                            }
                          }
                  ?>
                  <tr>
                    <th><?php  echo $no++?></th>
                    <th><?php  echo $u->MName?></th> 
                    <th><?php  echo $u->IDOrder?></th>
                    <th class="text-center"><?php if($u->ImageSource){?>
                            Ada Konfirmasi Pembayaran
                        <?php }else{?>
                            Belum Ada Konfirmasi Pembayaran
                        <?php } ?>
                    </th>
                    <th class="text-center">
                      <a href="<?php echo site_url('Data_Transaksi/Detail_Confirmation/'.$u->IDOrder);?>" type="button" id="<?php echo $u->IDOrder ?>" class="detail btn btn-primary btn-sm justify-content-center"><i class="fas fa fa-eye"></i> Detail</a>
                    </th>
                  <?php }
                  }} ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="max-width:90%;margin-left:3rem;">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i>Transaction (Sent)</h3>
                </div>
              <div class="card-body">
              <table id="example2" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>ID Transaction</th>
                    <th>Expedition</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    $no = 1;
                    $bantuan_NoPenjualan="";
                    $Total_Penjualan=0;
                    foreach($order as $u){
                      if($u->Status == 'Deliver'){
                      $Penjualan = $u->IDOrder;
                      if($bantuan_NoPenjualan != $Penjualan){
                        $bantuan_NoPenjualan= $Penjualan;
                        $Total_Penjualan=0;
                          foreach($order as $s){
                            if($Penjualan==$s->IDOrder){
                              $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                            }
                          }
                  ?>
                  <tr>
                    <th><?php  echo $no++?></th>
                    <th><?php  echo $u->MName?></th> 
                    <th><?php  echo $u->IDOrder?></th>
                    <th><?php  echo $u->Expedition?></th>
                    <th class="text-center">
                      <a href="<?php echo site_url('Data_Transaksi/Detail_Deliver/'.$u->IDOrder);?>" type="button" id="<?php echo $u->IDOrder ?>" class="detail btn btn-primary btn-sm justify-content-center"><i class="fas fa fa-eye"></i> Detail</a>
                    </th>
                  <?php }
                  }} ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail Transaction</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="tampil_modal">
              
              </div>
            </div>
            <div class="modal-footer justify-content-right">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="myModal2">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail Confrimation Payment</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="tampil_modal2">
              
              </div>
            </div>
            <div class="modal-footer justify-content-right">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="myModal3">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail Deliver</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="tampil_modal3">
              
              </div>
            </div>
            <div class="modal-footer justify-content-right">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->