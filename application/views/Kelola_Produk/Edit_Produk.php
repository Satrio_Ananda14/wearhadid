<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola Produk</a></li>
              <li class="breadcrumb-item active">Edit Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
        <?php foreach($produk as $u){ ?>
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"><?php echo $u->IDProduct; ?></h6>
          </div>
            <div class="card-body">
              <?php echo form_open_multipart('Kelola_Produk/ProsesEditProduk/'.$u->IDProduct)?>
                <div class="form-group">
                  <label>Nama Produk</label>
                  <input type="text" name="nama" class="form-control" value="<?php echo $u->ProductName; ?>" maxlength="25" size="10"  required>
                </div>
                <div class="form-group">
                  <label>Kategori</label>
                  <select name="kategori" class="form-control">
                    <?php foreach($kategori as $s) { ?>
                      <?php echo '<option value='.$s->IDCategory ?> <?php if($s->IDCategory==$u->IDCategory){ echo "selected"; }?>><?php echo $s->Name ?></option>
                    <?php } ?>
                  </select>   
                </div>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <input type="Text" name="deskripsi" class="form-control" value="<?php echo $u->Description; ?>" required>  
                </div>
                <div class="form-group">
                  <label>Jumlah Barang</label>
                  <input type="Number" name="stok" class="form-control" value="<?php echo $u->ItemStock; ?>" max="7500" min="0" oninvalid="this.setCustomValidity('Stok dibatasi sebanyak 7500')" oninput="setCustomValidity('')"  required>
                </div>
                <div class="form-group">
                  <label>Harga</label>
                  <input type="number" name="harga" id="masking1" max="200" min="100" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control" value="<?php echo $u->Price; ?>" required>
                </div>
                <div class="form-group">
                  <label>Foto</label>
                  <img src="<?php echo base_url('assets/dist/img/'),$u->ImageSource ?>" class="card-img"/>
                  <input type="file" name="gambar" class="form-control" value="<?php $u->ImageSource ?>" >
                </div>
                <?php }?>
                    <a href="<?php echo site_url('Kelola_Produk/DetailProduk/'),$u->IDProduct;?>" class="btn btn-light btn-icon-split" style="float: left;">
                    <span class="icon text-gray-600">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                    </a>
                    <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
              <?php echo form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>