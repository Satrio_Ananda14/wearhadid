<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kelola Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola Produk</a></li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i>Category</h3>
                  <a href="<?php echo site_url('Kelola_Produk/TambahKategori');?>" type="button" class="btn btn-primary btn-sm" style="float: right;"><i class="fas fa fa-plus"></i> Tambah Category </a>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">ID</th>
                    <th class="text-center">Kategori</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                        $no = 1;
                        foreach($category as $u){ 
                    ?>
                  <tr>
                    <th class="text-center"><?php echo $no++ ?></th>
                    <th class="text-center"><?php echo $u->IDCategory ?></th>
                    <th class="text-center"><?php echo $u->Name ?></th>
                    <th class="text-center">
                      <a href="<?php echo site_url('Kelola_Produk/EditKategori/'.$u->IDCategory);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Edit</a>
                      <a href="<?php echo site_url('Kelola_Produk/ProsesHapusKategori/'.$u->IDCategory);?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin menghapus data <?php echo $u->IDCategory?> ?')"><i class="fas fa fa-trash"></i> Hapus </a>
                    </th>
                  </tr>
                  <?php ;}?>
                  <?php  ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi Hapus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <label>Hapus Data ?</label>
        </div>
        <div class="modal-footer justify-content-between">
          <a type="button" href="<?php echo site_url('Kelola_Produk/ProsesHapusKategori/'.$u->IDCategory);?>" class="btn btn-primary" style="width:75px">Ya</a>
          <button type="button" class="btn btn-danger" data-dismiss="modal" style="width:75px">Tidak</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->