<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Kategori</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola Produk</a></li>
              <li class="breadcrumb-item active">Edit Kategori</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
            <div class="card-body">
            <?php foreach($category as $u){ ?>
              <form action="<?php echo site_url('Kelola_Produk/ProsesEditKategori/'), $u->IDCategory; ?>" method="post">
                <div class="form-group">
                  <label>Nama Kategori</label>
                  <input type="text" name="nama" class="form-control" value="<?php echo $u->Name; ?>" maxlength="25" size="10" required>
                </div>
                <?php }?>
                <a href="<?php echo site_url('Kelola_Produk/Kategori/');?>" class="btn btn-light btn-icon-split" style="float: left;">
                <span class="icon text-gray-600">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Kembali</span>
                </a>
                <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>