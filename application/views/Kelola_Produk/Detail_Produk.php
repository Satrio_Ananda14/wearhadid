<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola Produk</a></li>
              <li class="breadcrumb-item active">Detail Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12" style="max-width:80%;margin-left:5rem;">
            <div class="card mb-4">
            <?php foreach($produk as $u){ ?>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?php echo $u->IDProduct ?></th></h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                      <label>Ditambahkan Oleh Admin :</label>
                      <h4> <?php echo $u->Admin ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Nama Produk :</label>
                      <h4> <?php echo $u->ProductName ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Kategori</label>
                      <h4> <?php echo $u->category ?></div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <h4> <?php echo $u->Description ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Jumlah Stok</label>
                      <h4> <?php echo $u->ItemStock ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Harga</label>
                      <h4>Rp. <?php echo number_format($u->Price,0,',','.') ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Foto</label>
                      <img src="<?php echo base_url('assets/dist/img/'),$u->ImageSource ?>" class="card-img"/>
                    </div>
                    <hr class="sidebar-divider">
                  <a href="<?php echo site_url('Kelola_Produk/Produk');?>" class="btn btn-light btn-icon-split" >
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                  </a>
                  <a href="<?php echo site_url('Kelola_Produk/EditProduk/'),$u->IDProduct ;?>" class="btn btn-primary btn-icon-split" style="float: right;">
                    <span class="text">Edit Produk</span>
                    <?php } ?>
                  </a>
                </div>
              </div>
            </div>
            </div>
          </div>
</div>