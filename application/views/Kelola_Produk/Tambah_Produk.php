<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola Produk</a></li>
              <li class="breadcrumb-item active">Tambah Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:90%;margin-left:3rem;">
        <div class="card mb-4">
            <div class="card-body">
              <?php echo form_open_multipart('Kelola_Produk/ProsesTambahProduk')?>
                <div class="form-group">
                  <label>ID</label>
                  <input type="text" name="ID" class="form-control" value="<?php echo $ID ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control only-string" maxlength="25" size="10" required>
                </div>
                <div class="form-group">
                  <label>Kategori</label>
                  <select name="kategori" class="form-control">
                    <?php foreach($category as $u) { 
                    echo '<option value="'.$u->IDCategory.'">'.$u->Name.'</option>';
                    } ?>
                  </select> 
                </div>
                <div class="form-group">
                  <label>Stok</label>
                  <input type="number" name="stok" class="form-control only-number" max="7500" min="0" oninvalid="this.setCustomValidity('Stok dibatasi sebanyak 7500')" oninput="setCustomValidity('')" required>
                </div>
                <div class="form-group">
                  <label>Harga (Rp)</label>
                  <input type="number" name="harga" id="masking1" max="200" min="100" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <input type="text" name="deskripsi" class="form-control" required>  
                </div>
                <div class="form-group">
                  <label>Image</label>
                  <input type="file" name="gambar" class="form-control">
                </div>
                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary btn-sm " class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Tambah</button>
                </div>
              </form>
              <?php echo form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>