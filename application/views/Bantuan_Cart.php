<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Produk</h3>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" border="1">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                        $no = 1;
                        foreach($produk as $u){ 
                    ?>
                  <tr>
                    <th><?php echo $no++ ?></th>
                    <th><?php echo $u->ProductName ?></th>
                    <th><?php echo $u->Deskripsi ?></th>
                    <th><?php echo $u->category ?></th>
                    <th>Rp. <?php echo number_format($u->Price,0,',','.') ?></th>
                    <th class="justify-content-center">
                      <a href="<?php echo site_url('Cart/Add_Cart/'.$u->IDProduct);?>" class="btn btn-primary btn-sm" ><i class="fas fa fa-eye"></i> Add To Cart</a>
                    </th>
                  </tr>
                  <?php ;}?>
                  <?php  ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>