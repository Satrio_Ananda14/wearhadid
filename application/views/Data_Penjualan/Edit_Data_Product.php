<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Edit Reseller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Data Reseller</h6>
          </div>
          <div class="card-body">
          <?php foreach($Product as $u){ ?>
            <form class="form-horizontal" action="<?php echo site_url('Data_Penjualan/ProsesEditDataPenjualan/'); ?>" method="post" id="formpenjualan1">
                <input type="hidden" name="id" value="<?php echo $u->ID; ?>" class="form-control">
                <input type="hidden" name="idproduct" value="<?php echo $u->IDProduct; ?>" class="form-control">
                <input type="hidden" name="idpenjualan" value="<?php echo $u->IDTransaction; ?>"  class="form-control">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" value="<?php echo $u->ProductName; ?>" class="form-control" value= readonly>
                </div>
                <div class="form-group">
                    <label>Total Produk</label>
                    <input type="number" name="qty"  value="<?php echo $u->Quantity; ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Harga</label>
                    <input type="number" name="harga" id="masking1" value="<?php echo $u->Price; ?>" max="200" min="100" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control" required> 
                </div>
            <?php } ?>
                <a href="<?php echo site_url('Data_Penjualan/ProsesHapusDataPenjualanDetail/'.$u->ID);?>" class="btn btn-light btn-icon-split" style="float: left;">
                    <span class="icon text-gray-600">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                </a>
            <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
