<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Reseller</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Tambah Reseller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

  <section class="content">
    <div class="container-fluid">
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Tambah Data Penjualan</h6>
          </div>
            <div class="card-body">
              <form action="<?php echo site_url('Data_Penjualan/ProsesTambahDataPenjualan'); ?>" method="post">
                <div class="form-group">
                  <label>Tanggal</label>
                  <input type="date" name="tgl" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Produk</label>
                  <select name="produk" class="form-control">
                    <?php foreach($produk as $u) { 
                    echo '<option value="'.$u->IRProduct.'">'.$u->ProductName.'</option>';
                    } ?>
                  </select> 
                </div>
                <div class="form-group">
                  <label>Qty</label>
                  <input type="Number" name="qty" class="form-control" required>  
                </div>
                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary btn-sm " class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Tambah Data Penjualan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>