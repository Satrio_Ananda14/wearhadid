<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Edit Reseller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Data Reseller</h6>
          </div>
            <div class="card-body">
            <?php foreach($transaksi as $u){ ?>
              <form action="<?php echo site_url('Data_Penjualan/ProsesEditDataPenjualan/'), $u->ID; ?>" method="post" id="newModalForm">
                <div class="form-group">
                  <label>Tanggal</label>
                  <input type="text" name="tgl" class="form-control" value="<?php echo $u->Tanggal; ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Produk</label>
                  <select name="produk" class="form-control">
                    <?php foreach($produk as $s) { ?>
                    <?php echo '<option value='.$s->IRProduct ?> <?php if($s->IRProduct==$u->IDProduct){ echo "selected"; }?>><?php echo $s->ProductName ?></option> 
                    <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                  <label>Qty</label>
                  <input type="Number" name="qty" class="form-control" value="<?php echo $u->Quantity; ?>" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" >
                </div>
                <?php }?>
                  <a href="<?php echo site_url('Kelola_Produk/DetailProduk/');?>" class="btn btn-light btn-icon-split" style="float: left;">
                  <span class="icon text-gray-600">
                      <i class="fas fa-arrow-left"></i>
                  </span>
                  <span class="text">Kembali</span>
                  </a>
                  <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<script>
  $(function () {        

  $("#newModalForm").validate({
      rules: {
          pName: {
              required: true,
              minlength: 8
          },
          action: "required"
      },
      messages: {
          pName: {
              required: "Please enter some data",
              minlength: "Your data must be at least 8 characters"
          },
          action: "Please provide some data"
      }
  });
});
</script>