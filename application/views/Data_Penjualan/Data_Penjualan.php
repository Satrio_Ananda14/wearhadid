<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Penjualan</a></li>
              <li class="breadcrumb-item active">Penjualan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i>    Penjualan / Transaksi</h3>
                  <a href="<?php.// echo site_url('Data_Penjualan/TambahDataPenjualan');?>" type="button" class="btn btn-primary btn-sm" style="float: right;" data-toggle="modal" data-target="#modal-default"><i class="fas fa fa-plus"></i> Tambah Data</a>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center" >ID Penjualan</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Total Penjualan</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                        $no = 1;
                        $bantuan_NoPenjualan="";
                        $Total_Penjualan=0;
                        foreach($transaksi as $u){
                          $Penjualan = $u->IDTransaction;
                          if($bantuan_NoPenjualan != $Penjualan){
                            $bantuan_NoPenjualan= $Penjualan;
                            $Total_Penjualan=0;
                              foreach($transaksi as $s){
                                if($Penjualan==$s->IDTransaction){
                                  $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                                }
                              }
                            ?>
                            <tr>
                                <th class="text-center"><?php echo $no++?></th>
                                <th class="text-center"><?php echo $u->IDTransaction?></th>
                                <th class="text-center"><?php echo $u->Date?></th>
                                <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.') ?></th>
                                <th class="text-center">
                                  <a href="<?php echo site_url('Data_Penjualan/DetailPenjualan/'.$u->IDTransaction);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Detail </a>
                                </th>
                          <?php }
                          ?>
                             </tr>
                      <?php }?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Data Transaksi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo site_url('Data_Penjualan/SetBantuan'); ?>" method="post" id="formpenjualan1">
            <div class="form-group">
              <label>Tanggal</label>
              <input type="text" name="tgl" id="disableFuturedate" class="form-control" required>
              <!-- <input type="text" class="disableFuturedate"> -->
              <!-- <input type="date"  required> -->
            </div>
            <div class="form-group">
              <label>Nama Pembeli</label>
              <input type="text" name="nama" class="form-control only-string" maxlength="25"  size="10" required>
            </div>
            <div class="form-group">
              <label>Ekpedisi</label>
              <input type="text" name="ekpedisi" class="form-control" required>  
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <input type="text" name="almt" class="form-control" required>  
            </div>
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" form="formpenjualan1" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Tambah Data Transaksi</button>
        </div>
      </div>
          <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->