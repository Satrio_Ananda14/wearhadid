<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Detail Penjualan</a></li>
              <li class="breadcrumb-item active">Penjualan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card" style="height:auto;">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Transaksi</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="row">
                      <div class="col-6">
                        <label>No Transaksi :</label>
                        <h5><?php echo $_SESSION["bantuan_notransaksi"] ?></h5>
                      </div>
                      <div class="col-6">
                        <label>Tanggal :</label>
                        <h5><?php echo $_SESSION["bantuan_tgl"] ?></h5>
                      </div>
                  </div>
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                        <label>Nama Pembeli :</label>
                        <h5><?php echo $_SESSION["bantuan_nama"] ?></h5>
                    </div>
                    <div class="col-6">
                      <label>Ekpedisi :</label>
                      <h5><?php echo $_SESSION["bantuan_ekspedisi"] ?></h5>
                    </div>
                  </div>
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label>Alamat :</label>
                      <h5><?php echo $_SESSION["bantuan_almt"] ?></h5>
                    </div>
                  </div>
                </div>    
                  <hr class="sidebar-divider">
                <div class="row justify-content-md-center">
                  <div class="col col-lg-2">
                    <a href="<?php echo site_url('Data_Penjualan/Cetak_Invoice/').$_SESSION["bantuan_notransaksi"];?>"" type="button" class="btn btn-success" class="text-center">Cetak</a>
                  </div>
                  <div class="col-md-auto">
                          
                  </div>
                  <div class="col col-lg-2">
                    <a href="<?php echo site_url('Data_Penjualan/SimpanData/').$_SESSION["bantuan_notransaksi"];?>" type="button" class="btn btn-success" class="text-center" onclick="success_toast()">Simpan</a>
                  </div>
              </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card" style="height:auto;">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Tambah Produk</h3>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo site_url('Penjualan/ProsesTambahDataPenjualan'); ?>" method="post">
                  <div class="form-group">
                    <label>Produk</label>
                    <select name="produk" class="form-control">
                      <?php
                        foreach($produk as $u) {
                        echo '<option value="'.$u->IRProduct.'">'.$u->ProductName.'</option>';
                        } ?>
                    </select> 
                  </div>
                  <hr class="sidebar-divider">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-6">
                        <label>Qty</label>
                        <input type="Number" name="qty" max="10000" min="1" class="form-control" required>
                      </div>
                      <div class="col-6">
                        <label>Harga</label>
                        <input type="number" name="hrg" id="masking1" max="200" min="100" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control">
                      </div>
                    </div>
                     <hr class="sidebar-divider">
                    <div class="col-12">
                      <p style="text-align:justify; Color:Red;"><?php echo $this->session->userdata("error"); ?></p>
                    </div>
                  </div>
                   
                    <?php if($produk){ ?>
                      <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary btn-sm" class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Tambah</button>
                      </div>
                    <?php } ?>
                </form>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              
              <div class="card-body">
              <table class="table table-bordered table-striped">
                  <thead>
                  <tr style="text-align:center">
                    <th>No.</th>
                    <th>Produk</th>
                    <th>Total Produk Terjual</th>
                    <th>Harga Produk</th>
                    <th>Total Penjualan</th>
                    <th style="text-align:center">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                    $no = 1;
                    $Total_Harga = 0;
                    foreach($transaksi as $u){
                    ?>
                  <tr class="text-center">
                    <th><?php echo $no++?></th>
                    <th class="text-left"><?php echo $u->ProductName?></th>
                    <th><?php echo $u->Quantity?></th>
                    <th style="text-align:left">Rp. <?php echo number_format($u->Price,0,',','.') ?></th>
                    <th style="text-align:left">Rp. <?php echo number_format($u->Total_Price,0,',','.') ?></th>
                    <th>
                     <!-- <a href="<?php echo site_url('Data_Penjualan/EditDataPenjualan/'.$u->ID);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Edit</a> -->
                     <a href="<?php echo site_url('Data_Penjualan/ProsesHapusDataPenjualan/'.$u->ID);?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin menghapus data ?')"><i class="fas fa fa-trash"></i> Hapus </a>
                    </th>
                  </tr>
                  <tr>
                    <?php $Total_Harga = $Total_Harga + $u->Total_Price;
                     } ?>
                    <th colspan="4" style="text-align:center">Total Harga</th>
                    <th style="text-align:left">Rp. <?php echo number_format($Total_Harga,0,',','.') ?></th>
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>