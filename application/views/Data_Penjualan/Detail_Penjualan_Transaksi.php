<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Detail Penjualan</a></li>
              <li class="breadcrumb-item active">Penjualan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php foreach($transaksi as $u){
        $IDTransaksi = $u->IDTransaction;
        $Tanggal = $u->Date;
        $Nama = $u->NameBuyer;
        $Ekspedisi = $u->Expedition;
        $Address = $u->Address;
    }?>
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card" style="height:auto;">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Transaksi</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="row">
                      <div class="col-6">
                        <label>No Transaksi :</label>
                        <h5><?php echo $IDTransaksi ?></h5>
                      </div>
                      <div class="col-6">
                        <label>Tanggal :</label>
                        <h5><?php echo $Tanggal ?></h5>
                      </div>
                  </div>
                </div>
                  <hr class="sidebar-divider">
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                        <label>Nama Pembeli :</label>
                        <h5><?php echo $Nama ?></h5>
                    </div>
                    <div class="col-6">
                      <label>Ekpedisi :</label>
                      <h5><?php echo $Ekspedisi ?></h5>
                    </div>
                  </div>
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <div class="row">
                    <div class="col-12">
                      <label>Alamat :</label>
                      <h5><?php echo $Address ?></h5>
                    </div>
                  </div>
                </div> 
                <hr class="sidebar-divider">
                <div class="row justify-content-md-center">
                  <div class="col col-lg-2">
                  </div>
                  <div class="col-md-auto">
                    <a href="<?php echo site_url('Data_Penjualan/Cetak_Invoice/').$IDTransaksi?>" type="button" class="btn btn-success" class="text-center">Cetak</a>
                  </div>
                  <div class="col col-lg-2">
                  </div>
              </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              
              <div class="card-body">
              <table class="table table-bordered table-striped">
                  <thead>
                  <tr style="text-align:center">
                    <th>No.</th>
                    <th>Produk</th>
                    <th>Total Produk Terjual</th>
                    <th>Harga Produk</th>
                    <th>Total Penjualan</th>
                    <th style="text-align:center">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                    $no = 1;
                    $Total_Harga = 0;
                    foreach($transaksi as $u){
                    ?>
                  <tr class="text-center">
                    <th><?php echo $no++?></th>
                    <th class="text-left"><?php echo $u->ProductName?></th>
                    <th><?php echo $u->Quantity?></th>
                    <th style="text-align:left">Rp. <?php echo number_format($u->Price,0,',','.') ?></th>
                    <th style="text-align:left">Rp. <?php echo number_format($u->Total_Price,0,',','.') ?></th>
                    <th>
                    <a 
                      href="javascript:;"
                      data-id="<?php echo $u->ID ?>"
                      data-idproduct="<?php echo $u->IDProduct ?>"
                      data-idpenjualan="<?php echo $u->IDTransaction ?>"
                      data-nama="<?php echo $u->ProductName ?>"
                      data-total_produk="<?php echo $u->Quantity ?>"
                      data-harga="<?php echo $u->Price ?>"
                      data-toggle="modal" data-target="#edit-data">
                      <button  data-toggle="modal" data-target="#ubah-data" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i>Ubah</button>
                    </a>
                      <!-- <a href="<?php echo site_url('Data_Penjualan/EditProductTransaction/'.$u->ID);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i>Ubah</a> -->
                      <a href="<?php echo site_url('Data_Penjualan/ProsesHapusDataPenjualanDetail/'.$u->ID);?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin data yang dihapus sudah benar ?')"><i class="fas fa fa-trash"></i> Hapus </a>
                    </th>
                  </tr>
                  <tr>
                    <?php $Total_Harga = $Total_Harga + $u->Total_Price;
                     } ?>
                    <th colspan="4" style="text-align:center">Total Harga</th>
                    <th style="text-align:left">Rp. <?php echo number_format($Total_Harga,0,',','.') ?></th>
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="edit-data" aria-labelledby="myModalLabel" role="dialog" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ubah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form class="form-horizontal" action="<?php echo site_url('Data_Penjualan/ProsesEditDataPenjualan/'); ?>" method="post" id="formpenjualan1">
          <div class="modal-body">
            <input type="hidden" name="id" id="id" class="form-control">
            <input type="hidden" name="idproduct" id="idproduct" class="form-control">
            <input type="hidden" name="idpenjualan" id="idpenjualan" class="form-control">
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" id="nama" class="form-control" readonly>
            </div>
            <div class="form-group">
              <label>Total Produk</label>
              <input type="number" name="qty" id="total_produk" class="form-control" min="0" required>
            </div>
            <div class="form-group">
              <label>Harga</label>
              <input type="number" name="harga" id="harga" id="masking1" class="form-control" max="200000" min="100000" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" required>  
            </div>
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" form="formpenjualan1" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Edit Data Transaksi</button>
        </div>
      </div>
          <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->

  <div class="modal fade" id="moda">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi Hapus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <label>Hapus Data ?</label>
        </div>
        <div class="modal-footer justify-content-between">
          <a type="button" href="<?php echo site_url('Data_Penjualan/ProsesHapusDataPenjualanDetail/'.$u->ID);?>" class="btn btn-primary" style="width:75px">Ya</a>
          <button type="button" class="btn btn-danger" data-dismiss="modal" style="width:75px">Tidak</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
