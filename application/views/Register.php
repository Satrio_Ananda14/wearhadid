<!doctype html>
<html lang="en">
  <head>
  	<title>Daftar</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="<?php echo base_url('assets/css/style-login.css')?>" rel="stylesheet">

	</head>
	<body style="background:url('<?php echo base_url('assets/dist/img/batu.jpg')?>'); background-size: 100%;">
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
			</div>
			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-10">
					<div class="wrap d-md-flex">
						<div class="text-wrap p-4 p-lg-5 text-center d-flex align-items-center order-md-last" style="background-image:url('<?php echo base_url('assets/dist/img/wh5.jpg')?>'); background-size: 200%;">
							<div class="text w-100">
								<h2>Selamat Datang di Wearhadid</h2>
								<p>Sudah punya akun?</p>
								<a href="<?php echo site_url('Login')?>" class="btn btn-white btn-outline-white">Login</a>
							</div>
			      	</div>
						<div class="login-wrap p-4 p-lg-5">
			      	<div class="d-flex">
			      		<div class="w-100">
			      			<h3 class="mb-4">Daftar</h3>
			      		</div>
			      	</div>
					<form action="<?php echo site_url('Login/ProsesDaftar');?>" class="signin-form" method="post" >
			      		<div class="form-group mb-3">
			      			<label class="label">Username</label>
			      			<input type="text" name="usernama" class="form-control" placeholder="Username" required>
			      		</div>
						<div class="form-group mb-3">
							<label class="label">Password</label>
							<input type="password" name="password" class="form-control" placeholder="Password" required>
						</div>
						<div class="form-group mb-3">
							<label class="label">Konfirmasi Password</label>
							<input type="password" name= "validasi_pass" class="form-control" placeholder="Konfirmasi Password" required>
						</div>
						<div class="form-group mb-3">
							<label class="label">Nama</label>
							<input type="text" name= "nama" class="form-control" placeholder="Nama" required>
						</div>
						<div class="form-group mb-3">
							<label class="label">Jenis Kelamin</label>
							<select name="jeniskelamin" class="form-control" required>
							<option value="L">Laki-Laki</option>
							<option value="P">Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label>No. Telepon</label>
							<input type="number" name="telfon" class="form-control" placeholder="Nomor Telfon" required>
						</div>
						<div class="form-group mb-3">
							<label class="label">Email</label>
							<input type="email" name= "email" class="form-control" placeholder="Email" required>
						</div>
						<div class="form-group">
							<button type="submit" class="form-control btn submit px-3  text-light" style="background-color: #3F4244;">Daftar</button>
						</div>
		      		</div>
				</div>
			</div>
		</div>
	</section>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/main.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/popper.js')?>"></script>

	</body>
</html>

