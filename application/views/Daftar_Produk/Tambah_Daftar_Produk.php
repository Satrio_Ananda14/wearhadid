<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Produk Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daftar Produk</a></li>
              <li class="breadcrumb-item active">Tambah Produk Penjualan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Tambah Produk Penjualan</h6>
          </div>
            <div class="card-body">
            <?php foreach($produk as $u){ ?>
              <?php echo form_open_multipart('Daftar_Produk/ProsesTambahDaftarProduk/'.$u->IDProduct)?>
                <div class="form-group">
                  <label>Nama Produk</label>
                  <input type="text" name="nama" class="form-control" value="<?php echo $u->ProductName; ?>" readonly>
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <label>Kategori</label>
                  <input type="text" name="kategori" class="form-control" value="<?php echo $u->category; ?>" readonly> 
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <label>Jumlah Barang</label>
                  <input type="Number" name="stok" value="0" max="7500" min="0" class="form-control" required>
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <label>Harga Beli (Rp)</label>
                  <h5>Rp. <?php echo number_format($u->Price,0,',','.') ?></h5>
                </div>
                <hr class="sidebar-divider">
                <div class="form-group">
                  <label>Harga Jual (Rp)</label>
                  <input type="number" name="harga" id="masking1" max="200" min="100" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control" value="<?php echo $u->Price; ?>" required>
                </div>
                <?php }?>
                <hr class="sidebar-divider">
                    <a href="<?php echo site_url('Daftar_Produk/Daftar_Produk/')?>" class="btn btn-light btn-icon-split" style="float: left;">
                    <span class="icon text-gray-600">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                    </a>
                    <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin menambah data produk pada daftar ?')">Simpan</button>
              <?php echo form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>