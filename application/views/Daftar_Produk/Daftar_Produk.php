<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Daftar Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daftar Produk</a></li>
              <li class="breadcrumb-item active">Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="max-width:90%;margin-left:3rem;">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i>  Produk Wearhadid</h3>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                    <th>Aksi</ th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    $no = 1;
                    foreach($produk as $u){ 
                  ?>
                  <tr>
                    <th><?php echo $no++ ?></th>
                    <th><?php echo $u->ProductName?></th> 
                    <th><?php echo $u->category ?></th>
                    <th>Rp. <?php echo number_format($u->Price,0,',','.') ?></th>
                    <th class="justify-content-center">
                      <a href="<?php echo site_url('Daftar_Produk/TambahDaftarProduk/'.$u->IDProduct);?>" class="btn btn-success btn-sm" ><i class="fas fa fa-plus"></i>Tambah</a>
                    </th>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="max-width:90%;margin-left:3rem;">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Daftar Produk Reseller</h3>
                </div>
              <div class="card-body">
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                        $no = 1;
                        foreach($daftarproduk as $s){ 
                    ?>
                  <tr>
                    <th><?php echo $no++ ?></th>
                    <th><?php echo $s->ProductName ?></th>
                    <th><?php echo $s->category ?></th>
                    <th><?php echo $s->Stock ?></th>
                    <th>Rp. <?php echo number_format($s->Price,0,',','.') ?></th>
                    <th>
                        <a href="<?php echo site_url('Daftar_Produk/EditDaftarProduk/'.$s->No);?>" class="btn btn-primary btn-sm" ><i class="fas fa fa-eye"></i> Edit</a>
                        <a href="<?php echo site_url('Daftar_Produk/ProsesHapusDaftarProduk/'.$s->No);?>" class="btn btn-danger btn-sm justify-content-center" onclick="return confirm('Apakah Anda yakin data yang dihapus sudah benar ?')"><i class="fas fa fa-trash"></i>Hapus</a>
                    </th>
                  </tr>
                  <?php ;}?>
                  <?php  ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>