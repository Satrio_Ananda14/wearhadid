<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kelola List Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Kelola List Product</li>
              <li class="breadcrumb-item active">Kelola List Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Kelola List Product</h6>
          </div>
            <div class="card-body">
            <?php foreach($daftarproduk as $u){ ?>
              <form action="<?php echo site_url('Daftar_Produk/ProsesEditDaftarProduk/'), $u->No; ?>" method="post">
                <div class="form-group">
                  <label>Nama Produk</label>
                  <input type="text" name="nama" class="form-control" value="<?php echo $u->ProductName; ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Kategori</label>
                  <input type="text" name="kategori" class="form-control" value="<?php echo $u->category; ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Stok Tersedia</label>
                  <input type="Number" name="stok" class="form-control" min=0 value="<?php echo $u->Stock; ?>" required>
                </div>
                <div class="form-group">
                  <label>Harga (Rp)</label>
                  <input type="number" name="harga" id="masking1" max="200" min="100" oninvalid="setCustomValidity('Harga Harus Antara Rp. 100.000 sampai Rp. 200.000')" oninput="setCustomValidity('')" class="form-control" value="<?php echo $u->Price; ?>" required>
                </div>
                <div class="col-12">
                      <p style="text-align:justify; Color:Red;"><?php echo $this->session->userdata("error"); ?></label>
                    </div>
                <?php }?>
                  <a href="<?php echo site_url('Kelola_Produk/DetailProduk/');?>" class="btn btn-light btn-icon-split" style="float: left;">
                  <span class="icon text-gray-600">
                      <i class="fas fa-arrow-left"></i>
                  </span>
                  <span class="text">Kembali</span>
                  </a>
                  <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>