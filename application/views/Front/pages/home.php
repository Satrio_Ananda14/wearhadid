<div class="page-header"></div>
<!-- Slider/ Carousel -->
<!-- <div class="col-md-auto h-auto ">   -->
<div id="myCarousel" class="carousel slide">
      <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" data-interval="50">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <?php foreach($Slider as $u){
              if($u->Status == "Utama"){
              ?>
              <img src="<?php echo base_url('assets/dist/img/'),$u->ImageSource ?>" class="d-block img-fluid" alt="...">
            <!-- Carousel Caption -->
            <div class="carousel-caption d-flex h-100 align-items-center justify-content-center">
              <div class="title font-weight-light" style="font-size:12vw; line-height: 0.2em; margin-bottom: flex;" >WEARHADID</div>
              <br>
            </div>
            <?php }
            } ?>
          </div>
          <?php foreach($Slider as $u){ 
            if($u->Status == "Lainnya"){?>
            <div class="carousel-item">
              <img src="<?php echo base_url('assets/dist/img/'),$u->ImageSource ?>" class="d-block img-fluid" alt="...">
            </div>
          <?php }
          }?>
        </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
