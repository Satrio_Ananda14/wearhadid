<html>
    <head>
     <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    </head>
    <body style="background:url('<?php echo base_url('assets/dist/img/wh6.jpeg ')?>') no-repeat; background-size: cover" class="w-100 h-100vh">
    <div class="container rounded bg-light mt-3 mb-auto">
        <div class="p-2">
            <div class="row">
                <div class="col-md-8 cart" id="crt">
                    <div class="title">
                        <div class="row">
                            <div class="col">
                                <h4><b>Shopping Cart</b></h4>
                            </div>
                            <?php $no=0; foreach($cart as $u){$no++;}?>
                            <div class="col"><h5><?php echo $no?> items</h5></div>
                        </div>
                    </div>
                        
                    <?php 
                        $total_harga_cart=0;
                        $no=0;
                        foreach($cart as $u){ 
                            $no++;?>
                    <div class="row" style="margin:10px">
                        <div class="row main align-items-center">
                                <div class="col-2"><img class="img-fluid" src="<?php echo base_url('assets/dist/img/').$u->img ?>"></div>
                                <div class="col">
                                    <div class="row text-muted">Shirt</div>
                                    <div class="row"><?php echo $u->Name ?></div>
                                </div>
                                <form id="frm<?php echo $u->IDCart?>">
                                    <div style="margin-top:20px">
                                        <input type="hidden" name="cartid" value="<?php echo $u->IDCart;?>">
                                        <div class="col"> QTY : <input class="text-center" type="number" name="qty" class="form-control" value="<?php echo $u->Qty; ?>" onchange="updcart(<?php echo $u->IDCart; ?>)" onkeyup="updcart(<?php echo $u->IDCart; ?>)" style="width:50px"> </div>
                                    </div>
                                </form>
                                <?php 
                                    $qty = $u->Qty; 
                                    $harga = $u->Price;
                                    $total_harga = $qty * $harga;
                                    $total_harga_cart = $total_harga_cart + $total_harga;
                                ?>
                                <div class="col">Rp. <?php echo number_format($total_harga,0,',','.')?> <a href="<?php echo site_url('Cart/delete/').$u->IDCart; ?>"> <span class="close" style="margin-right:25px">&#10005;</span></a></div>
                            </div> 
                        </div>
                        <?php } ?>
                        <!-- </form> -->
                    </div>
                    <div class="col-md-4 summary">
                        <div>
                            <h5><b>Summary</b></h5>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col" style="margin-left:1px;">ITEMS <?php echo $no ?></div>
                        </div>
                        <?php
                        $total_seluruhnya=0;
                        foreach($cart as $u){
                        $total_harga_cart=0;?>
                        <div class="row">
                            <div class="col" style="margin-left:1px;">- <?php echo $u->Name?></div>
                            <?php 
                                    $qty = $u->Qty; 
                                    $harga = $u->Price;
                                    $total_harga = $qty * $harga;
                                    $total_harga_cart = $total_harga_cart + $total_harga;
                                    $total_seluruhnya = $total_seluruhnya + $total_harga_cart;
                            ?>
                            <div class="col text-right">Rp. <?php echo  number_format($total_harga_cart,0,',','.') ?></div>
                        </div>
                        <?php }?>
                       
                        <div class="row" style="border-top: 1px solid rgba(0,0,0,.1); padding: 2vh 0;">
                            <div class="col">TOTAL PRICE</div>
                            <div class="col text-right" id="update">Rp. <?php echo  number_format($total_seluruhnya,0,',','.') ?></div>
                        </div>
                    <!-- </div> -->
                <!-- </div>
                <div class="row"> -->
                    <!-- <div class="col-md-12">
                        <div class="row" style=" border-top: 1px solid rgba(0,0,0,.1);">
                            <h5 align="justify" style="margin-top:10px; margin-right:15px;">
                                Silakan untuk melakukan Transfer Ke Nomor Rekening Bank. MANDIRI, Dengan : <br><br> Nomor Rek. 1320005708398 <br>A.T Satrio Ananda Setiawan. <br><br>
                                Dengan Nominal Sebesar Rp. <?php echo  number_format($total_seluruhnya,0,',','.') ?>. Biaya Ongkos Kirim Akan Ditanggu Pembeli Dengan Sistem COD (Untuk Biaya Pengiriman).
                                Bukti Pembayaran Dapat Dikirimkan Melalui Link yang dikirim melalui email.
                            </h5>
                        </div>
                    </div> -->
                    <!-- <div class="col-md-4 summary"> -->
                        <?php foreach($member as $s){
                            $Address = $s->Address;
                            $Postal_Code = $s->Postal_Code;
                            $Detail_Address = $s->Detail_Address;
                        }?>

                        <form action="<?php echo site_url('Cart/Checkout'); ?>" method="post" id="form1">
                            <div class="row" style="border-top: 1px solid rgba(0,0,0,.1);">
                                <p style="margin-bottom:0px;">Address</p> 
                            </div>
                            <div class="row">
                                <textarea class="form-control" name="address" rows="2" placeholder="Address" required><?php echo $Address?></textarea>
                            </div>
                            <div class="row">
                                <p style="margin-bottom:0px;">Detail Address</p>
                            </div>
                            <div class="row my-1">
                                <textarea class="form-control" name="detailaddress" rows="2" placeholder="Block, Floor. ex. Block 1, Second Floor, No.2" required><?php echo $Detail_Address?></textarea>
                            </div>
                            <div class="row">
                                <p style="margin-bottom:0px;">Province</p>
                            </div>
                            <div class="row my-1">
                                <input type="text" class="form-control" name="provinsi"  placeholder="ex. Jawa Barat"  required>
                            </div>
                            <div class="row my-1">
                                <p style="margin-bottom:0px;">City</p>
                            </div>
                            <div class="row my-1">
                                <input type="text" class="form-control" name="kota"  placeholder="ex. Kota Cimahi" required>
                            </div>
                            <div class="row my-1">
                                <p style="margin-bottom:0px;">Districts</p>
                            </div>
                            <div class="row my-1">
                                <input type="text" class="form-control" name="kecamatan" placeholder="ex. Cimahi Utara" required>
                            </div>
                            
                            <div class="row">
                                <p style="margin-bottom:0px;">Postal Code</p>
                            </div>
                            <div class="row">
                                <input type="text" class="form-control" name="postalcode" placeholder="Postal Code" value="<?php echo $Postal_Code?>" required>
                            </div>
                        </form>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <a href="<?php echo site_url('Front'); ?>" type="button" class="btn btn-danger">Back</a>
              <!-- <button type="submit" id="<?php echo $u->IDMember?>" class="detail btn btn-primary"> Check Out</button> -->
              <div type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">Check Out</button></div>
              <!-- <button type="submit" class="btn btn-primary" form="form1" class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Check Out</button> -->
            </div>
            
        </div>
    </div>
</body>


<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Checkout</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <label>Order Akan Tersimpan, Mohon Tunggu Konfirmasi dari Email Untuk Kalkulasi Total Biaya Pengiriman </label>
          
          <label><br>Terima Kasih</label>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" form="form1" onclick="return confirm('Apakah Anda yakin checkout sudah benar?')">Check Out</button>
        </div>
      </div>
          <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()?>assets/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- Bootstrap CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>		
	<script src="<?php echo base_url()?>assets/bantuan/jquery.flipster.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   

<script>
$(document).ready(function(){
    $('.detail').click(function(){
      var idorder = $(this).attr("id");
      $.ajax({
          url: '<?php echo base_url(); ?>/Cart/TabelCheckout',
          method: 'post',
          data: {idorder : idorder},
          success:function(data){
              $('#myModal').modal("show");
              $('#tampil_modal').html(data);  
          }
      });
    });
});
</script>
    
<script>
    function updcart(id){
        $.ajax({
            url:'<?php echo base_url(); ?>/Cart/update',
            type:'POST',
            data:$("#frm"+id).serialize(),
            success:function(res){
                // $('#modal-xl').modal("show");
                location.reload();
            }
        });
    }
</script>

<html>