<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wearhadid</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bantuan/jarallax.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bantuan/style.css">
	<style>
		.btn3
		{
			padding: 5px 20px;
			margin: 5px;
			font-size: 16px;
			letter-spacing: 1px;
			border: 2px solid #787777;
			color: #fff;
			cursor: pointer;
			background: linear-gradient(to right, transparent 50%, #787777 50%);
			background-size: 200%;
			background-position: left;
			transition: background-position 0.5s;
		}
	</style>
</head>
<body id="slider2" class="slide-in">
	<div>
	<section>
		<div class="box">
			<h2 data-jarallax-element="0 -200">Premium Cotton Goods</h2>
			<div class="container">
				<div class="imgBx jarallax">
					<img src="<?php echo base_url('assets/dist/img/wh6.jpeg')?>" class="jarallax-img">
				</div>
				<div class="content" data-jarallax-element="-200 0">
					<p>Finest quality with favorable price
					</p>
				</div>
			</div>
		</div>
		<a href="#details" class="box2">
				<span></span>
				<span></span>
				<span></span>
		</a>
	</section>
	<?php foreach($produk as $u){
		$Product = $u->ProductName;
		$desription = $u->Description;
	}?>

	<section id="details">
		<div class="box">
			<h2 data-jarallax-element="0 200"><?php echo $Product?></h2>
			<div class="container">
				<div class="imgBx jarallax">
					<img src="<?php echo base_url('assets/dist/img/').$u->ImageSource?>" class="jarallax-img">
				</div>
				<div class="content" data-jarallax-element="-200 0">
					<p><?php echo $desription?></p>
					<br>
					<?php 
					// $ID=$_SESSION["id"];
					// $arr = str_split($ID);
					// //Jika Admin atau Reseller Login tidak menunjukan Tombol add to cart
					// if($arr[0]!='A' || $arr[0]!='R'){
					
					// }
					// // jika Member tombol add to cart ditambahkan
					// else if($arr[0]!='M')
					// {
					?>
						<a href="<?php echo site_url("Cart/Add_Cart/").$u->IDProduct;?>" type="button" class="btn3 text-center">Add To Cart</a>
					<?php// }?>
					<br>
					<br>
					<p style="text-align:justify; Color:Red;"><?php echo $this->session->userdata("error"); ?></p>
				</div>
			</div>
		</div>
	</section>

	<script src="<?php echo base_url(); ?>assets/bantuan/jarallax.js"></script>
	<script type="<?php echo base_url(); ?>assets/bantuan/jarallax-element.js"></script>
	<script type="text/javascript">
		jarallax(document.querySelectorAll('.jarallax'), {
		    speed: 0.5
		});

	var mainHeadings = document.getElementById("main-headings-js");
  
  	mainHeadings.style.transform = "scale(0.7)";
	</script>
	<script>
		var $slider = document.getElementById('slider');
		var $toggle = document.getElementById('toggle');

		$toggle.addEventListener('click', function() {
		    var isOpen = $slider.classList.contains('slide-in');

		    $slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
		});
	</script>
</body>
</html>