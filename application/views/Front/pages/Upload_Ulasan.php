<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  </head>
  <body style="size:1040px;height:1040px;background:url('../assets/img/images/Home/wh6.jpeg') no-repeat; background-size: cover">
      <!-- DataTable with Hover -->
      <div class="card mb-4" style="max-width:75%;margin-left:15rem; margin-top:10rem;">
          <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Kofirmasi Barang Yang Sudah Sampai</h3>
                </div>
              </div>
              <?php echo form_open_multipart('Cart/proses_upload_ulasan/'.$id)?>
                <div class="form-group">
                  <label>Upload Barang Sudah Sampai</label>
                  <input type="file" name="gambar" class="form-control">  
                </div>
                <div class="d-flex justify-content-center">
                  <p style="text-align:justify; Color:Red;"><?php echo $this->session->userdata("error"); ?></p>
                </div>
                <div class="form-group">
                  <label>Ulasan</label>
                  <textarea class="form-control" name="ulasan" rows="2" placeholder="Ulasan" required></textarea>
                </div>
                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary btn-sm " class="text-center" onclick="return confirm('Apakah Anda yakin bukti transfer sudah benar?')">Upload</button>
                </div>
                </form>
              <?php echo form_close() ?>
      </div>
  </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>