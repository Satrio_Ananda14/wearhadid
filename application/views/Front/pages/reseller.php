      <style>      
            .wrapper{
			width: 100%;
			height: 100vh;
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.card2{
			background: #2a2a2a;
			width: 320px;
			height: 480px;
			margin: 10px;
			box-shadow: 0 0 5px #2a2a2a;
			border-radius: 20px;
			position: relative;
			overflow: hidden;
			cursor: pointer;
			text-align: center !important;
		}

		.card2::before{
			/*background: #1f627f;*/
			background-image: url("<?= base_url(); ?>assets/dist/img/wh11.png");
                  background-position: left;
		/*	opacity: 0.9;*/
			position: absolute;
			width: 100%;
			height: 100%;
			content: "";
			top: -60%;
                  left: 0%;
			transform: skewY(345deg);
			transition: 1s ease-in;
		}
		.card2:hover::before{
			top: -60%;
/*			transform: scaleX(-1);*/
			transform: skewY(375deg);
                  align:left;
		}
		.card2:hover .image2 img{
			width: 80%;
		}
		.about-product2{
			position: absolute;
			bottom: -50px !important;
			left: 20px;
			color: #fff;
			text-align: center;
			transition: 1s ease-in;
		}
		.card2:hover .about-product2{
			bottom: 5px !important;
		}
		.button{
			color: #fff;
			width: 150px;
			background: #000000 !important;
			border-color: #000000 !important;
			margin-top: 30px;
		}
		.about-product2 h3{
			margin-left: 25px;
		}
      </style>
      <div class="container-fluid" id="Reseller">      
            <div class="wrapper">
                  <div class="card2 card-fluid text-center w-100">
				<!-- <div class="image">
					<img src="img/Transparent/Black/1-tp.png" alt="" width=300>
				</div> -->
				<div class="about-product2 text-center">
					<h3>Join With Us</h3>
					<h6>Registration <strong>Free</strong></h6>
					<button class="btn btn-success button">Join Here</button>
				</div>
			</div>
		</div>
	</div>
    