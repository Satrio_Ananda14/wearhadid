<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<link rel="stylesheet" href="style.css">
	<style>
		table {
			border-collapse:collapse;
			table-layout:fixed;
			width: 800px;
			font-family: arial;
      		color:#5E5B5C;
		}
		table th{
			font-family: Helvetica;
  			color: white;
  			text-align: Center;
  			font-size: 14px;
  			height: 30px;
		}
		table td {
			word-wrap:break-word;
			width: 21%;
			text-align: center;
			height : 30px;
		}
		.h1{
			font-family: Calibri;
			font-size : 26px;
		}
		.h4{
			font-family: Calibri;
			font-size : 12px;
			height : 12px;
		}

		
	</style>
</head>
<body>
<center>
<h2 text-align="center" ><B>Wearhadid</B></h2>
<div text-align="center">0858-6247-0796</div>
<div text-align="center">--INVOICE--</div>
<div>----------------------------------------------------------------------------------------------------------------------------------------------------</div>
<?php foreach($checkout as $u){
     $IDTransaksi = $u->IDOrder;
     $Tanggal = $u->Date;
     $Nama = $u->MName;
     $Address = $u->Address;
     $Detail_Address = $u->Detail_Address;
     $Postal_Code = $u->Postal_Code;
    }?>
<table>
		<tr>
			<td text-align="left"> No Transaksi : </td>
            <td style="text-align:left;"> <?php echo $IDTransaksi ?> </td>
			<td text-align="left"> Tanggal : </td>
			<td  style="text-align:left;"> <?php echo $Tanggal ?> </td>
		</tr>
		<tr>
			<br>
		</tr>
		<tr>
			<td class="text-left"> Nama : </td>
			<td style="text-align:left;"> <?php echo $Nama ?> </td>
			<td class="text-left"> Alamat : </td>
			<td style="text-align:left; width:20px"><p style="text-align:left; width:150px"><?php echo $Address ?>/<p></td>
		</tr>
        <tr>
            <td class="text-left">Detail Alamat : </td>
			<td style="text-align:left; "> <?php echo $Detail_Address ?> </td>
			<td class="text-left">Kodepos : </td>
			<td style="text-align:left;"> <?php echo $Postal_Code ?> </td>
		</tr>
</table>
<div>----------------------------------------------------------------------------------------------------------------------------------------------------</div>
<table class="33" width="700" border="0" cellpadding="2" cellspacing="1" align="center">
<tr>
	<td><b>Barang</b></td>
	<td><b>QTY</b></td>
	<td><b>Harga</b></td>
	<td><b>Total Harga</b> </td>
</tr>

<?php 
	$Total_Harga=0;
	foreach($checkout as $u) {?>
	<tr>
        <td class="text-left"><?php echo $u->Name?></th>
    	<td><?php echo $u->Qty?></th>
        <td>Rp. <?php echo number_format($u->PriceProduct,0,',','.') ?></th>
        <td>Rp. <?php echo number_format($u->Total_Price,0,',','.') ?></th>
<?php 
		$Total_Harga = $Total_Harga + $u->Total_Price;
		} ?>

</table>
<script type="text/javascript">
		window.print();
</script>
<div>---------------------------------------------------------------------------------------------------------------------------------------------------</div>
<table class="33" width="700" border="0" cellpadding="2" cellspacing="1" align="center">
		 <tr>
			<td><b>Total Harga</b></td>
			<td></td>
			<td></td>
			<td><b>Rp. <?php echo number_format($Total_Harga,0,',','.') ?></b></td>
		</tr>
</table>
<div>----------------------------------------------------------------------------------------------------------------------------------------------------</div>
<div text-align="center">PEMBAYARAN DAPAT MELALUI BANK MANDIRI</div>
<div text-align="center">DENGAN NO. REKENING : 1320005708398</div>
<div text-align="center">A.T SATRIO ANANDA SETIAWAN</div>
<div text-align="center">BUKTI PEMBAYARAN DAPAT DIUNGGAH MELALUI</div>
<div text-align="center">LINK YANG DISEDIAKAN PADA EMAIL</div>
<div>----------------------------------------------------------------------------------------------------------------------------------------------------</div>
<div text-align="center">TERIMA KASIH</div>
<div text-align="center">TELAH MEMBELI PRODUK WEARHADID</div>
<div text-align="center">LAYANAN KONSUMEN 0858-6247-0796</div>
</center>
</body>
</html>
