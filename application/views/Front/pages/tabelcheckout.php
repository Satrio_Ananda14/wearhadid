<html>
    <head>
     <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
<body style="background:url('<?php echo base_url('assets/img/images/Home/wh6.jpeg') ?>') no-repeat; background-size: cover" class="w-100 h-100vh">
    <div class="container rounded bg-light mt-3 mb-auto">
        <div class="p-2">
            <div class="row">
                <div class="col-md-8 cart" id="crt" style="margin-bottom:10px">
                    <div class="title">
                        <div class="row">
                            <div class="col">
                                <h4><b>Checkout</b></h4>
                            </div>
                            <?php $no=0; foreach($cart as $u){$no++;}?>
                            <div class="col"><h5><?php echo $no?> items</h5></div>
                        </div>
                    </div>
                        
                    <?php 
                        $total_harga_cart=0;
                        $no=0;
                        foreach($cart as $u){ 
                            $no++;?>
                    <div class="row" style="margin:10px">
                        <div class="row main align-items-center">
                                <div class="col-2"><img class="img-fluid" src="<?php echo base_url('assets/dist/img/').$u->img ?>"></div>
                                <div class="col">
                                    <div class="row text-muted">Shirt</div>
                                    <div class="row"><?php echo $u->Name ?></div>
                                </div>
                                <div class="col"> QTY : <?php echo $u->Qty; ?> </div>
                                <div class="col">Rp. <?php echo number_format($u->Total_Price,0,',','.')?></div>
                            </div> 
                        </div>
                        <?php } ?>
                        <!-- </form> -->
                    </div>
                    
                    <div class="col-md-4 summary">
                        <div>
                            <h5><b>Summary</b></h5>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col" style="margin-left:1px;">ITEMS <?php echo $no ?></div>
                        </div>
                        <?php
                        $total_seluruhnya=0;
                        foreach($cart as $u){
                            $IDTransaksi = $u->IDOrder;
                            $Tanggal = $u->Date;
                            $IDMember = $u->IDMember;
                            $Nama = $u->MName;
                            $Address = $u->Address;
                            $Detail_Address = $u->Detail_Address;
                            $Postal_Code= $u->Postal_Code;
                            $total_seluruhnya= $total_seluruhnya + $u->Total_Price;
                            $total_harga_cart=0;?>
                        <div class="row">
                            <div class="col" style="margin-left:1px;">- <?php echo $u->Name?></div>
                            <div class="col text-right">Rp. <?php echo  number_format($u->Total_Price,0,',','.') ?></div>
                        </div>
                        <?php }?>
                        <div class="row" style="border-top: 1px solid rgba(0,0,0,.1); padding: 2vh 0;">
                            <div class="col">TOTAL PRICE</div>
                            <div class="col text-right" id="update">Rp. <?php echo  number_format($total_seluruhnya,0,',','.') ?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row" style=" border-top: 1px solid rgba(0,0,0,.1);">
                            <h5 align="justify" style="margin-top:10px; margin-right:15px;">
                                Silakan untuk melakukan Transfer Ke Nomor Rekening Bank. MANDIRI, Dengan : <br><br> Nomor Rek. 1320005708398 <br>A.T Satrio Ananda Setiawan. <br><br>
                                Dengan Nominal Sebesar Rp. <?php echo  number_format($total_seluruhnya,0,',','.') ?>. Biaya Ongkos Kirim Akan Ditanggu Pembeli Dengan Sistem COD (Untuk Biaya Pengiriman).
                                Bukti Pembayaran Dapat Dikirimkan Melalui Link yang dikirim melalui email.
                            </h5>
                        </div>
                    </div>
                    <div class="col-md-4 summary" style="border-left: 1px solid rgba(0,0,0,.1);">
                        <div class="row" style=" border-top: 1px solid rgba(0,0,0,.1);">
                            <p style="margin-bottom:0px; margin-left:25px; margin-right:20px; margin-top:10px;">No Transaksi :</p> 
                        <!-- </div>
                        <div class="row"> -->
                            <P style="margin-top:10px;"> <?php echo $IDTransaksi ?></p>
                            <!-- <textarea class="form-control" name="address" rows="2" placeholder="Address" required></textarea> -->
                        </div>
                        <div class="row">
                            <p style="margin-bottom:0px; margin-left:25px; margin-right:20px; margin-top:10px;">Nama Pembeli :</p> 
                        <!-- </div>
                        <div class="row"> -->
                            <p style="margin-top:10px;"> <?php echo $Nama ?></p>
                            <!-- <textarea class="form-control" name="address" rows="2" placeholder="Address" required></textarea> -->
                        </div>
                        <div class="row">
                            <p style="margin-bottom:0px; margin-left:25px; margin-right:20px; margin-top:10px;">Address :</p> 
                        <!-- </div>
                        <div class="row"> -->
                            <p style="margin-top:10px;"> <?php echo $u->Address ?></p>
                            <!-- <textarea class="form-control" name="address" rows="2" placeholder="Address" required></textarea> -->
                        </div>
                        <div class="row">
                            <p style="margin-bottom:0px; margin-left:25px; margin-right:20px; margin-top:10px;">Detail Address :</p>
                        <!-- </div>
                        <div class="row my-1"> -->
                            <p style="margin-top:10px;"> <?php echo $u->Detail_Address ?></p>
                            <!-- <textarea class="form-control" name="detailaddress" rows="2" placeholder="Block, Floor. ex. Block 1, Second Floor, No.2" required></textarea> -->
                        </div>
                        <div class="row">
                            <p style="margin-bottom:0px; margin-left:25px; margin-right:20px; margin-top:10px;">Postal Code :</p>
                        <!-- </div>
                        <div class="row"> -->
                            <p style="margin-top:10px;"> <?php echo $u->Postal_Code ?></p>
                            <!-- <input type="text" class="form-control" name="postalcode" style="width:30%;" placeholder="Postal Code" required> -->
                        </div>
                    </div>
                </div>
            <div class="modal-footer justify-content-between">
                <a href="<?php echo site_url('Front'); ?>" type="button" class="btn btn-danger">Back</a>
                <a href="<?php echo site_url('Cart/cetak_invoice/'.$IDTransaksi); ?>" class="detail btn btn-primary"> Print </a>
              <!-- <button type="submit" class="btn btn-primary" form="form1" class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Check Out</button> -->
            </div>
        </div>
    </div>
</body>