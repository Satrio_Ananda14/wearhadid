<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  </head>
  <body style="background:url('<?php echo base_url('assets/dist/img/wh6.jpeg ')?>') no-repeat; background-size: cover" class="w-100 h-100vh">
  <div class="container rounded bg-light mt-3 mb-auto">
    <!-- <div class="row"> -->
    <form action="<?php echo site_url('Front/ProsesRegistrasiReseller'); ?>" method="post" id="formregister">
            <div class="p-2">
                <div class="d-flex justify-content-center align-items-center mb-2">
                    <h3 class="text-center">Join Reseller Form</h3>
                </div>
                <div class="row mt-2">
                    <input type="hidden" name="id" value="<?php echo $ID?>">
                    <div class="col-md-6">
                      <label class="labels">Name</label>
                      <input type="text" class="form-control only-string" placeholder="Name" name="nama" maxlength="25" size="10" required>
                    </div>
                    <div class="col-md-6">
                      <label class="labels">Username</label>
                      <input type="text" class="form-control" placeholder="Username" name="usernama" id="un" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                      <label class="labels">Password</label>
                      <input type="Password" class="form-control" name='password' id="pass" required>
                    </div>
                    <div class="col-md-6">
                      <label class="labels">Konfirmasi Password</label>
                      <input type="Password" class="form-control" id="rpt" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6 wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                      <label class="labels">Email</label>
                      <input type="email" class="form-control" placeholder="Email" name="email" required>
                    </div>
                    <div class="col-md-6"><label class="labels"><label>Gender</label>
                      <select name="jeniskelamin" class="form-control" required>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                      </select></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12"><label class="labels">Phone</label>
                      <input type="text" class="form-control" placeholder="Phone Number" name="telfon" maxlength="12" minlength="10" id="telpon" required>
                    </div>
                    <div class="col-md-12">
                      <label class="labels">Adress</label>
                      <Textarea type="text" class="form-control" placeholder="Adress" name="alamat" required></Textarea>
                    </div>
                </div>
                <div type="button" class="btn btn-outline-dark btn-lg btn-block mt-4 mb-3" data-toggle="modal" data-target="#modal-default">Register</button></div>
                <!-- <div class="btn btn-outline-dark btn-lg btn-block mt-4 mb- 3" type="button">Save Profile</button></div> -->
          </div>
      <!-- </div> -->
    </form>
  </div>



  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Registrasi Reseller</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <label>Account yang dibuat tidak dapat langsung digunakan. Silakan Tunggu Konfirmasi Email Apakah Akun Dapat Digunakan.</label>
          
          <label><br>Terima Kasih Telah Mendaftar Sebagai Reseller Wearhadid</label>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" form="formregister" onclick="return confirm('Apakah Anda yakin data yang pendaftaran sudah benar?')">Register</button>
        </div>
      </div>
          <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
  </div>
      <!-- /.modal -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
<script>
  $('.only-string').keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });

    $('.only-number').keypress(function (e){
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
</script>

<script type="text/javascript">
	window.onload = function () 
	{
			document.getElementById("un").onchange = validateUsername;
			document.getElementById("pass").onchange = validatePass;
			document.getElementById("rpt").onchange = validateRptPassword;
			document.getElementById("telpon").onchange = validateTelpon;
			document.getElementById("email").onchange = validateEmail;
      document.getElementById("stk").onchange = validatestock;
	}
	
	function validateUsername()
	{
		var pengguna=document.getElementById("un").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validatePass()
	{
		var PassAwl=document.getElementById("pass").value;	
		if(PassAwl.length <4)
		{
      alert("Password Min 4 digit");
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validateRptPassword(){
		var pass2=document.getElementById("rpt").value;
		var pass1=document.getElementById("pass").value;
		if(pass1!=pass2)
			document.getElementById("rpt").setCustomValidity("Passwords Tidak Sama");
		else
			document.getElementById("rpt").setCustomValidity('');
	}
	
	function validateTelpon()
	{
		var Tlp=document.getElementById("telpon").value;
		if(Tlp.length <10 || Tlp.length>12)
		{
			alert("Nomor Telepon harus 10-12 digit!"); 
			return false;
		}
		else
		{
			return true;
		}
	}

  function validatestock()
  {
    var pengguna=document.getElementById("stk").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
</script>
  </body>
</html>