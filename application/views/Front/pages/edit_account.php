<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  </head>
  <body style="background:url('<?php echo base_url('assets/dist/img/wh6.jpeg ')?>') no-repeat; background-size: cover" class="w-100 h-100vh">
  <div class="container rounded bg-light mt-3 mb-auto">
    <!-- <div class="row"> -->
    <?php foreach($member as $u){ ?>
    <form action="<?php echo site_url('Front/ProsesEditMember/').$u->IDMember;?>" method="post" id="formregister">
            
        <div class="p-2">
                <div class="d-flex justify-content-center align-items-center mb-2">
                    <h3 class="text-center">Edit Account</h3>
                </div>
                <div class="row mt-2">
                    <div class="col-md-6">
                      <label class="labels">Name</label>
                      <input type="text" name="nama" class="form-control" value="<?php echo $u->Name; ?>" required>
                    </div>
                    <div class="col-md-6">
                      <label class="labels">Username</label>
                      <input type="text" name="usernama" class="form-control" id="un" value="<?php echo $u->Username; ?>" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                      <label class="labels">Password</label>
                      <input type="password" name="password" id="pass" class="form-control" minlength="4" >
                    </div>
                    <div class="col-md-6">
                      <label class="labels">Konfirmasi Password</label>
                      <input type="password" name="password2" id="rpt" class="form-control" >
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6 wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                      <label class="labels">Email</label>
                      <input type="Email" name="email" class="form-control" value="<?php echo $u->Email; ?>" required>
                    </div>
                    <div class="col-md-6"><label class="labels"><label>Gender</label>
                        <?php if($u->Gender == 'L' ){?>
                        <select name="jeniskelamin" class="form-control" required>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                        </select>
                        <?php } else if($u->Gender == 'P'){?>
                        <select name="jeniskelamin" class="form-control" required>
                        <option value="P">Perempuan</option>
                        <option value="L">Laki-Laki</option>
                        </select>
                    <?php } ?>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6"><label class="labels">Phone</label>
                    <input type="text" name="telfon" class="form-control" value="<?php echo $u->Phone; ?>" maxlength="12" minlength="10" id="telpon" required>
                    </div>
                    <div class="col-md-6"><label class="labels">Postal Code</label>
                        <input type="text" name="kodepost" class="form-control" value="<?php echo $u->Postal_Code; ?>" maxlength="5" minlength="5" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                      <label class="labels">Adress</label>
                      <Textarea type="text" class="form-control" placeholder="Adress" name="alamat" required><?php echo $u->Address; ?></Textarea>
                    </div>
                </div>
                <?php }?>
                <button type="submit" class="btn btn-outline-dark btn-lg btn-block mt-4 mb-3" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Edit</button>
                <!-- <div class="btn btn-outline-dark btn-lg btn-block mt-4 mb- 3" type="button">Save Profile</button></div> -->
          </div>
      <!-- </div> -->
    </form>
  </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
<script>
  $('.only-string').keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });

    $('.only-number').keypress(function (e){
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
</script>

<script type="text/javascript">
	window.onload = function () 
	{
			document.getElementById("un").onchange = validateUsername;
			document.getElementById("pass").onchange = validatePass;
			document.getElementById("rpt").onchange = validateRptPassword;
			document.getElementById("telpon").onchange = validateTelpon;
			document.getElementById("email").onchange = validateEmail;
      document.getElementById("stk").onchange = validatestock;
	}
	
	function validateUsername()
	{
		var pengguna=document.getElementById("un").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validatePass()
	{
		var PassAwl=document.getElementById("pass").value;	
		if(PassAwl.length <4)
		{
      alert("Password Min 4 digit");
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validateRptPassword(){
		var pass2=document.getElementById("rpt").value;
		var pass1=document.getElementById("pass").value;
		if(pass1!=pass2)
			document.getElementById("rpt").setCustomValidity("Passwords Tidak Sama");
		else
			document.getElementById("rpt").setCustomValidity('');
	}
	
	function validateTelpon()
	{
		var Tlp=document.getElementById("telpon").value;
		if(Tlp.length <10 || Tlp.length>12)
		{
			alert("Nomor Telepon harus 10-12 digit!"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
</script>
  </body>
</html>