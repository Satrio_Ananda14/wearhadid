<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Setting Slider</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Slider</li> 
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:90%;margin-left:3rem;">
        <div class="card mb-4">
            <div class="card-body">
              <?php echo form_open_multipart('Front/ProsesTambahGambar')?>
                <div class="form-group">
                  <label>Image</label>
                  <input type="file" name="gambar" class="form-control">
                </div>
                <div class="form-group">
                  <label>Kategori</label>
                  <select name="Status" class="form-control">
                    <option value="Utama">Utama</option>
                    <option value="Lainnya">Lainnya</option>
                  </select> 
                </div>
                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary btn-sm " class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Daftar</button>
                </div>
              </form>
              <?php echo form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>