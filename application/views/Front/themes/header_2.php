<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS & js -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bantuan/style2.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bantuan/style5.css">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bantuan/jquery.flipster.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/fontawesome/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/js/jquery.min.js">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/fontawesome/css/fontawesome.min.css">
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
        <script src= "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></script>
        <script src= "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></script>
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src= "https://code.jquery.com/jquery-3.6.0.slim.min.js"></script>

    </head>

    <style>
        /* NAVBAR STYLING STARTS */
        .navbar {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 20px;
        background-color: teal;
        color: #fff;
        }
        .nav-links a {
        color: #fff;
        }
        /* LOGO */
        .logo {
        font-size: 32px;
        }
        /* NAVBAR MENU */
        .menu {
        display: flex;
        gap: 1em;
        font-size: 18px;
        }
        .menu li:hover {
        /* background-color: #4c9e9e; */
        border-radius: 5px;
        transition: 0.3s ease;
        }
        .menu li {
        padding: 10px 10px;
        }
        /* DROPDOWN MENU */
        .services {
        position: relative; 
        }
        .dropdown {
        background-color: rgb(1, 139, 139);
        padding: 1em 0;
        position: absolute; /*WITH RESPECT TO PARENT*/
        display: none;
        border-radius: 8px;
        top: 35px;
        }
        .dropdown li + li {
        margin-top: 10px;
        }
        .dropdown li {
        padding: 0.5em 1em;
        width: 8em;
        text-align: center;
        }
        .dropdown li:hover {
        /* background-color: #4c9e9e; */
        }
        .services:hover .dropdown {
        display: block;
        }

        /*HAMBURGER MENU*/
        input[type=checkbox]{
            display: none;
            } 

            /*HAMBURGER MENU*/
            .hamburger {
           
            display: none;
            font-size: 24px;
            user-select: none;
            }

            /* APPLYING MEDIA QUERIES */
            @media (max-width: 768px) {
            .menu { 
            display:none;
            position: absolute;
            background-color: #1e1e1e;
            right: 0;
            left: 0;
            text-align: center;
            padding: 16px 0;
            }

            .menu li:hover {
            display: inline-block;
            /* background-color:#4c9e9e; */
            transition: 0.3s ease;
            }

            .menu li + li {
            width:100%;
            margin-top: 12px;
            }

            input[type=checkbox]:checked ~ .menu{
            margin-top: 365px;
            display: block;
            z-index: 1;
            }

            .hamburger {
            display: block;
            }

            .dropdown {
            padding : 10px;
            left: 50%;
            top: 30px;
            transform: translateX(100%);
            }
            
            .my-auto{
            margin-left:50px;
            }
        }

        /* The sticky class is added to the header with JS when it reaches its scroll position */
        .sticky {
        position: fixed;
        top: 0;
        width: 100%
        }

        /* Add some top padding to the page content to prevent sudden quick movement (as the header gets a new position at the top of the page (position:fixed and top:0) */
        .sticky + .content {
        padding-top: 102px;
        }
    </style>

    <body style="background-color:#000000;">
    <header class="header bg-dark fixed-top" id="myHeader">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="menu-area d-flex justify-content-between">
                    <div class="menu-content-area d-flex align-items-center">
                        <input type="checkbox" id="checkbox_toggle"/>
                        <label for="checkbox_toggle" class="hamburger"  style="padding-left:10px; padding-top:5 px; color: white;">&#9776;</label>
                        <img class="my-auto" src="<?= base_url(); ?>assets/img/icon/logol.png" width="40px" height="40px" style="margin-left:40px;">
                        <div class="menu">
                        <ul class="mb-auto mt-auto">
                            <li style="display: inline-block;"><a class="nav-link text-white-50" aria-current="page" href="#" style="transition: all 0.3s ease 0s">Home</a></li>
                            <li style="display: inline-block; border-radius: 20px;"><a class="nav-link text-white-50" href="#Product">Product</a> </li>
                            <li style="display: inline-block;"><a class="nav-link text-white-50" href="#Reseller">Reseller</a> </li>
                            <li style="display: inline-block;"><a class="nav-link text-white-50" href="#Contact_Us">Contact Us</a> </li>
                        <ul>
                        </div>
                    
                    </div>
                        <div class="menu-content-area d-flex align-items-left">
                            <!-- Header Social Area -->
                            
                            <div class="header-social-area d-flex align-items-center">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-shopping-bag fa-2x mt-2 mb-auto me-auto text-white" style="font-size: 24px;" ></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in me-auto" aria-labelledby="alertsDropdown">
                                <?php
                                $no = 0;
                                foreach($cart as $u){ 
                                    $Member=$u->IDMember;
                                    $no++;?>
                                <a class="dropdown-item">
                                    <div class="row">
                                        <div class="col-6" style="max-width:25%">
                                            <img src="<?php echo base_url('assets/dist/img/').$u->img ?>" style="width:50px; height:50px;"/>
                                        </div>
                                        <div class="col-6">
                                            <p style="margin-bottom:2px"><?php echo $u->Name?></p>
                                            <span class="count"> Quantity: <?php echo $u->Qty?></span>
                                        </div>
                                    </div>
                                </a>
                                <?php } ?>

                                <?php if($no == 0){
                                    $Member="";?>
                                     <a class="dropdown-item">
                                        <div class="row">
                                            <div class="col cart-detail-product text-center">
                                                <p><?php echo "Cart Kosong";?></p>
                                            </div>
                                        </div>
                                    </a>
                                <?php } ?>
                                    <div class="dropdown-divider"> </div>
                                    <?php if($Member != ""){?>
                                    <a class="dropdown-item text-center" href="<?php echo site_url("Cart/cart_list/").$Member;?>">View Cart</a>
                                <?php } ?>
                                </div>
                            </div>

                            <div class="header-social-area d-flex align-items-center">
                                <a class="nav-link dropdown-toggle" ref="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                    <!-- <img class="img-profile rounded-circle mt-2 mb-auto me-auto " src="https://i.imgur.com/uIgDDDd.jpg" style="width: 30px; height: 30px;"> -->
                                    <i class="far fa-user-circle fa-2x mt-2 mb-auto me-auto text-white" style="font-size: 24px;" ></i>
                                </a>
                                <?php if($this->session->userdata('logins')) {?>
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in me-auto" aria-labelledby="userDropdown"> 
                                        <?php
                                            $ID=$_SESSION["id"];
                                            $arr = str_split($ID);
                                         if($arr[0] == 'A'){
                                            ?>
                                            <a class="dropdown-item" href="<?php echo site_url("Back_Admin"); ?>"> 
                                                <i class="fas fa-chart-bar fa-sm fa-fw mr-flex text-gray-400"></i> Dashboard 
                                            </a>
                                        <?php }else if($arr[0] == 'R'){ ?>
                                            <a class="dropdown-item" href="<?php echo site_url("Back_Reseller");?>"> 
                                                <i class="fas fa-chart-bar fa-sm fa-fw mr-flex text-gray-400"></i> Dashboard 
                                            </a>
                                        <?php }
                                         if($arr[0] == 'A'){
                                        ?>
                                            <a class="dropdown-item" href="<?php echo site_url("Back_Admin/EditProfil/").$ID;?>"> 
                                                <i class="fa fa-cogs fa-sm fa-fw mr-flex text-gray-400"></i> Settings Account
                                            </a>
                                        <?php }else if($arr[0] == 'R'){ ?> 
                                            <a class="dropdown-item" href="<?php echo site_url("Back_Reseller/EditProfil/").$ID;?>"> 
                                                <i class="fa fa-cogs fa-sm fa-fw mr-flex text-gray-400"></i> Settings Account
                                            </a>
                                        <?php }else if($arr[0] == 'M'){ ?> 
                                            <a class="dropdown-item" href="<?php echo site_url("Front/EditAccount/");?>"> 
                                                <i class="fa fa-cogs fa-sm fa-fw mr-flex text-gray-400"></i> Settings Account
                                            </a>
                                        <?php }?>
                                        <a class="dropdown-item" href="<?php echo site_url("Login/logout"); ?>"> 
                                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> Logout 
                                        </a>
                                    </div>
                                <?php }else{?>
                                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in me-auto" aria-labelledby="userDropdown">
                                        <a class="dropdown-item text-center" href="<?php echo site_url("Login"); ?>"> 
                                            Login
                                        </a>  
                                        <a class="dropdown-item" href="<?php echo site_url("Login/Daftar");?>"> 
                                            Register Member
                                        </a>
                                    </div>
                                <?php }?>
                                
                            </div>
                            <!-- Menu Icon -->
                            <span class="navbar-toggler-icon" id="menuIcon"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</header>
