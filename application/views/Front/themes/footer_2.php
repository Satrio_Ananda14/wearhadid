</div>
    <footer class="text-light fixed-bottom d-flex flex-column" style="background-color: #3F4244;">
    <!-- Grid Container -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="menu-area d-flex justify-content-between bg">
                        <div class="menu-content-area d-flex align-items-center">
                        <!-- Copyright Text -->
                            <small class="ml-2 ml-sm-2 mb-2">Copyright &copy; CV.Vorteil 2021</small>
                        </div>
                        <div class="social-contact"> 
                            <a href="https://www.facebook.com/wear.hadid"><span class="fab fa-facebook-f mr-4 me-lg-2 text-sm" style="color: white;"></span></a>
                            <a href="https://www.instagram.com/wearhadid/"><span class="fab fa-instagram mr-4 me-lg-2 text-sm" style="color: white;"></span></a>
                            <a href="https://www.wearhadid.id/"><span class="fab fa-google mr-4 me-lg-2 text-sm" style="color: white;"></span></a>
                            <a href="https://wa.me/628996961011"><span class="fab fa-whatsapp mr-4 me-lg-2 text-sm" style="color: white;"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-body">
                <div id="tampil_modal">
              
                </div>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script>
        // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myFunction()};

        // Get the header
        var header = document.getElementById("myHeader");

        // Get the offset position of the navbar
        var sticky = header.offsetTop;

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        }
    </script>

    <script>
        $(document).ready(function(){
            $('.detail').click(function(){
                var id = $(this).attr("id");
                $.ajax({
                    url: '<?php echo base_url(); ?>/Cart/cart_list',
                    method: 'post',
                    data: {id : id},
                    success:function(data){
                        $('#modal-xl').modal("show");
                        $('#tampil_modal').html(data);  
                    }
                });
            });
        });
    </script>

<script>
    function updcart(id){
        $.ajax({
            url:'<?php echo base_url(); ?>/Cart/Update',
            type:'POST',
            data:$("#frm"+id).serialize(),
            success:function(data){
                location.reload();
            }
        });
    }
</script>


 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()?>assets/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- Bootstrap CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>		
	<script src="<?php echo base_url()?>assets/bantuan/jquery.flipster.min.js"></script>
    <script>
	    $('.carousel2').flipster({
	    	style:'carousel',
	    	spacing: -0.3,
	    });
	</script>
	<script>
		var $slider = document.getElementById('slider');
		var $toggle = document.getElementById('toggle');

		$toggle.addEventListener('click', function() {
		    var isOpen = $slider.classList.contains('slide-in');

		    $slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
		});
	</script>
    
    <script>
	    var scene = document.getElementById('scene');
	    var parallax = new Parallax(scene);

	        TweenMax.from(".image2", 1, {
	            delay: 2,
	            opacity: 0,
	            y: -800,
	            ease: Expo.easeInOut
	        })
    </script>
    </body>
</html>