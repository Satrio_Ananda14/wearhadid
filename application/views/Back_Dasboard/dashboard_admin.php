  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard Admin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Stock Product</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="position-relative mb-4">
                  <canvas id="product" height="176"></canvas>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          
          <!--  -->
          <?php 
            $no_bantu=0;
            $bantu_ID_1 = "";
            foreach($Transaksi as $u) {
              if($u->Status== 'Deliver'){
                $ID=$u->Date;
                if($bantu_ID_1 != $ID){
                  $bantu_ID_1 = $ID;
                  $no_bantu++;
                }
              }
            }

              $no_bantu_2 = $no_bantu--;
              $no = 0;
              $bantu_ID = "";
              $Total_Penjualan_1 = 0;
              $Total_Penjualan_2 = 0;
              foreach( $Transaksi as $u) {
                if($u->Status== 'Deliver'){
                  $ID=$u->Date;
                  if($bantu_ID != $ID){
                    $bantu_ID = $ID;
                    $no++;
                    if($no==$no_bantu){
                      foreach($Transaksi as $s){
                        if($u->Date == $s->Date){
                            $Total_Penjualan_1=$Total_Penjualan_1+$s->Total_Price;
                        }
                      }
                    }
                    if($no==$no_bantu_2){
                      foreach($Transaksi as $s){
                        if($u->Date == $s->Date){
                            $Total_Penjualan_2=$Total_Penjualan_2+$s->Total_Price;
                        }
                      }
                    }
                  }
                }               
              }

            if($Total_Penjualan_2!=0 && $Total_Penjualan_1!=0){
              $kenaikan_penjualan = (($Total_Penjualan_2 - $Total_Penjualan_1) / $Total_Penjualan_1)*100;
              if($kenaikan_penjualan > 1){
                $kenaikan_penjualan_Fix = number_format($kenaikan_penjualan);
              } else {
                $kenaikan_penjualan_Fix = number_format($kenaikan_penjualan, 3, ".", ",");
              }
            }
          ?>

          <!-- Penjulaan Untuk Mingguan  -->
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Sales</h3>
                  <h3 class="card-title">View Report</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="ml-auto d-flex flex-column text-right">
                    <!-- Kondisional Untuk Arrow Atas atau Bawah  -->
                  <?php if($Total_Penjualan_2!=0 && $Total_Penjualan_1!=0){
                        //Penjualan Turun
                              if($kenaikan_penjualan_Fix < 0){?>
                        <span class="text-danger">
                        <i class="fas fa-arrow-down"></i> <?php echo abs($kenaikan_penjualan_Fix)."%";?>
                      <?php }else{?>
                        <!-- Penjualan Naik -->
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> <?php echo $kenaikan_penjualan_Fix."%";?>
                      <?php }}else{?>
                        <!-- Tidak ada Kenaikan atau keturunan -->
                        <span class="text-primary">
                         0 %
                      <?php }?>
                      
                    </span>
                    <span class="text-muted">Since Last Day</span>
                  </p>
                </div>
                <div class="position-relative mb-4">
                  <canvas id="sales" height="130"></canvas>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>

      <!-- Pendaftaran Reseller Baru -->
      <div class="row">
        <div class="col-lg-12" style="margin-left:0.5rem; max-width:98.5%;">
            <div class="card">
              <div class="card-body">
                <h3 class="card-title"> Reseller Baru dan Non Aktif </h3> <br>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <!-- tampilan tabel kolom -->
                  <tr>
                    <th>No.</th>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Telepon</th>
                    <th>Status</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                      //mengambil data admin tidak aktif
                      $no = 1;
                      foreach($reseller as $u){ 
                        if($u->Status == 'Non Aktif'){
                  ?>
                  <tr>
                    <th><?php echo $no++ ?></th>
                    <th><?php echo $u->IDReseller ?></th>
                    <th><?php echo $u->Name ?></th>
                    <th><?php echo $u->Phone ?></th>
                    <th><?php echo $u->Status ?></th>
                    <th class="text-center">
                      <a href="<?php echo site_url('Kelola_User/Accept_Reseller/'.$u->IDReseller);?>" class="btn btn-success btn-sm" onclick="return confirm('Apakah anda yakin ingin menerima Reseller ?')"><i class="fas fa fa-check"></i> Accept</a>
                      <a href="<?php echo site_url('Kelola_User/Reject_Reseller/'.$u->IDReseller);?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin Menolak Reseller ?')"><i class="fas fa fa-times"></i> Reject</a></th>
                    </th>
                  </tr>
                  <?php };} ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
      </div>

      <!-- Penjualan Wearhadid Perbulan -->
      <div class="row">
        <div class="col-lg-12" style="max-width:97%;margin-left:1rem;">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Sales Every Month</h3>
                  <a href="<?php echo site_url('Laporan');?>">View Report</a>
                </div>
              </div>
              <?php
                $i=1;
                $data_tertinggi[1] = 0;
                
                foreach($Bulan as $u){
                  if($u->Total_Akhir > 0){
                    $z = $i;
                  }
                  $i++;
                }

                $no_bulan_sebelumnya = $z--;
                $a=1;
                $Nilai_Awal_Bulan=0;

                foreach($Bulan as $u){
                  if($u->Total_Akhir && $a==$z){
                    $a.",";
                    $Nilai_Awal_Bulan = $u->Total_Akhir;
                  }

                  if($u->Total_Akhir && $a==$no_bulan_sebelumnya){
                    $a.",";
                    $Nilai_Akhir_Bulan = $u->Total_Akhir;
                  }
                  $a++;
                }

                if($Nilai_Awal_Bulan!=0 && $Nilai_Akhir_Bulan!=0){
                  $kenaikan_penjualan_bulan = (($Nilai_Akhir_Bulan - $Nilai_Awal_Bulan) / $Nilai_Awal_Bulan)*100;
                  // if($kenaikan_penjualan_bulan>1){
                    $kenaikan_penjualan_bulan_Fix = number_format($kenaikan_penjualan_bulan);
                  // }else{
                  //   $kenaikan_penjualan_bulan_Fix = number_format($kenaikan_penjualan_bulan, 3,'.',',');
                  // }
                }
              ?>

              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg">Rp. <?php echo number_format($Penjualan_Tertinggi_Bulan)?> </span>
                    <span>Sales Over Time</span>
                  </p>
                  <p class="ml-auto d-wflex flex-column text-right">
                    
                      <?php if($Nilai_Akhir_Bulan!=0 && $Nilai_Awal_Bulan!=0){
                              if($kenaikan_penjualan_bulan_Fix < 0){?>
                        <span class="text-danger">
                        <i class="fas fa-arrow-down"></i> <?php echo abs($kenaikan_penjualan_bulan_Fix)."%";?>
                      <?php }else{?>
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> <?php echo $kenaikan_penjualan_bulan_Fix."%";?>
                      <?php }}else{?>
                        <span class="text-primary">
                         0 %
                      <?php }?>
                      
                    </span>
                    <span class="text-muted"><br>Since Last Month</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-5">
                  <canvas id="sales_month" height="130"></canvas>
                </div>
                
                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> This year
                  </span>
                  <span>
                    <i class="fas fa-square text-gray"></i> Last year
                  </span>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
      </div> 
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  