  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard Reseller</h1> <!-- nanti ada tambahan -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Stock Product</h3>
                </div>
              </div>
              <div class="card-body">
                <div class="position-relative mb-5">
                  <canvas id="product" height="173"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> Total Produk Left
                  </span>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          
          <?php 
            $no_bantu=0;
            $bantu_ID_1 = "";
            foreach(array_reverse($Transaksi) as $u) {
              $ID=$u->Date;
              if($bantu_ID_1 != $ID){
                $bantu_ID_1 = $ID;
                $no_bantu++;
              }
            }

            $no_bantu_2 = $no_bantu--;
            $no = 0;
            $bantu_ID = "";
            $Total_Penjualan_1 = 0;
            $Total_Penjualan_2 = 0;
            foreach(array_reverse($Transaksi) as $u) {
              $ID=$u->Date;
              if($bantu_ID != $ID){
                $bantu_ID = $ID;
                $no++;
                if($no==$no_bantu){
                  foreach($Transaksi as $s){
                    if($u->Date == $s->Date){
                        $Total_Penjualan_1=$Total_Penjualan_1+$s->Total_Price;
                    }
                  }
                }
                if($no==$no_bantu_2){
                  foreach($Transaksi as $s){
                    if($u->Date == $s->Date){
                        $Total_Penjualan_2=$Total_Penjualan_2+$s->Total_Price;
                    }
                  }
                }               
              }
            }
            if($Total_Penjualan_2!=0 && $Total_Penjualan_1!=0){
              $kenaikan_penjualan = (($Total_Penjualan_2 - $Total_Penjualan_1) / $Total_Penjualan_1)*100;
              $kenaikan_penjualan_Fix = number_format($kenaikan_penjualan);
            }
          ?>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Sales</h3>
                  <a href="<?php echo site_url('Laporan_Reseller');?>">View Report</a>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="ml-auto d-flex flex-column text-right">
                    
                      <?php if($Total_Penjualan_2!=0 && $Total_Penjualan_1!=0){
                              if($kenaikan_penjualan_Fix < 0){?>
                        <span class="text-danger">
                        <i class="fas fa-arrow-down"></i> <?php echo abs($kenaikan_penjualan_Fix)."%";?>
                      <?php }else{?>
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> <?php echo $kenaikan_penjualan_Fix."%";?>
                      <?php }}else{?>
                        <span class="text-primary">
                         0 %
                      <?php }?>
                      
                    </span>
                    <span class="text-muted">Since Last Day</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-5">
                  <canvas id="sales" height="130"></canvas>
                </div>
                
                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> This year
                  </span>
                  <span>
                    <i class="fas fa-square text-gray"></i> Last year
                  </span>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
        <div class="row">
        <div class="col">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Sales</h3>
                  <a href="<?php echo site_url('Laporan_Reseller');?>">View Report</a>
                </div>
              </div>
              <?php
              $z=0;
              $i=1;
              $data_tertinggi[1] = 0;
              foreach($Bulan as $u){
                if($u->Total_Akhir > 0){
                  $z = $i;
                }
                $i++;
              }

              $no_bulan_sebelumnya = $z--;
              $a=1;
              $Nilai_Awal_Bulan=0;
              $Nilai_Akhir_Bulan=0;
              foreach($Bulan as $u){
                if($u->Total_Akhir && $a==$z){
                  $a.",";
                  $Nilai_Awal_Bulan = $u->Total_Akhir;
                }

                if($u->Total_Akhir && $a==$no_bulan_sebelumnya){
                  $a.",";
                  $Nilai_Akhir_Bulan = $u->Total_Akhir;
                }
                $a++;
              }

              if($Nilai_Awal_Bulan!=0 && $Nilai_Akhir_Bulan!=0){
                $kenaikan_penjualan_bulan = (($Nilai_Akhir_Bulan - $Nilai_Awal_Bulan) / $Nilai_Awal_Bulan)*100;
                $kenaikan_penjualan_bulan_Fix = number_format($kenaikan_penjualan_bulan);
              }

              ?>

              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg">Rp. <?php echo number_format($Penjualan_Tertinggi_Bulan)?> </span>
                    <span>Sales Over Time</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    
                      <?php if($Nilai_Akhir_Bulan!=0 && $Nilai_Awal_Bulan!=0){
                              if($kenaikan_penjualan_bulan_Fix < 0){?>
                        <span class="text-danger">
                        <i class="fas fa-arrow-down"></i> <?php echo abs($kenaikan_penjualan_bulan_Fix)."%";?>
                      <?php }else{?>
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> <?php echo $kenaikan_penjualan_bulan_Fix."%";?>
                      <?php }}else{?>
                        <span class="text-primary">
                         0 %
                      <?php }?>
                      
                    </span>
                    <span class="text-muted">Since Last Month</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-5">
                  <canvas id="sales_month" height="130"></canvas>
                </div>
                
                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> This year
                  </span>
                  <span>
                    <i class="fas fa-square text-gray"></i> Last year
                  </span>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
  