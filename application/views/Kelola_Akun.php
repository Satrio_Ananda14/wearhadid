<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Reseller</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Edit Reseller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Data Reseller</h6>
          </div>
            <div class="card-body">
            <?php foreach($member as $u){ ?>
              <form action="<?php echo site_url('Kelola_User/ProsesEditMember/'), $u->IDMember; ?>" method="post">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" value="<?php echo $u->Name; ?>" required>
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="usernama" class="form-control" value="<?php echo $u->Username; ?>" required>  
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="Text" name="password" class="form-control" value="<?php echo $u->Password; ?>" required>  
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control" value="<?php echo $u->Address; ?>" required>
                </div>
                <div class="form-group">
                  <label>Kode Post</label>
                  <input type="text" name="kodepost" class="form-control" value="<?php echo $u->Kodepos; ?>" required>
                </div>
                <div class="form-group">
                  <label>Detail Alamat</label>
                  <input type="text" name="dltalamat" class="form-control" value="<?php echo $u->Detail_Alamat; ?>" required>
                </div>
                <div class="form-group">
                  <label>No. Telepon</label>
                  <input type="text" name="telfon" class="form-control" value="<?php echo $u->Phone; ?>" required>
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <?php if($u->Gender == 'L' ){?>
                        <select name="jeniskelamin" class="form-control" required>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                        </select>
                    <?php } else if($u->Gender == 'P'){?>
                        <select name="jeniskelamin" class="form-control" required>
                        <option value="P">Perempuan</option>
                        <option value="L">Laki-Laki</option>
                        </select>
                    <?php } ?>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="Email" name="email" class="form-control" value="<?php echo $u->Email; ?>" required>
                </div>
                <?php }?>
                    <a href="<?php echo site_url('Kelola_User/DetailReseller/'),$u->IDMember;?>" class="btn btn-light btn-icon-split" style="float: left;">
                    <span class="icon text-gray-600">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                    </a>
                    <button type="submit" class="btn btn-primary btn-icon-split" style="float: right;" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>