<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">


  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo site_url("Back_Reseller");?>" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo site_url("Daftar_Produk");?>" class="nav-link">Product</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <div class="navbar-search-block">
        </div>
      </li>

      <!-- Profil -->
      <li class="nav-item">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <img src="<?php echo base_url('')?>assets/dist/img/Non_Profil.jpg" height=100% width= 100% alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url('/Back_Reseller/EditProfil/').$_SESSION['id']?>" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> Edit Akun
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url();?>/Login/logout" class="dropdown-item" onclick="return confirm('Yakin untuk Logout ?')">
                        <i class="fas fa-sign-out-alt mr-2"></i> Logout
                    </a>
        </div>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
