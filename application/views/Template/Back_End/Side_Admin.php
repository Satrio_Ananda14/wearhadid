  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url('assets/dist/img/logol.png" alt="AdminLTE Logo" width= 20% heigh = 20%  style="opacity: .8 ')?>">
      <span class="brand-text font-weight-light">Wearhadid</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">Admin - <?php echo $_SESSION["name"];?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo site_url("Back_Admin");?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url("Front/Slider");?>" class="nav-link">
              <i class="nav-icon fas fa-images"></i>
              <p>
                Slider Front End
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url("Data_Transaksi");?>" class="nav-link">
              <i class="nav-icon fas fa-file-invoice-dollar"></i>
              <p>
                Transaction
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url("Penjualan_Reseller");?>" class="nav-link">
              <i class="nav-icon fas fa-file-invoice-dollar"></i>
              <p>
                Transaction Reseller 
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href=<?php echo site_url("Laporan");?> class="nav-link">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>
                Report  
              </p>
            </a>
          </li>
          <li class="nav-header">
                DATA MASTER
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-folder"></i>
              <p>
                Product  
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" >
              <li class="nav-item">
                <a href=<?php echo site_url("Kelola_Produk/Produk");?> class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Produk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href=<?php echo site_url("Kelola_Produk/Kategori");?> class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                User
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" >
              <li class="nav-item">
                <a href=<?php echo site_url("Kelola_User/Admin");?> class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href=<?php echo site_url("Kelola_User/Reseller");?> class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Reseller</p>
                </a>
              </li>
              <li class="nav-item">
                <a href=<?php echo site_url("Kelola_User/Member");?> class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Member</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>