    <!-- /.content-wrapper -->
    <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://vorteil.co.id">Vorteil</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 0.0.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.mask.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.js"></script>

<!-- DataTables  & Plugins -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>assets/dist/js/demo.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url()?>assets/dist/js/pages/dashboard3.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!-- Ajax -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->

<script>
$(document).ready(function(){
    $('#masking1').mask('000.000.000', {reverse: true});
})
</script>

<!-- Chart Batang Stock Product -->
<script>
var ctx = document.getElementById('product');
var visitors = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [<?php foreach($produk as $u) { echo '"'.$u->ProductName.'",'; }?>],
      datasets: [{
        axis: 'y',
        label: 'Sisa Stok',
        data: [<?php foreach($produk as $u) {  echo $u->ItemStock.','; }?>],
        fill: false,
        backgroundColor: [
          <?php $i=0; foreach($produk as $u) {
             $b[$i]=rand(0,255);  
             $r[$i]=rand(0,255);
             $g[$i]=rand(0,255); ?>
          'rgba( <?php echo $r[$i]?>, <?php echo $g[$i]?>, <?php echo $b[$i]?>, 0.1)',
          <?php $i++;}?>
        ],
        borderColor: [
          <?php $a=0; foreach($produk as $u) {
             ?>
          'rgb( <?php echo $r[$a]?>, <?php echo $g[$a]?>, <?php echo $b[$a]?>)',
          <?php $a++; }?>
        ],
        borderWidth: 1,
      }]
    }
});
</script>


<script>
  var ctx = document.getElementById('sales_month');
  var visitors = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [<?php
      for($a=1;$a<=12;$a++){
        echo '"'.$Nama_Bulan[$a].'",';
      }?>],
      datasets: [{
        axis: 'y',
        label: 'Sales',
        data: [
        <?php 
        $i=1;
      foreach($Bulan as $u){
        echo '"'.$u->Total_Akhir.'",';
        if($u->Total_Akhir > 0){
          $z=$i;
        }
        $i++;
      }
        ?>],
        fill: false,
        borderColor: [
          'rgb(75, 192, 192)',
        ],
        borderWidth: 1,
        tension: 0.2
      }]
    }
});
</script>

<!-- Chart Garis Penjualan Minggunan -->
<script>
var ctx = document.getElementById('sales');
var visitors = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [<?php 
      $no_bantu=0;
      $bantu_ID_1 = "";
      foreach($Transaksi as $u) {
          if($u->Status=='Deliver'){
          $ID=$u->Date;
          $jumlah_data = 0;
          if($bantu_ID_1 != $ID){
            $bantu_ID_1 = $ID;
            $no_bantu++;
          }
        }
      }

      $cal = $no_bantu-7;

      $no = 0;
      $bantu_ID = "";
      foreach($Transaksi as $u) {
        if($u->Status=='Deliver'){
          $ID=$u->Date;
          if($bantu_ID != $ID){
            $bantu_ID = $ID;
            if($no>=$cal){
            $tgl = str_replace("-","/",$bantu_ID);
            echo '"'.$tgl.'",';
            }
            $no++;
          }
        }
      }?>],
      datasets: [{
        axis: 'y',
        label: 'Sales',
        data: [
        <?php 
        $no = 0;
        $bantu_ID = "";
        foreach($Transaksi as $u) {
          if($u->Status=='Deliver'){
            $ID=$u->Date;
            if($bantu_ID != $ID){
              $bantu_ID = $ID;
              $Total_Penjualan = 0;
              if($no>=$cal){
                foreach($Transaksi as $s){
                  if($u->Date == $s->Date && $s->Status=='Deliver'){
                      $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                  }
                }
                echo '"'.$Total_Penjualan.'",';
              }
              $no++;   
            }
          }            
        }?>],
        fill: false,
        borderColor: [
          'rgb(75, 192, 192)',
        ],
        borderWidth: 1,
        tension: 0.2
      }]
    }
});
</script>

<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

<script>

  $(function () {
    $("#example2").DataTable();
  });
</script>

<script>
  $(document).ready(function(e){
    $('.detail').on('click',function(){
      var idorder = $(this).attr("id");
      $.ajax({
          url: '<?php echo base_url(); ?>/Data_Transaksi/tabelorder',
          method: 'post',
          data: {idorder : idorder},
          success:function(data){
              $('#myModal').modal("show");
              $('#tampil_modal').html(data);  
          }
      });
    });

    $('.edit').click(function(){
      var idorder = $(this).attr("id");
      $.ajax({
          url: '<?php echo base_url(); ?>/Data_Transaksi/tabelbuktipembayaran',
          method: 'post',
          data: {idorder : idorder},
          success:function(data){
              $('#myModal2').modal("show");
              $('#tampil_modal2').html(data);  
          }
      });
    });

    $('.detail_deliver').click(function(){
      var idorder = $(this).attr("id");
      $.ajax({
          url: '<?php echo base_url(); ?>/Data_Transaksi/tabeldeliver',
          method: 'post',
          data: {idorder : idorder},
          success:function(data){
              $('#myModal3').modal("show");
              $('#tampil_modal3').html(data);  
          }
      });
    });
  });
</script>

<script>
  $('.only-string').keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });

    $('.only-number').keypress(function (e){
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
</script>

<script type="text/javascript">
	window.onload = function () 
	{
			document.getElementById("un").onchange = validateUsername;
			document.getElementById("pass").onchange = validatePass;
			document.getElementById("rpt").onchange = validateRptPassword;
			document.getElementById("telpon").onchange = validateTelpon;
			document.getElementById("email").onchange = validateEmail;
      document.getElementById("stk").onchange = validatestock;
	}
	
	function validateUsername()
	{
		var pengguna=document.getElementById("un").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validatePass()
	{
		var PassAwl=document.getElementById("pass").value;	
		if(PassAwl.length <4)
		{
      alert("Password Min 4 digit");
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validateRptPassword(){
		var pass2=document.getElementById("rpt").value;
		var pass1=document.getElementById("pass").value;
		if(pass1!=pass2)
			document.getElementById("rpt").setCustomValidity("Passwords Tidak Sama");
		else
			document.getElementById("rpt").setCustomValidity('');
	}
	
	function validateTelpon()
	{
		var Tlp=document.getElementById("telpon").value;
		if(Tlp.length <10 || Tlp.length>12)
		{
			alert("Nomor Telepon harus 10-12 digit!"); 
			return false;
		}
		else
		{
			return true;
		}
	}

  function validatestock()
  {
    var pengguna=document.getElementById("stk").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
</script>

</body>
</html>