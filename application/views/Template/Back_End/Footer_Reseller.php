    <!-- /.content-wrapper -->
    <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://vorteil.co.id">Vorteil</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 0.0.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.mask.min.js"></script>

<!-- Bootstrap -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.js"></script>

<!-- DataTables  & Plugins -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/toastr/toastr.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>assets/dist/js/demo.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url()?>assets/dist/js/pages/dashboard3.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!-- Jquery DatePicker -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Form Masking Harga dengan titik -->
<script>
$(document).ready(function(){
    $('#masking1').mask('#.##0', {reverse: true});
    $('#masking2').mask('#.##0,0', {reverse: true});
    $('#masking3').mask('#,##0.00', {reverse: true});
})
</script>

<script>
var rupiah1 = document.getElementById("rupiah1");
rupiah1.addEventListener("keyup", function(e) {
  rupiah1.value = convertRupiah(this.value);
});
rupiah1.addEventListener('keydown', function(event) {
	return isNumberKey(event);
});
 
var rupiah2 = document.getElementById("rupiah2");
rupiah2.addEventListener("keyup", function(e) {
  rupiah2.value = convertRupiah(this.value, "Rp. ");
});
rupiah2.addEventListener('keydown', function(event) {
	return isNumberKey(event);
});
 
function convertRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split  = number_string.split(","),
    sisa   = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);
 
	if (ribuan) {
		separator = sisa ? "." : "";
		rupiah += separator + ribuan.join(".");
	}
 
	rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
	return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
}
 
function isNumberKey(evt) {
    key = evt.which || evt.keyCode;
	if ( 	key != 188 // Comma
		 && key != 8 // Backspace
		 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
		 && (key < 48 || key > 57) // Non digit
		) 
	{
		evt.preventDefault();
		return;
	}
}
</script>


<!-- Template Tabel -->
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

<script>
  $(function () {
    $("#example2").DataTable();
  });
</script>


<!-- Chart Batang Product -->
<script>
var ctx = document.getElementById('product');
var visitors = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [<?php foreach($produk as $u) {  echo '"'.$u->ProductName.'",'; }?>],
      datasets: [{
        axis: 'y',
        label: 'Sisa Stok',
        data: [<?php foreach($produk as $u) {  echo $u->Stock.','; }?>],
        fill: false,
        borderColor: [
          'rgb(75, 192, 192)',
        ],
        borderWidth: 1,
      }]
    }
});
</script>

<!-- Chart Garis Penjualan Minggunan -->
<script>
var ctx = document.getElementById('sales');
var visitors = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [<?php 
      $no_bantu=0;
      $bantu_ID_1 = "";
      foreach(array_reverse($Transaksi) as $u) {
        $ID=$u->Date;
        $jumlah_data = 0;
        if($bantu_ID_1 != $ID){
          $bantu_ID_1 = $ID;
          $no_bantu++;
        }
      }

      $cal = $no_bantu-7;

      $no = 0;
      $bantu_ID = "";
      foreach(array_reverse($Transaksi) as $u) {
        $ID=$u->Date;
        if($bantu_ID != $ID){
          $bantu_ID = $ID;
          if($no>=$cal){
          $tgl = str_replace("-","/",$bantu_ID);
          echo '"'.$tgl.'",';
          }
          $no++;
        }
      }?>],
      datasets: [{
        axis: 'y',
        label: 'Sales',
        data: [
        <?php 
        $no = 0;
        $bantu_ID = "";
        foreach(array_reverse($Transaksi) as $u) {
          $ID=$u->Date;
          if($bantu_ID != $ID){
            $bantu_ID = $ID;
            $Total_Penjualan = 0;
            if($no>=$cal){
              foreach($Transaksi as $s){
                if($u->Date == $s->Date){
                    $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                }
              }
              echo '"'.$Total_Penjualan.'",';
            }
            $no++;
            
          }
        }?>],
        fill: false,
        borderColor: [
          'rgb(75, 192, 192)',
        ],
        borderWidth: 1,
        tension: 0.2
      }]
    }
});
</script>

<!-- Chart Sales Bulanan -->
<script>
  var ctx = document.getElementById('sales_month');
  var visitors = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [<?php
      for($a=1;$a<=12;$a++){
        echo '"'.$Nama_Bulan[$a].'",';
      }?>],
      datasets: [{
        axis: 'y',
        label: 'Sales',
        data: [
        <?php 
        $i=1;
      foreach($Bulan as $u){
        echo '"'.$u->Total_Akhir.'",';
        if($u->Total_Akhir > 0){
          $z=$i;
        }
        $i++;
      }
        ?>],
        fill: false,
        borderColor: [
          'rgb(75, 192, 192)',
        ],
        borderWidth: 1,
        tension: 0.2
      }]
    }
});
</script>

<!-- Bantuan Modal Edit Penjualan Satu Item -->
<script>
     $(document).ready(function() {
         $('#edit-data').on('show.bs.modal', function (event) {
             var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
             var modal = $(this)

             // Isi nilai pada field
             modal.find('#id').attr("value",div.data('id'));
             modal.find('#idproduct').attr("value",div.data('idproduct'));
             modal.find('#idpenjualan').attr("value",div.data('idpenjualan'));
             modal.find('#nama').attr("value",div.data('nama'));
             modal.find('#total_produk').attr("value",div.data('total_produk'));
             modal.find('#harga').attr("value",div.data('harga'));
         });
     });
</script>

<!-- Toast untuk validasi Data Tersimpan -->
<script>
  toastr.option={
    "showDuration": "400",
    "timeOut": "3000"
  }

  function success_toast(){
    toastr.success('Data Tersimpan')
  }
</script>

<!-- Datepicker Tambah Penjualan Reseller -->
<script>
  $(document).ready(function () {
    var currentDate = new Date();
    $('#disableFuturedate').datepicker({
    dateFormat: 'yy-mm-dd',
    autoclose:true,
    endDate: "currentDate",
    maxDate: currentDate
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    $('#disableFuturedate').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
          this.value = this.value.replace(/[^0-9^-]/g, '');
        }
    });
  });
</script>

<script>
  $('.only-string').keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });

    $('.only-number').keypress(function (e){
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
</script>

<script type="text/javascript">
	window.onload = function () 
	{
			document.getElementById("un").onchange = validateUsername;
			document.getElementById("pass").onchange = validatePass;
			document.getElementById("rpt").onchange = validateRptPassword;
			document.getElementById("telpon").onchange = validateTelpon;
			document.getElementById("email").onchange = validateEmail;
      document.getElementById("stk").onchange = validatestock;
	}
	
	function validateUsername()
	{
		var pengguna=document.getElementById("un").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validatePass()
	{
		var PassAwl=document.getElementById("pass").value;	
		if(PassAwl.length <4)
		{
      alert("Password Min 4 digit");
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function validateRptPassword(){
		var pass2=document.getElementById("rpt").value;
		var pass1=document.getElementById("pass").value;
		if(pass1!=pass2)
			document.getElementById("rpt").setCustomValidity("Passwords Tidak Sama");
		else
			document.getElementById("rpt").setCustomValidity('');
	}
	
	function validateTelpon()
	{
		var Tlp=document.getElementById("telpon").value;
		if(Tlp.length <10 || Tlp.length>12)
		{
			alert("Nomor Telepon harus 10-12 digit!"); 
			return false;
		}
		else
		{
			return true;
		}
	}

  function validatestock()
  {
    var pengguna=document.getElementById("stk").value;		
		if(pengguna.length <8 || pengguna.length>12)
		{
      alert("Username 8-15 Karakter"); 
			return false;
		}
		else
		{
			return true;
		}
	}
<script>

</body>
</html>