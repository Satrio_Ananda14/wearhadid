<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?php echo base_url('assets/dist/img/Logol.png')?>" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo site_url('Back_Admin');?>" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo site_url('Kelola_Produk/Produk');?>" class="nav-link">Product</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge"><?php echo $JN ?></span>
        </a>
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
          aria-labelledby="alertsDropdown" style="width:275px">
          <span class="dropdown-item dropdown-header"><?php echo $JN?> Notifications</span>
          <?php foreach($Notifikasi as $u){
            if($u->Status == "not seen"){
              $ID = $u->ID;
              $arr1 = str_split($ID);
              $Hak_Akses = $arr1[0];
              if($Hak_Akses != 'R'){
              ?>  
              <a href="<?php echo base_url('Back_Admin/NotifikasiOrder/').$u->No;?>" class="dropdown-item d-flex align-items-center">
                <div class="mr-1">
                    <i class="fas fa-envelope mr-2"></i>
                </div>
                <div style="world-wap:break-word">
                  <div class="small text-gray-500"><?php echo $u->Date; ?></div>
                  <div style=" width: 215px; height: auto; word-wrap: break-word; overflow-wrap: break-word; white-space: pre-wrap;"><?php echo $u->Value_Notification_Order; ?></div>
                </div>
              </a>
          <?php }else{
            ?>
              <a href="<?php echo base_url('Back_Admin/NotifikasiRegisterReseller/').$u->No;?>" class="dropdown-item d-flex align-items-center">
              <div class="mr-1">
                <i class="fas fa-user-plus"></i>
              </div>
              <div style="world-wap:break-word">
                <div class="small text-gray-500"><?php echo $u->Date; ?></div>
                <div style=" width: 215px; height: auto; word-wrap: break-word; overflow-wrap: break-word; white-space: pre-wrap;"><?php echo $u->Value_Notification_Order; ?></div>
              </div>
              </a>
          <?php }
          }
        }?>
      </li>
      <!-- Profile -->
      <li class="nav-item">
        <a class="nav-link" data-toggle="dropdown">
            <img src="<?php echo base_url('')?>assets/dist/img/Non_Profil.jpg" height=100% width= 100% alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
            <a href="<?php echo base_url('/Back_Admin/EditProfil/').$_SESSION['id']?>" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> Edit Akun
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url();?>/Login/logout" class="dropdown-item" onclick="return confirm('Yakin untuk Logout ?')">
              <i class="fas fa-sign-out-alt mr-2"></i>Logout
            </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
