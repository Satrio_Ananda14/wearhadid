 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url('assets/dist/img/logol.png" alt="AdminLTE Logo" width= 20% heigh = 20%  style="opacity: .8 ')?>">
      <span class="brand-text font-weight-light">Wearhadid</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">Reseller - <?php echo $_SESSION["name"];?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo site_url("Back_Reseller");?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dasboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo site_url("Data_Penjualan");?>" class="nav-link">
              <i class="nav-icon fas fa-file-invoice-dollar"></i>
              <p>
                Transaction
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url("Daftar_Produk");?>" class="nav-link">
              <i class="nav-icon fas fa-folder"></i>
              <p>
                 List Product  
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url("Laporan_Reseller");?>" class="nav-link">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>
                 Report
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>