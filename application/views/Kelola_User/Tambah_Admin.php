<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Admin</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Tambah Admin</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?php echo $ID ?></h6>
          </div>
            <div class="card-body">
              <form action="<?php echo site_url('Kelola_User/ProsesTambahAdmin'); ?>" method="post">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control only-string" maxlength="25" size="10" required>
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="usernama" id="un" class="form-control"  required>  
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" id="pass" class="form-control" minlength="4" required>  
                </div>
                <div class="form-group">
                  <label>Konfirmasi Password</label>
                  <input type="password" name="password2" id="rpt" class="form-control" required>  
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>No. Telepon</label>
                  <input type="text" name="telfon" class="form-control only-number" maxlength="12" minlength="10" id="telpon" required>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select name="jeniskelamin" class="form-control" required>
                  <option value="L">Laki-Laki</option>
                  <option value="P">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="Email" name="email" class="form-control" required>
                </div>
                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary btn-sm " class="text-center" onclick="return confirm('Apakah Anda yakin data yang Anda akan daftarkan sudah benar?')">Daftar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>