<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Reseller</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Detail Reseller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12" style="max-width:60%;margin-left:15rem;">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Data Reseller</h6>
                </div>
                <?php foreach($reseller as $u){ ?>
                <div class="card-body">
                    <div class="form-group">
                      <label>Nama :</label>
                      <h4> <?php echo $u->Name ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Username</label>
                      <h4> <?php echo $u->Username ?></div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Password</label>
                      <h4> ******</h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Alamat</label>
                      <h4> <?php echo $u->Address ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>No. Telepon</label>
                      <h4> <?php echo $u->Phone ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <h4> <?php echo $u->Gender ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Email</label>
                      <h4> <?php echo $u->Email ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                    <div class="form-group">
                      <label>Status</label>
                      <h4> <?php echo $u->Status ?></h4>
                    </div>
                    <hr class="sidebar-divider">
                  <a href="<?php echo site_url('Kelola_User/Reseller');?>" class="btn btn-light btn-icon-split" >
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                  </a>
                  <a href="<?php echo site_url('Kelola_User/EditReseller/'),$u->IDReseller ;?>" class="btn btn-primary btn-icon-split" style="float: right;">
                    <span class="text">Edit Reseller</span>
                    <?php } ?>
                  </a>
                </div>
              </div>
            </div>
            </div>
          </div>
</div>