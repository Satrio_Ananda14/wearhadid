<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    <!-- Row -->
    <div class="row">
      <!-- DataTable with Hover -->
      <div class="col-lg-12" style="max-width:60%;margin-left:15rem;margin-top:2rem">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Edit Profil</h6>
          </div>
            <div class="card-body">
            <?php foreach($admin as $u){ ?>
              <form action="<?php echo base_url('Back_Admin/ProsesEditAdmin/'), $u->IDAdmin; ?>" method="post">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" value="<?php echo $u->Name; ?>" maxlength="25" size="10" required>
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="usernama" class="form-control" value="<?php echo $u->Username; ?>" required>  
                </div>
                <div class="form-group">
                  <label>Password Baru</label>
                  <input type="password" name="password" id="pass" minlength="4" class="form-control">  
                </div>
                <div class="form-group">
                  <label>Konfirmasi Password Baru</label>
                  <input type="password" name="password2" id="rpt" class="form-control">  
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control" value="<?php echo $u->Address; ?>" required>
                </div>
                <div class="form-group">
                  <label>No. Telepon</label>
                  <input type="text" name="telfon" class="form-control" value="<?php echo $u->Phone; ?>" maxlength="12" minlength="10" id="telpon" required>
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <?php if($u->Gender == 'L' ){?>
                        <select name="jeniskelamin" class="form-control" required>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                        </select>
                    <?php } else if($u->Gender == 'P'){?>
                        <select name="jeniskelamin" class="form-control" required>
                        <option value="P">Perempuan</option>
                        <option value="L">Laki-Laki</option>
                        </select>
                    <?php } ?>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="Email" name="email" class="form-control" value="<?php echo $u->Email; ?>">
                </div >
                <?php }?>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary btn-icon-split" class="text-center" onclick="return confirm('Apakah Anda yakin data yang diubah sudah benar?')">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>