<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kelola Reseller</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kelola User</a></li>
              <li class="breadcrumb-item active">Reseller</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Reseller Baru dan Non Aktif </h3>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Telepon</th>
                    <th>L/P</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                      $no = 1;
                      foreach($reseller as $u){ 
                        if($u->Status == 'Non Aktif'){
                  ?>
                  <tr>
                    <th><?php echo $no++ ?></th>
                    <th><?php echo $u->IDReseller ?></th>
                    <th><?php echo $u->Name ?></th>
                    <th><?php echo $u->Phone ?></th>
                    <th><?php echo $u->Gender ?></th>
                    <th><?php echo $u->Status ?></th>
                    <th>
                      <!-- <a href="<?php echo site_url('Kelola_User/DetailReseller/'.$u->IDReseller);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Detail</a> -->
                      <!-- <a type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-sm"><i class="fas fa fa-trash"></i> Hapus </a></th> -->
                      <!-- <a href="<?php echo site_url('Kelola_User/ProsesHapusReseller/'.$u->IDReseller);?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin menghapus data ?')"><i class="fas fa fa-trash"></i> Reject </a> -->
                      <a href="<?php echo site_url('Kelola_User/Accept_Reseller/'.$u->IDReseller);?>" class="btn btn-success btn-sm" onclick="return confirm('Apakah anda yakin ingin menerima Reseller ?')"><i class="fas fa fa-check"></i> Accept</a>
                      <a href="<?php echo site_url('Kelola_User/Reject_Reseller/'.$u->IDReseller);?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin Menolak Reseller ?')"><i class="fas fa fa-times"></i> Reject</a></th>
                    </th>
                  </tr>
                  <?php };} ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i> Reseller </h3>
                  <a href="<?php echo site_url('Kelola_User/TambahReseller');?>" type="button" class="btn btn-primary btn-sm" style="float: right;"><i class="fas fa fa-user-plus"></i> Tambah Reseller </a>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>L/P</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                        $no = 1;
                        foreach($reseller as $u){
                          if($u->Status == 'Aktif'){
                    ?>
                  <tr>
                    <th><?php echo $no++ ?></th>
                    <th><?php echo $u->IDReseller ?></th>
                    <th><?php echo $u->Name ?></th>
                    <th><?php echo $u->Address ?></th>
                    <th><?php echo $u->Phone ?></th>
                    <th><?php echo $u->Gender ?></th>
                    <th><?php echo $u->Status ?></th>
                    <th>
                      <a href="<?php echo site_url('Kelola_User/DetailReseller/'.$u->IDReseller);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Detail</a>
                      <!-- <a type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-sm"><i class="fas fa fa-trash"></i> Hapus </a></th> -->
                      <a href="<?php echo site_url('Kelola_User/ProsesHapusReseller/'.$u->IDReseller);?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin menghapus data ?')"><i class="fas fa fa-trash"></i> Hapus </a>
                    </th>
                  </tr>
                  <?php ;}} ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi Hapus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Hapus Data ?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <a type="button" href="<?php echo site_url('Kelola_User/ProsesHapusReseller/'.$u->IDReseller);?>" class="btn btn-primary" style="width:75px">Ya</a>
          <button type="button" class="btn btn-danger" data-dismiss="modal" style="width:75px">Tidak</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->