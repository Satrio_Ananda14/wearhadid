<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Laporan</h1>
                </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Kelola Produk</a></li>
                    <li class="breadcrumb-item active">Produk</li>
                </ol>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="row">
        <div class="col-12" style="max-width:90%;margin-left:3rem;">
        <div class="card">
          <div class="card-header">
               <h3 class="card-title"> <i class="fas fa-table"></i>  Penjualan / Transaksi</h3>
           </div>
           <div class="card-body"> 
                <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php if($month){ echo 'active'; } ?>" id="custom-content-above-home-tab" data-toggle="pill" href="#Bulan" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Month</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($day){ echo 'active'; } ?>" id="custom-content-above-profile-tab" data-toggle="pill" href="#Hari" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Day</a>
                    </li>
                </ul>
                <div class="tab-custom-content">
                  <p class="lead mb-0"></p>
                </div>
                <div class="tab-content" id="custom-content-above-tabContent">
                    <div class="tab-pane fade show <?php if($month){ echo 'active'; } ?>" id="Bulan" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                        <table>
                            <tr>
                            <form action="<?php echo site_url('Laporan/'); ?>" method="post" id="form1">
                                <td><h5>Bulan  </h5></td>
                                <td><h5>:</h5></td>
                                <td>
                                    <select name="bulan" class="form-control">
                                        <?php for($i=0; $i<12; ++$i) { 
                                            $time=strtotime(sprintf('--%d months',$i));
                                            $monthvalue=date('m', $time);
                                            $monthname=date('F',$time);
                                            printf('<option value="%s">%s</option>', $monthvalue,$monthname);
                                        }?>
                                    </select> 
                                </td>
                                <td>   <td>
                                <td><h5>Tahun  </h5></td>
                                <td><h5>:</h5></td>
                                <td> 
                                    <select name="tahun" class="form-control">
                                    <?php $y=(int)date("Y") ?>
                                    <option value="<?php echo $y;?>" selected="true"><?php echo $y; ?></option>
                                    <?php $y--;
                                    for(;$y>"2014"; $y--){?>
                                        <option value="<?php echo $y;?>"><?php echo $y; ?></option>
                                    <?php } ?>
                                </select>  </td>
                            </form>
                                <td> <button type="submit" form="form1" class="btn btn-primary btn-sm " class="text-center">Cari</button></td> 
                            <tr>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr style="text-align:center">
                                    <th>No.</th>
                                    <th>Reseller</th>
                                    <th>Total Produk</th>
                                    <th>Total Penjualan</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $no = 1;
                            $true=0;
                            $bantu_ID = "";
                            foreach($month as $u){
                                $true=1;
                                $ID=$u->IDReseller;
                                if($bantu_ID != $ID){
                                    $bantu_ID = $ID; 
                                    $Total_Penjualan = 0;
                                    $Produk = 0;
                                    foreach($month as $s){
                                        if($u->IDReseller == $s->IDReseller){
                                            $Produk = $Produk + $s->Quantity;
                                            $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <th><?php echo $no++?></th>
                                        <th><?php echo $u->Name?></th>
                                        <th><?php echo $Produk?></th>
                                        <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.')?></th>
                                    </tr>
                                    <?php 
                                }
                            ?>
                        <?php }?>
                        </tbody>
                        </table>
                        <table>
                        <?php if($true==1){ ?>
                            <a style=" margin-left: auto; margin-right: auto;"href="<?php echo site_url('Laporan/Cetak_Bulanan/'.$bulan.'/'.$tahun)?>" class="btn btn-primary btn-sm center">
                            <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Cetak Laporan</span>
                            </a>
                        <?php }?>
                        </table>
                    </div>
                    <div class="tab-pane fade show <?php if($day){ echo 'active'; }?>" id="Hari" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                    <table>
                        <tr height =70px>
                        <form action="<?php echo site_url('Laporan/'); ?>" method="post" id="form2">
                            <td align="center"><h5>Tanggal</h5></td>
                            <td align="center" width=20><h5>:</h5></td>
                            <td> <input type="date" name="tgl" class="form-control" value="<?php echo $hari ?>" required> </td>
                        </form>    
                            <td> <button type="submit" form="form2" class="btn btn-primary btn-sm " class="text-center">Cari</button></td> 
                        <tr>
                    </table>
                    <br>
                    <table id="example1" class="table table-bordered table-striped"> 
                        <?php 
                        $no = 0;
                        $true=0;
                        $bantu_ID = "";
                        foreach($day as $u){
                            $true=1;
                            $ID=$u->IDReseller;
                            $jumlah_data = 0;
                            foreach($day as $s){
                                if($u->IDReseller == $s->IDReseller){
                                    $jumlah_data = $jumlah_data + 1;
                                }
                            }?>
                                    <?php if($bantu_ID != $ID){
                                        $bantu_ID = $ID;
                                        $Total_Penjualan = 0;
                                        $Produk = 0;
                                        foreach($day as $s){
                                            if($u->IDReseller == $s->IDReseller){
                                                $Produk = $Produk + $s->Quantity;
                                                $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                                            }
                                        }
                                        ?>
                                    <thead>
                                    <tbody>
                                    <tr style="text-align:center">
                                        <th>No.</th>
                                        <th>Reseller</th>
                                        <th>Nama Produk</th>
                                        <th>Total Produk</th>
                                        <th>Total Penjualan</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <th rowspan="<?php echo $jumlah_data?>"><?php echo $u->IDReseller?> </th>
                                        <th rowspan="<?php echo $jumlah_data?>"><?php echo $u->Name?></th>
                                    <?php }
                                    $no++;
                                    ?>
                                    <th ><?php echo $u->PName?></th>
                                    <th><?php echo $u->Quantity?></th>
                                    <th>Rp. <?php echo number_format($u->Total_Price,0,',','.')?></th>
                                </tr>
                                <?php if($bantu_ID == $ID && $no== $jumlah_data){
                                    $no=0;?>
                                <tr>
                                    <th colspan=4 style="text-align:center">Total</th>
                                    <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.')?></th>
                                </tr>
                                <tr>
                                    <th colspan=4> <th>
                                
                                <?php }?>
                        <?php }?>
                    </tbody>
                    </table>
                    <table>
                    <?php if($true==1){ ?>
                        <a style=" margin-left: auto; margin-right: auto;"href="<?php echo site_url('Laporan/Cetak_Harian/'.$hari)?>" class="btn btn-primary btn-sm center">
                            <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Cetak Laporan</span>
                        </a>
                    <?php }?>
                    </table>
                    </div>
                </div>
            </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>