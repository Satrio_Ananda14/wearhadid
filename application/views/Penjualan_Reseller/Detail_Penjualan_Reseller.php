<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Penjualan</a></li> 
              <li class="breadcrumb-item active"><?php echo $_SESSION["reseller"];?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php foreach($user as $u){?>
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card" style="height:300px;">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                <label class="card-title">Reseller</label>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="row">
                      <div class="col-6">
                        <label>ID Reseller :</label>
                        <h5><?php echo $u->IDReseller ?></h5>
                      </div>
                      <div class="col-6">
                        <label>Nama :</label>
                        <h5><?php echo $u->Name ?></h5>
                      </div>
                  </div>
                </div>
                  <hr class="sidebar-divider">
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                        <label>Nomor HP :</label>
                        <h5><?php echo $u->Phone ?></h5>
                    </div>
                    <div class="col-6">
                      <label>Jenis Kelamin :</label>
                      <h5><?php if($u->Gender == "L"){
                        echo "Laki-Laki";
                      }else{
                        echo "Perempuan";
                      } ?></h5>
                    </div>
                  </div>
                </div>
                  <hr class="sidebar-divider">
                  <?php }?>
              </div>
            </div>
            <!-- /.card -->
          </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <!-- /.col -->
                <div class="card card-outline">
              <div class="card-header">
              <h3 class="card-title"> 
                <i class="fas fa-table"></i>
                  Penjualan / Transaksi
              </h3>
              </div>
              <div class="card-body"> 
                <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#Tanggal" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Tanggal</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#Barang" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Produk</a>
                  </li>
                </ul>
                <div class="tab-custom-content">
                  <p class="lead mb-0"></p>
                </div>
                <div class="tab-content" id="custom-content-above-tabContent">
                  <div class="tab-pane fade show active" id="Tanggal" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Tanggal</th>
                      <th>Total Penjualan</th>
                      <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                          $no = 1;
                          $bantuan_tanggal=0;
                          foreach($transaksi as $u){
                            $tanggal = $u->Date;
                            if($bantuan_tanggal != $tanggal){
                              $bantuan_tanggal= $tanggal;
                              $Total_Penjualan=0;
                              foreach($transaksi as $s){
                                if($tanggal==$s->Date){
                                  $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                                }
                              }
                              ?>
                              <tr>
                                  <th><?php echo $no++?></th>
                                  <th><?php echo $u->Date?></th>
                                  <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.') ?></th>
                                  <th>
                                    <a href="<?php echo site_url('Penjualan_Reseller/DetailPenjualanPertanggal/'.$u->Date);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Detail </a>
                                  </th>
                            <?php } ?>
                              </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                  
                  </div>
                  <div class="tab-pane fade" id="Barang" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                    <h4 class="mt-1">Produk Reseller</h4>
                    <table id="example2" class="table table-bordered table-striped mt-1">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Kategori</th>
                      <th>Total Barang</th>
                      <th>Terjual</th>
                      <th>Stok</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no = 1;
                        foreach($daftarproduk as $s){
                          $Total_Barang_Terjual=0;
                          $Jumlah_Barang=0;
                          foreach($transaksi as $u){
                            if($s->IRProduct == $u->IDProduct){
                              $Total_Barang_Terjual=$Total_Barang_Terjual+$u->Quantity;
                            }
                          }
                          $Jumlah_Barang = $s->Stock + $Total_Barang_Terjual;
                      ?>
                    <tr>
                      <th><?php echo $no++ ?></th>
                      <th><?php echo $s->ProductName ?></th>
                      <th><?php echo $s->category ?></th>
                      <th><?php echo $Jumlah_Barang ?></th>
                      <th><?php echo $Total_Barang_Terjual ?></th>
                      <th><?php echo $s->Stock ?></th>
                    </tr>
                    <?php ;}?>
                    <?php  ?>
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>