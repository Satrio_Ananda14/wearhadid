<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Penjualan</a></li>
              <li class="breadcrumb-item active">Penjualan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i>Penjualan / Transaksi</h3>
                </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr style="text-align:center">
                    <th>No.</th>
                    <th>Reseller</th>
                    <th>Total Produk Terjual</th>
                    <th>Total Penjualan</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                    $no = 1;
                    foreach($reseller as $u){ 
                      $Produk = 0;
                      $Total_Penjualan = 0;
                      foreach($transaksi as $t){
                        if($t->IDReseller == $u->IDReseller){
                          $Produk = $Produk + $t->Quantity;
                          $Total_Penjualan = $Total_Penjualan + $t->Total_Price;
                        }
                      }
                    ?>
                  <tr>
                    <th><?php echo $no++?></th>
                    <th><?php echo $u->Name?></th>
                    <th><?php echo $Produk?></th>
                    <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.')?></th>
                    <th>
                     <a href="<?php echo site_url('Penjualan_Reseller/DetailPenjualanReseller/'.$u->IDReseller);?>" class="btn btn-primary btn-sm"><i class="fas fa fa-eye"></i> Detail</a>
                    </th>
                  </tr>
                  <?php  }?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>