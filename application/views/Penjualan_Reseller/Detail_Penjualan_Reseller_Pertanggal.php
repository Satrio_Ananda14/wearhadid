<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Penjualan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Penjualan</a></li>
              <li class="breadcrumb-item active">Penjualan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php foreach($user as $u){?>
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card" style="height:300px;">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <label class="card-title">Reseller</label>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="row">
                      <div class="col-6">
                        <label>ID Reseller :</label>
                        <h5><?php echo $u->IDReseller ?></h5>
                      </div>
                      <div class="col-6">
                        <label>Nama :</label>
                        <h5><?php echo $u->Name ?></h5>
                      </div>
                  </div>
                </div>
                  <hr class="sidebar-divider">
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                        <label>Nomor HP :</label>
                        <h5><?php echo $u->Phone ?></h5>
                    </div>
                    <div class="col-6">
                      <label>Jenis Kelamin :</label>
                      <h5><?php if($u->Gender == "L"){
                        echo "Laki-Laki";
                      }else{
                        echo "Perempuan";
                      } ?></h5>
                    </div>
                  </div>
                </div>
                  <hr class="sidebar-divider">
                  <?php }?>
              </div>
            </div>
            <!-- /.card -->
          </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-table"></i>    Penjualan / Transaksi</h3>
              </div>
              <div class="card-body">
              <table id="example1" class="table table-bordered table-striped"> 
                        <?php 
                        $no = 0;
                        $no_1 =1;
                        $true=0;
                        $bantu_ID = "";
                        foreach($transaksi as $u){
                            $true=1;
                            $ID=$u->IDTransaction;
                            $jumlah_data = 0;
                            foreach($transaksi as $s){
                                if($u->IDTransaction == $s->IDTransaction){
                                    $jumlah_data = $jumlah_data + 1;
                                }
                            }?>
                                    <?php if($bantu_ID != $ID){
                                        $bantu_ID = $ID;
                                        $Total_Penjualan = 0;
                                        $Produk = 0;
                                        foreach($transaksi as $s){
                                            if($u->IDReseller == $s->IDReseller && $u->IDTransaction == $s->IDTransaction){
                                                $Produk = $Produk + $s->Quantity;
                                                $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                                            }
                                        }
                                        ?>
                                    <thead>
                                    <tbody>
                                    <tr style="text-align:center">
                                        <th>No.</th>
                                        <th>No_Transaksi</th>
                                        <th>Nama Produk</th>
                                        <th>Total Produk</th>
                                        <th>Total Penjualan</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <th rowspan="<?php echo $jumlah_data?>"><?php echo $no_1++?> </th>
                                        <th rowspan="<?php echo $jumlah_data?>"><?php echo $u->IDTransaction?></th>
                                    <?php }
                                    $no++;
                                    ?>
                                    <th ><?php echo $u->Name?></th>
                                    <th><?php echo $u->Quantity?></th>
                                    <th>Rp. <?php echo number_format($u->Total_Price,0,',','.')?></th>
                                </tr>
                                <?php if($bantu_ID == $ID && $no== $jumlah_data){
                                    $no=0;?>
                                <tr>
                                    <th colspan=4 style="text-align:center">Total</th>
                                    <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.')?></th>
                                </tr>
                                <tr>
                                    <th colspan=4> <th>
                                
                                <?php }?>
                        <?php }?>
                    </tbody>
                    </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>