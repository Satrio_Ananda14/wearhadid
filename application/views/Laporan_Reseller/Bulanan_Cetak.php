<!DOCTYPE html>
<html>

<head>
    <style>
        .line-title {
            border: 0;
            border-style: inset;
            border-top: 1px solid #000;
        }

        table {
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <!-- <img src="assets/admin2/img/logo/logo2.png" style="position: absolute; width: 95px; height: auto; margin-left:10px;" > -->
    <table style="width: 100%;">
        <tr>
            <td align="center">
                <span style="line-height: 1.6; font-weight: bold; font-size: 20;">
                    <a href="<?php echo site_url('Laporan_Reseller') ?>" style="color:black; line-height: 1.6; font-weight: bold; font-size: 20; text-decoration:none;"> Wearhadid </a>
                </span>
                <span style="line-height: 1.6; font-size: 10">
                    <br>
                    Kota Bandung, Jawa Barat, Indonesia
                    <br>
                    No. Telepon : 08996961011
                </span>
            </td>
        </tr>
    </table>
    <br>
    <hr class="line-title">
    <br>
    <?php
    $dateObj = DateTime::createFromFormat('!m', $bulan);
    $namabulan = $dateObj->format('F');
    ?>

    <h3 style="text-align : center">LAPORAN PENJUALAN BARANG RESELLER WEARHADID</h3>
    <h4 style="text-align : center">Bulan <?php echo $namabulan ?>, Tahun <?php echo $tahun ?></h4>
    <table border="1" style="width: 100%;">
        <thead>
            <tr>
                <th>No.</th>
                <th>Tanggal</th>
                <th>Total Produk</th>
                <th>Total Penjualan</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $bantuan_tanggal = 0;
            $true = 0;
            foreach ($transaksi as $u) {
                $tanggal = $u->Date;
                $true = 1;
                if ($bantuan_tanggal != $tanggal) {
                    $bantuan_tanggal = $tanggal;
                    $Total_Penjualan = 0;
                    $Total_Quantity = 0;
                    foreach ($transaksi as $s) {
                        if ($tanggal == $s->Date) {
                            $Total_Quantity = $Total_Quantity = $s->Quantity;
                            $Total_Penjualan = $Total_Penjualan + $s->Total_Price;
                        }
                    }
            ?>
                    <tr>
                        <th><?php echo $no++ ?></th>
                        <th><?php echo $u->Date ?></th>
                        <th><?php echo $Total_Quantity ?></th>
                        <th>Rp. <?php echo number_format($Total_Penjualan, 0, ',', '.') ?></th>
                    <?php } ?>
                    </tr>
                <?php } ?>
        </tbody>
    </table>
    <br><br>
    <footer>
        <h5 style="text-align : right;">
            Bandung, <?php echo date('d F Y'); ?>
        </h5>
        <script>
            window.print();
        </script>
    </footer>
</body>

</html>