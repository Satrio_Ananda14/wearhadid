<!DOCTYPE html>
<html>
<head>
	<style>
		.line-title{
			border: 0;
			border-style: inset;
			border-top: 1px solid #000;
		}
		table {
			border-collapse: collapse;
		}
	</style>
</head>
<body>
   <!-- <img src="assets/admin2/img/logo/logo2.png" style="position: absolute; width: 95px; height: auto; margin-left:10px;" > -->
	<table style="width: 100%;">
		<tr>
			<td align="center">
				<span style="line-height: 1.6; font-weight: bold; font-size: 20;">
				<a href="<?php echo site_url('Laporan_Reseller')?>" style="color:black; line-height: 1.6; font-weight: bold; font-size: 20; text-decoration:none;">	Wearhadid </a>
				</span>
				<span style="line-height: 1.6; font-size: 10">
				<br>
				Kota Bandung, Jawa Barat, Indonesia
				<br>
				No. Telepon : 08996961011 
				</span>
			</td>
		</tr>
    </table>
    <br>
    <hr class="line-title">
	<br>

   <h3 style="text-align : center">LAPORAN PENJUALAN BARANG RESELLER WEARHADID</h3>
   <h4 style="text-align : center">Tanggal, <?php echo date('d F Y', strtotime($hari)); ?></h4>
	<br>
	<table border=1 style="width: 100%;"> 
        <?php 
        $no = 0;
        $no_1 =1;
        $true=0;
        $bantu_ID = "";
        foreach($transaksi as $u){
            $true=1;
            $ID=$u->IDTransaction;
            $jumlah_data = 0;
            foreach($transaksi as $s){
                if($u->IDTransaction == $s->IDTransaction){
                    $jumlah_data = $jumlah_data + 1;
                }
            }?>
                    <?php if($bantu_ID != $ID){
                        $bantu_ID = $ID;
                        $Total_Penjualan = 0;
                        $Produk = 0;
                        foreach($transaksi as $s){
                            if($u->IDReseller == $s->IDReseller && $u->IDTransaction == $s->IDTransaction){
                                $Produk = $Produk + $s->Quantity;
                                $Total_Penjualan=$Total_Penjualan+$s->Total_Price;
                            }
                        }
                        ?>
                    <thead>
                    <tbody>
                    <tr style="text-align:center">
                        <th>No.</th>
                        <th>No_Transaksi</th>
                        <th>Nama Produk</th>
                        <th>Total Produk</th>
                        <th>Total Penjualan</th>
                    </tr>
                    </thead>
                    <tr>
                        <th rowspan="<?php echo $jumlah_data?>"><?php echo $no_1++?> </th>
                        <th rowspan="<?php echo $jumlah_data?>"><?php echo $u->IDTransaction?></th>
                    <?php }
                    $no++;
                    ?>
                    <th ><?php echo $u->ProductName?></th>
                    <th><?php echo $u->Quantity?></th>
                    <th>Rp. <?php echo number_format($u->Total_Price,0,',','.')?></th>
                </tr>
                <?php if($bantu_ID == $ID && $no== $jumlah_data){
                    $no=0;?>
                <tr>
                    <th colspan=4 style="text-align:center">Total</th>
                    <th>Rp. <?php echo number_format($Total_Penjualan,0,',','.')?></th>
                </tr>
                <tr>
                    <th colspan=4> <th>
                                
                <?php }?>
        <?php }?>
    </tbody>
    </table>
	<br><br>
	<footer>
    <h5 style="text-align : right;">
		Bandung, <?php echo date('d F Y');?>
    </h5>
    <script>
    window.print();
    </script>
	</footer>
</body>
</html>