<?php
    class Data_Penjualan extends CI_Controller {

        function __construct(){
            parent::__construct();
            //Tidak Dapat mengakses halaman back-end resller jika belum login
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                //jika yang login bukan reseller maka akan pindah pada halaman login
                if($arr[0]!='R'){
                    redirect("Login");
                }
            }
            $this->load->model('Transaksi');
            $this->load->model('Produk');
        }

        //halaman data penjualan reseller
        function index(){
            $ID=$_SESSION["id"];
            $data['transaksi'] = $this->Transaksi->tampil_data_transaksi($ID)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            $this->load->view('Data_Penjualan/Data_Penjualan.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //proses tambah data penjualan
        // public function TambahDataPenjualan()
        // {
        //     $ID=$_SESSION["id"];
        //     $data['produk'] = $this->Produk->tampil_data_Daftarproduk($ID)->result();
        //     $this->load->view('Template/Back_End/Header_Reseller.php');
        //     $this->load->view('Template/Back_End/Nav_Reseller.php');
        //     $this->load->view('Template/Back_End/Side_Reseller.php');
        //     $this->load->view('Data_Penjualan/Tambah_Data_Penjualan.php', $data);
        //     $this->load->view('Template/Back_End/Footer_Reseller.php');
        // }


        // public function TambahDataPenjualan_Tanggal($tanggal)
        // {
        //     $ID=$_SESSION["id"];
        //     $dt = $this->Transaksi->tampil_data_transaksi_tgl($ID,$tanggal)->result();
        //     $Product = array();
        //     foreach($dt as $u){
        //         echo $Product[] = $u->IDProduct;
        //     }
        //     $data['produk'] = $this->Produk->daftar_belum_dijual_tanggal($ID,$Product)->result();
        //     $data['tgl'] = $tanggal;
        //     $this->load->view('Template/Back_End/Header_Reseller.php');
        //     $this->load->view('Template/Back_End/Nav_Reseller.php');
        //     $this->load->view('Template/Back_End/Side_Reseller.php');
        //     $this->load->view('Data_Penjualan/Tambah_Data_Penjualan_Tanggal.php', $data);
        //     $this->load->view('Template/Back_End/Footer_Reseller.php');
        // }

        function ProsesTambahDataPenjualan() 
        { 
            $Product = $this->input->post("produk");
            $ID = $_SESSION["id"];
            $data = $this->Produk->Detail_Bantuan_Daftar_Produk($ID,$Product)->row();
            $Produk_Name = $data->ProductName; 
            $Price = $data->Price;
            $Stock = $data->Stock;
            $Primary = $data->No;
            $QTY = $this->input->post("qty");
            $Sisa_Stok = $Stock-$QTY;
            $hrg = $this->input->post("hrg");
            $harga= preg_replace("/[^0-9]/", "", $hrg);
            if($harga){
                $Total_Price =  $QTY * $harga;
                $Price = $harga;
            }else{
                $Total_Price =  $QTY * $Price;
            }
            $tanggal=$this->input->post("tgl");


            $penjualan = array(
            "ID" => "",
            "IDTransaction" =>  $_SESSION["bantuan_notransaksi"],
            "IDReseller" => $_SESSION["id"],
            "IDProduct" => $this->input->post("produk"),
            "ProductName" => $Produk_Name,
            "NameBuyer" => $_SESSION["bantuan_nama"],
            "Expedition" => $_SESSION["bantuan_ekspedisi"],
            "Address" => $_SESSION["bantuan_almt"],
            "Price" => $Price, 
            "Quantity" => $this->input->post("qty"),
            "Total_Price" => $Total_Price,
            "Date" => $_SESSION["bantuan_tgl"]
            );

            $produk = array (
            "Stock" => $Sisa_Stok
            );

            if($Sisa_Stok >= 0){
                if($this->Transaksi->insertdatapenjualan($penjualan) && $this->Produk->Edit_Stok_Daftar_Produk($Primary,$produk)){ 
                    redirect(site_url("Data_Penjualan/TambahDataPenjualan_2/")); 
                }else{ 
                    redirect(site_url("Data_Penjualan/TambahDataPenjualan")); 
                }
            } else {
                $validasi = "*Produk ".$Produk_Name." Hanya Tersisa ".$Stock.", Masukan Qty kurang dari atau sama dengan sisa stock yang tersedia";
                $this->session->set_flashdata("error",$validasi);
                redirect(site_url("Data_Penjualan/TambahDataPenjualan_2/"));
            }
        }


        // public function EditDataPenjualan($id){
        //     $ID=$_SESSION["id"];
        //     $data['transaksi'] = $this->Transaksi->Detail_Data_Penjualan($id)->result();
        //     $data['produk'] = $this->Produk->tampil_data_Daftarproduk($ID)->result();
        //     $this->load->view('Template/Back_End/Header_Reseller.php');
        //     $this->load->view('Template/Back_End/Nav_Reseller.php');
        //     $this->load->view('Template/Back_End/Side_Reseller.php');
        //     $this->load->view('Data_Penjualan/Edit_Data_Penjualan.php', $data);
        //     $this->load->view('Template/Back_End/Footer_Reseller.php');
        // }

        public function ProsesEditDataPenjualan(){
            $id = $this->input->post("id");
            $no_penjualan = $this->input->post("idpenjualan"); 
            #Data Penjualan Sebelumnya
            $Penjualan = $this->Transaksi->Detail_Data_Penjualan($id)->row();
            $QTY_Penjualan = $Penjualan->Quantity;
            $Product_Penjualan = $Penjualan->IDProduct;

            #Data bantuan untuk Perhitungan Total Harga
            $Product_Baru = $this->input->post("idproduct");
            $ID = $_SESSION["id"];
            $QTY_Baru = $this->input->post("qty");
            $hrg = $this->input->post("harga");

            #Data Daftar Produk
            $Daftar_Produk = $this->Produk->Detail_Bantuan_Daftar_Produk($ID,$Product_Baru)->row();
            if($hrg){
                $Price = $hrg;
            }else{
                $Price = $Daftar_Produk->Price;
            }
            $Stock = $Daftar_Produk->Stock;
            $Key = $Daftar_Produk->No;

            #Perhitungan Total Harga
            $Total_Harga =  $QTY_Baru * $Price;

            #Update Stok Daftar Barang
            if($Product_Penjualan != $Product_Baru){
                $Daftar_Produk_2 = $this->Produk->Detail_Bantuan_Daftar_Produk($ID,$Product_Penjualan)->row();
                $Stock_2 = $Daftar_Produk_2->Stock;
                $Sisa_Stok_2 = $Stock_2 + ($QTY_Penjualan);
                $Key_2 = $Daftar_Produk_2->No;

                $produk_sebelumnya = array(
                    "Stock" => $Sisa_Stok_2
                );

                $this->Produk->Edit_Stok_Daftar_Produk($Key_2,$produk_sebelumnya);

                $Sisa_Stok = $Stock-$QTY_Baru;

            }else if($QTY_Penjualan != $QTY_Baru){

                $Sisa_Stok = $Stock-($QTY_Baru-$QTY_Penjualan);

            }else{
                $Sisa_Stok = $Stock;
            }

            #Data Penjualan Baru
            $penjualan = array(
                "IDReseller" => $_SESSION["id"],
                "IDProduct" => $this->input->post("idproduct"),
                "Price" => $Price, 
                "Quantity" => $this->input->post("qty"),
                "Total_Price" => $Total_Harga
            );

            #Update Daftar Barang Baru
            $produk_baru = array (
                "Stock" => $Sisa_Stok,
            );

            $tanggal = $this->input->post("tgl");

            if($this->Transaksi->Edit_Data_Penjualan($id,$penjualan) && $this->Produk->Edit_Stok_Daftar_Produk($Key,$produk_baru)){ 
                redirect(site_url("Data_Penjualan/DetailPenjualan/".$no_penjualan)); 
            }else{ 
                redirect(site_url("Data_Penjualan/")); 
            } 
        }

         //proses hapus data penjualan pada penjualan baru
        public function ProsesHapusDataPenjualan($ID){
            $Penjualan = $this->Transaksi->Detail_Data_Penjualan($ID)->row();
            $QTY = $Penjualan->Quantity;
            $IDProduct = $Penjualan->IDProduct;
            $IDReseller = $_SESSION["id"];
            

            $Daftar_Produk = $this->Produk->Detail_Bantuan_Daftar_Produk($IDReseller,$IDProduct)->row();
            $Stock = $Daftar_Produk->Stock;
            $Key = $Daftar_Produk->No;

            $Sisa_Stok = $Stock + $QTY;

            $Produk= array(
                "Stock" => $Sisa_Stok
            );

            $this->Produk->Edit_Stok_Daftar_Produk($Key,$Produk);
            $this->Transaksi->Hapus_Data_Penjualan($ID); 
            if($_SESSION["bantuan_tgl"]!=""){
                redirect(site_url("Data_Penjualan/TambahDataPenjualan_2/"));
                //redirect(site_url("Data_Penjualan/DetailPenjualan/".$Penjualan->Tanggal));
            }else{
               // redirect(site_url("Data_Penjualan"));
               redirect(site_url("Data_Penjualan/"));
            }
             
        }

        //proses hapus data penjualan pada detail penjualan
        public function ProsesHapusDataPenjualanDetail($ID){
            $Penjualan = $this->Transaksi->Detail_Data_Penjualan($ID)->row();
            $QTY = $Penjualan->Quantity;
            $IDProduct = $Penjualan->IDProduct;
            $IDReseller = $_SESSION["id"];
            $No_Penjualan = $Penjualan->IDTransaction;

            $Daftar_Produk = $this->Produk->Detail_Bantuan_Daftar_Produk($IDReseller,$IDProduct)->row();
            $Stock = $Daftar_Produk->Stock;
            $Key = $Daftar_Produk->No;

            $Sisa_Stok = $Stock + $QTY;

            $Produk= array(
                "Stock" => $Sisa_Stok
            );

            $this->Produk->Edit_Stok_Daftar_Produk($Key,$Produk);
            $this->Transaksi->Hapus_Data_Penjualan($ID); 
            if($this->Transaksi->tampil_data_transaksi_id($No_Penjualan,$IDReseller)->num_rows() > 0){
                redirect(site_url("Data_Penjualan/DetailPenjualan/".$No_Penjualan));
                //redirect(site_url("Data_Penjualan/DetailPenjualan/".$Penjualan->Tanggal));
            }else{
               // redirect(site_url("Data_Penjualan"));
               redirect(site_url("Data_Penjualan/"));
            }
             
        }

        //halaman detail penjualan
        public function DetailPenjualan($id){
            $reseller=$_SESSION["id"];
            $data['transaksi'] = $this->Transaksi->tampil_data_transaksi_id($id,$reseller)->result();
            // $dt = $this->Transaksi->tampil_data_transaksi_id($id,$reseller)->result();
            // foreach($dt as $u){
            //     echo $u->No_Penjualan;
            // }
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            $this->load->view('Data_Penjualan/Detail_Penjualan_Transaksi.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //metode tambah data penjualan
        public function TambahDataPenjualan_2(){
            $id=$_SESSION["id"];
            $dt = $this->Transaksi->tampil_data_transaksi_id($_SESSION["bantuan_notransaksi"],$id)->result();
            $Product = array();
            foreach($dt as $u){
                $Product[] = $u->IDProduct;
            }
            $data['produk'] = $this->Produk->daftar_belum_dijual_tanggal($id,$Product)->result();
            $data['transaksi'] = $this->Transaksi->tampil_data_transaksi_id($_SESSION["bantuan_notransaksi"],$id)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            $this->load->view('Data_Penjualan/Tambah_Penjualan_Transaksi.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //metode bantuan untuk menambah transaksi dan juga generate data transaksi
        public function SetBantuan(){
            $id=$_SESSION["id"];
            $tgl = $this->input->post("tgl");
            $nama = $this->input->post("nama");
            $ekspedisi = $this->input->post("ekpedisi");
            $almt = $this->input->post("almt");
            $arr1 = preg_replace("/[^0-9]/", "", $tgl);
            $notransaksi = "";
            $count = $this->Transaksi->CheckTanggal($id,$tgl)->num_rows();
            if($count == 0){
                $notransaksi = 'T'.$id.$arr1.'1';
            }else{
                $dt=$this->Transaksi->CheckTanggal($id,$tgl)->result();
                $bantuan_NoPenjualan="";
                $i=0;
                foreach($dt as $s){
                    $Penjualan = $s->IDTransaction;
                    if($bantuan_NoPenjualan != $Penjualan){
                        $bantuan_NoPenjualan = $Penjualan;
                        $i = $i + 1;
                    }
                }
                $i++;
                $notransaksi = 'T'.$id.$arr1.$i;
                if($this->Transaksi->tampil_data_transaksi_id($notransaksi,$id)->num_rows() > 0){
                    $i++;
                    $notransaksi = 'T'.$id.$arr1.$i;
                }
            }
            $session_data = array(
                "bantuan_notransaksi" => $notransaksi,
                "bantuan_tgl" => $tgl,
                "bantuan_nama" =>  $nama,
                "bantuan_ekspedisi" => $ekspedisi,
                "bantuan_almt" => $almt
            );
            $this->session->set_userdata($session_data);
            redirect(site_url("Data_Penjualan/TambahDataPenjualan_2"));
        }

        //menyimpan data transaksi
        public function SimpanData(){
            $array_items = array(
                "bantuan_notransaksi", 
                "bantuan_tgl",
                "bantuan_nama",
                "bantuan_ekspedisi",
                "bantuan_almt"
            );
            $this->session->unset_userdata($array_items);
            redirect(site_url("Data_Penjualan"));
        }

        //cetak invoice penjualan
        public function Cetak_Invoice($id)
	    {
            $ID=$_SESSION["id"];
            $data['Transaksi'] = $this->Transaksi->tampil_data_transaksi_id($id,$ID)->result();
            $this->load->view('Data_Penjualan/Cetak_Invoice.php',$data);
	    }

        // public function EditProductTransaction($id){
        //     $data['Product'] = $this->Transaksi->Detail_Data_Penjualan($id)->result();
        //     $this->load->view('Template/Back_End/Header_Reseller.php');
        //     $this->load->view('Template/Back_End/Nav_Reseller.php');
        //     $this->load->view('Template/Back_End/Side_Reseller.php');
        //     $this->load->view('Data_Penjualan/Edit_Data_Product', $data);
        //     $this->load->view('Template/Back_End/Footer_Reseller.php');
        // }
    }
?>