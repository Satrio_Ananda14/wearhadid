<?php
    class Laporan_Reseller extends CI_Controller {

        function __construct(){
            parent::__construct();
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                if($arr[0]!='R'){
                    redirect("Login");
                }
            }
            $this->load->model('Transaksi');
            $this->load->model('Akun');
        }

        public function index()
	    {
            $bln = $this->input->post('bulan');
            $thn = $this->input->post('tahun');
            $hari = $this->input->post('tgl');
            $data['hari'] = $hari;
            $data['bulan'] = $bln;
            $data['tahun'] = $thn;
            $ID=$_SESSION["id"];
            $data['month'] = $this->Transaksi->tampil_data_bln_reseller($bln, $thn, $ID)->result();
            $data['day'] = $this->Transaksi->tampil_data_hari_reseller($hari, $ID)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            $this->load->view('Laporan_Reseller/Laporan.php',$data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
	    }

        public function Cetak_Bulanan_Reseller($bln,$thn)
	    {
            $ID=$_SESSION["id"];
            $data['bulan'] = $bln;
            $data['tahun'] = $thn;
            $data['transaksi'] = $this->Transaksi->tampil_data_bln_reseller($bln, $thn, $ID)->result();
            $this->load->view('Laporan_Reseller/Bulanan_Cetak.php',$data);
	    }

        public function Cetak_Harian_Reseller($tgl)
	    {
            $ID=$_SESSION["id"];
            $data['hari'] = $tgl;
            $data['transaksi'] = $this->Transaksi->tampil_data_hari_reseller($tgl, $ID)->result();
            $this->load->view('Laporan_Reseller/Harian_Cetak.php',$data);
	    }
    }
?>