<?php
    class Cart extends CI_Controller {

        function __construct(){
            parent::__construct();
            //model yang digunakan dalam Controller ini
            $this->load->model('Produk');
            $this->load->model('Transaksi');
            $this->load->model('Notifikasi');
            $this->load->model('Akun');
            $this->load->library('email');
        }

        //bantuan ketika front belum selesai dibuat
        // function index(){
        //     $data['produk'] = $this->Produk->tampil_data_produk()->result();
        //     $this->load->view('Bantuan_Cart.php', $data);
        // }

        //proses menambahkan data kedalam database cart 
        function Add_Cart($IDBarang){
            // melihat id yang login
            $ID=$_SESSION["id"];
            //memasukan data barang dalam bentuk array
            $cart = array(
                "IDCart" => "",
                "IDProduct" => $IDBarang,
                "IDMember" => $ID,
                "Qty" => '1',
            );

            //echo $data = $this->Transaksi->checkcart($ID,$IDBarang)->num_rows();

            // memeriksa jika member sudah login atau belum
            if($ID){
                //jika sudah maka dilihat apakah barang sudah masuk dalam cart atau belum menghindari 
                // double data
                $total1 = $this->Akun->Detail_Admin($ID)->num_rows();
                $total2 = $this->Akun->Detail_Reseller($ID)->result();
                if($this->Transaksi->checkcart($ID,$IDBarang)->num_rows() <= 0){
                    //jika belum maka data bisa langsung dimasukan dalam cart 
                    if($total1 > 0){
                        //jika sudah berhasil dimasukan akan pindah pada bagian landing page
                        $this->session->set_flashdata("error","Admin Tidak Dapat Menambah cart");
                        redirect(site_url("Front/Detail_Product/").$IDBarang);
                    }else if($total1 > 0){
                        //jika sudah berhasil dimasukan akan pindah pada bagian landing page
                        $this->session->set_flashdata("error","Reseller Tidak Dapat Menambah cart");
                        redirect(site_url("Front/Detail_Product/").$IDBarang);
                    }else if($this->Transaksi->insertcart($cart)){
                        redirect(site_url("Front"));
                    }
                }
                //jika barang sudah ada maka halaman menampilkan informasi baru barang sudah ada dalam cart 
                else
                {
                    $this->session->set_flashdata("error","Product sudah ada pada cart");
                    redirect(site_url("Front/Detail_Product/").$IDBarang);
                }
            }
            //jika belum login akan dipindahkan untuk login terlebih dahulu
            else
            {
                redirect(site_url("Login"));
            }
        }

        function Checkout(){
            //melihat akun yang aktif
            $ID=$_SESSION["id"];
            //mngambil semua data cart
            $data = $this->Transaksi->tampil_data_cart($ID)->result();
            //periksa jumlah banyaknya data di cart
            $jmlhdt = $this->Transaksi->tampil_data_cart($ID)->num_rows();

            //pembuatan atau generate ID Order
            $tgl = date('Y-m-d');
            $arr1 = preg_replace("/[^0-9]/", "", $tgl);
            $notransaksi = "";
            $count = $this->Transaksi->CheckTanggalOrder($ID,$tgl)->num_rows();
            if($count == 0){
                $notransaksi = 'T'.$ID.$arr1.'1';
            }else{
                $dt=$this->Transaksi->CheckTanggalOrder($ID,$tgl)->result();
                $bantuan_NoPenjualan="";
                $i=0;
                foreach($dt as $s){
                    $Penjualan = $s->IDOrder;
                    if($bantuan_NoPenjualan != $Penjualan){
                        $bantuan_NoPenjualan = $Penjualan;
                        $i = $i + 1;
                    }
                }
                $i++;
                $notransaksi = 'T'.$ID.$arr1.$i;
            }

            //data pengiriman order
            $province = $this->input->post("provinsi");
            $city = $this->input->post("kota");
            $district = $this->input->post("kecamatan");
            $almt = $this->input->post("address");
            $dtlalmt = $this->input->post("detailaddress");
            $pc = $this->input->post("postalcode");

            $no=0;
            //perulangan buat seluruh data tabel cart
            foreach($data as $u){
                $no++;
                $totalprice = 0;
                $idcart=$u->IDCart;
                $idproduct=$u->IDProduct;
                $price=$u->Price;
                $qtyfix = $u->Qty;
                $totalprice = $qtyfix * $price;

                $penjualan = array(
                    "No" => "",
                    "IDOrder" =>  $notransaksi,
                    "IDProduct" => $idproduct,
                    "IDMember" => $_SESSION["id"],
                    "Province" => $province,
                    "City" => $city,
                    "Districts" => $district,
                    "Address" => $almt,
                    "Detail_Address" => $dtlalmt,
                    "Postal_Code" => $pc,
                    "Qty" => $qtyfix,
                    "Shipping_Cost" => "",
                    "Total_Price" => $totalprice, 
                    "Status" => 'Order',    
                    "Date" => $tgl
                );
                //memasukan data kedalam tabel order
                $this->Transaksi->insertdataorder($penjualan);

                //menghapus data yang ada dalam cart karena sudah berpindah kedalam order
                $this->Transaksi->deletecart($idcart);
            }

            //Kirim Email Member
            // $dt = $this->Akun->Detail_Member($ID)->row();

            // $to = $dt->Email;
            // $nama = $dt->Name;
            // $username = $dt->Username;
            // $passmd = $dt->Password;

            // $subject = 'Konfirmasi Pemesanan Barang';

            // //isi email
            // $input_msg = '<html>
            //                 <body>
            //                     <h4> Hallo '.$nama.',</h4>
            //                     <table>
            //                         <tr width=10px>
            //                             <td style="font-size:13px;">
            //                                Pesanan Anda telah Kami Terima Dengan ID : '.$notransaksi.'. Pada Tanggal '.$tgl.'
            //                             </td>
            //                         </tr>
            //                     </table>
            //                     <table>
            //                         <tr>
            //                             <td colspan=4 style="font-size:12px;">
            //                                <p>Segera lakukan pembayaran pesananmu dengan detail sebagai berikut : <p>
            //                             </td>
            //                         </tr>
            //                         <tr>
            //                             <td colspan=4 style="font-size:12px;">
            //                                 Ringkasan Pembayaran
            //                             </td>
            //                         </tr>
            //                     </table>
            //                     <table border=1>
            //                         <tr>
            //                             <td><b>Barang</b></td>
            //                             <td><b>QTY</b></td>
            //                             <td><b>Harga</b></td>
            //                             <td><b>Total Harga</b> </td>
            //                         </tr>'; foreach($data as $u){
            //                             $qty = $u->Qty; 
            //                             $harga = $u->Price;
            //                             $total_harga = $qty * $harga;
            //                             $total_harga_cart = $total_harga_cart + $total_harga;
            //                             $total_seluruhnya = $total_seluruhnya + $total_harga_cart;
            //                         $input_msg .='<tr>
            //                             <td class="text-left">'.$u->Name.'</td>
    	    //                             <td>'.$u->Qty.'</td>
            //                             <td>Rp. '.number_format($u->Price,0,',','.').'</td>
            //                             <td>Rp. '.number_format($total_harga ,0,',','.').'</td>
            //                         </tr>';
            //                         }
            //                     $input_msg.='
            //                         <tr>
            //                             <td colspan=3><b>Total Biaya</b></td>
            //                             <td>Rp. '.number_format($total_seluruhnya,0,',','.').'</td>
            //                         </tr>
            //                     </table>
            //                     <table>
            //                         <tr>
            //                             <td>Alamat</td>
            //                             <td> : </td>
            //                             <td>'.$almt.'</td>
            //                         </tr>
            //                         <tr>
            //                             <td>
            //                                 Detail
            //                             </td>
            //                             <td> : </td>
            //                             <td>
            //                                 '.$dtlalmt.'
            //                             </td>
            //                         </tr>
            //                         <tr>
            //                             <td>
            //                                 Kodepos
            //                             </td>
            //                             <td> : </td>
            //                             <td>
            //                                 '.$pc.'
            //                             </td>
            //                         </tr>
            //                         <tr>
            //                             <td colspan=3>
            //                                 Biaya Ongkos Kirim Akan Ditanggu Pembeli Dengan Sistem COD (Untuk Biaya Pengiriman)
            //                             </td>
            //                         </tr>
            //                         <tr>
            //                             <td align=center colspan=3>
            //                                 <br>
            //                                 <div text-align="center">Pembayaran Dapat Melalui BANK MANDIRI</div>
            //                                 <div text-align="center">Dengan No. Rekening : 1320005708398</div>
            //                                 <div text-align="center">A.T SATRIO ANANDA SETIAWAN</div>
            //                                 <div text-align="center">Bukti Pembayaran Dapat Diunggah Melalui</div>
            //                                 <div text-align="center">Link Berikut : <a href="http://localhost/Wearhadid/Cart/upload_bukti"> Link </a></div>
            //                             </td>
            //                         </tr>
            //                     </table>
            //                 </body>
            //             ';   
            // //$msg = $header_message.$input_msg.$footer_message;
            // $from = "Wearhadid";

            // $config['protocol']     = 'smtp';
            // $config['smtp_host']    = 'ssl://smtp.gmail.com';
            // $config['smtp_port']    = '465';
            // $config['smtp_timeout'] = '7';
            // $config['smtp_user']    = 'satriosetiawan99@gmail.com';
            // $config['smtp_pass']    = '198237setiawan';
            // $config['charset']      = 'utf-8';
            // $config['newline']      = "\r\n";
            // $config['mailtype']     = 'html'; 
            // $config['validation']   = TRUE; 

            // $this->email->initialize($config);
            // $this->email->from('satriosetiawan99@gmail.com', 'Admin');
            // $this->email->to($to); 
            // $this->email->subject($subject);
            // $this->email->message($input_msg);  
            // $this->email->send();
            // $this->email->print_debugger();


            //Memasukan informasi untuk Notifikasi
            $kalimat_notifikasi = "Terdapat Pesanan Baru Dengan ID ".$notransaksi." Mohon Periksa Pesanan Tersebut";
            $notifikasi = array(
                "No" => "",
                "ID" => $notransaksi,
                "Value_Notification_Order" => $kalimat_notifikasi,
                "Status" => "not seen",
                "Date" =>$tgl
            );
            $this->Notifikasi->insertnotifikasi($notifikasi);


            //redirect website ketika sudah melakukan check out
            if($no == $jmlhdt){
                redirect(site_url("Front"));
            }
        }
        
        //melakukan update 
        function update(){
            $cartid=$this->input->post("cartid");
            //$cartid=1;
            $qty = array(
                "Qty"=> $this->input->post("qty")
                //"Qty" =>5
            );
            $this->Transaksi->updatecart($cartid,$qty);
        }

        //menghapus product pada cart
        public function delete($ID){ 
            $data = $this->Transaksi->tampil_data_cart_id($ID)->row();
            $IDMember = $data->IDMember;
            $this->Transaksi->deletecart($ID);
            redirect(site_url("cart/cart_list/").$IDMember); 
        }

        //melihat isi cart
        function cart_list($ID){
            //$ID= $this->input->post("id");
            //$ID = "M001";
            $data['member'] = $this->Akun->Detail_Member($ID)->result();
            $data['cart'] = $this->Transaksi->tampil_data_cart($ID)->result();
            $this->load->view('Front/pages/cart',$data);
        }

        //Halaman ketika sudah melakukan checkout
        function tabelcheckout($ID){
            $data['cart'] =  $this->Transaksi->tampil_data_Order_Detail($ID)->result();
            $this->load->view('Front/pages/tabelcheckout',$data);
        }
        
        //cetak invoice ketika checkout
        function cetak_invoice($ID){
            $data['checkout'] =  $this->Transaksi->tampil_data_Order_Detail($ID)->result();
            $this->load->view('Front/pages/cetak_invoice',$data);
        }

        //Halaman upload bukit pembayaran
        function upload_bukti(){
            $this->load->view('Front/pages/Upload_Bukti');
        }

        //proses upload bukti kedalam data base
        function proses_upload_bukti(){
            $Id=$this->input->post("Id");
            $data = $this->Transaksi->tampil_data_Order_Detail($Id)->result();
            foreach($data as $u){
                $Image = $u->ImageSource;
            }
            echo $Image;

            if(!$Image){
                $foto = $_FILES['gambar'];
                $config['upload_path'] = './assets/dist/img'; 
                $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
                
                $this->load->library('upload', $config); 
                if (!$this->upload->do_upload('gambar')) { 
                    $foto="";
                } else { 
                    $foto = $this->upload->data('file_name');
                } 

                echo $foto;
                
                $order = array(
                    "ImageSource" => $foto
                );

                if($this->Transaksi->Update_Bukti_Trans($Id,$order)){ 
                    redirect(site_url("Front")); 
                }else{ 
                    redirect(site_url("Cart/upload_bukti")); 
                } 
            }else{
                $this->session->set_flashdata("error","Anda Telah mengupload Bukti Transfer untuk order ini");
                redirect(site_url("Cart/upload_bukti"));
            }
        }

        //Halaman upload bukit pembayaran
        function upload_ulasan($ID){
            $data['id']= $ID;
            $this->load->view('Front/pages/Upload_Ulasan',$data);
        }

        //proses upload bukti kedalam data base
        function proses_upload_ulasan($ID){
            $ulasan = $this->input->post("ulasan");
            $data = $this->Transaksi->tampil_data_Order_Detail($ID)->result();
            foreach($data as $u){
                $Image = $u->ImageUlasan;
            }
            echo $Image;

            if(!$Image){
                $foto = $_FILES['gambar'];
                $config['upload_path'] = './assets/dist/img'; 
                $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
                
                $this->load->library('upload', $config); 
                if (!$this->upload->do_upload('gambar')) { 
                    $foto="";
                } else { 
                    $foto = $this->upload->data('file_name');
                } 

                echo $foto;
                
                $order = array(
                    "Ulasan" => $ulasan,
                    "ImageUlasan" => $foto
                );

                if($this->Transaksi->Update_Bukti_Trans($ID,$order)){ 
                    redirect(site_url("Front")); 
                }else{ 
                    redirect(site_url("Cart/upload_ulasan")); 
                } 
            }else{
                $this->session->set_flashdata("error","Anda Telah mengupload Ulasan untuk order ini");
                redirect(site_url("Cart/upload_ulasan"));
            }
        }
    }
?>