<?php
    class Kelola_User extends CI_Controller {

        function __construct(){
            parent::__construct();
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                if($arr[0]!='A'){
                    redirect("Login");
                }
            }
            $this->load->model('Akun');
            $this->load->model('Notifikasi');
            $this->load->library('email');
        }
        #region Halaman Utama
        public function index()
	    {
            $data['admin'] = $this->Akun->tampil_data_admin()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
		    $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Kelola_admin.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
	    }

        public function Admin()
	    {
            $data['admin'] = $this->Akun->tampil_data_admin()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
		    $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php', $data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Kelola_admin.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
	    }

        public function Reseller()
	    {
            $data['reseller'] = $this->Akun->tampil_data_reseller()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
		    $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Kelola_Reseller.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
	    }

        public function Member()
	    {
            $data['member'] = $this->Akun->tampil_data_member()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
		    $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Kelola_Member.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
	    }
        #endregion

        #region Halaman Tambah Akun
        public function TambahAdmin()
        {
            $data = $this->Akun->tampil_data_admin()->last_row();
            $ID = $data->IDAdmin;
            $arr1 = str_split($ID);
            $length = count($arr1);
            if($arr1[3] == 9){
                $arr1[3] = 0;
                $arr1[2] = $arr1[2]+1;
            }
            else{
                $arr1[3] = $arr1[3]+1;
            }

            if($arr1[2] == 9){
                $arr1[3] = 0;
                $arr1[2] = 0;
                $arr1[1]+1;
            }

            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            $dt["ID"]=$id;
            $dt['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $dt['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$dt);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Tambah_Admin.php', $dt);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function TambahReseller()
        {
            $data = $this->Akun->tampil_data_reseller()->last_row();
            $ID = $data->IDReseller;
            $arr1 = str_split($ID);
            $length = count($arr1);
            if($arr1[3] == 9){
                $arr1[3] = 0;
                $arr1[2] = $arr1[2]+1;
            }
            else{
                $arr1[3] = $arr1[3]+1;
            }

            if($arr1[2] == 9){
                $arr1[3] = 0;
                $arr1[2] = 0;
                $arr1[1]+1;
            }

            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            $dt["ID"]=$id;
            $dt['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $dt['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$dt);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Tambah_Reseller.php', $dt);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function TambahMember()
        {
            $data = $this->Akun->tampil_data_member()->last_row();
            $ID = $data->IDMember;
            $arr1 = str_split($ID);
            $length = count($arr1);
            if($arr1[3] == 9){
                $arr1[3] = 0;
                $arr1[2] = $arr1[2]+1;
            }
            else{
                $arr1[3] = $arr1[3]+1;
            }

            if($arr1[2] == 9){
                $arr1[3] = 0;
                $arr1[2] = 0;
                $arr1[1]+1;
            }

            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            $dt["ID"]=$id;
            $dt['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $dt['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$dt);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Tambah_Member.php', $dt);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion
        
        #region Proses Tambah Akun
        public function ProsesTambahAdmin(){
            $data = $this->Akun->tampil_data_admin()->last_row();
            $ID = $data->IDAdmin;
            $arr1 = str_split($ID);
            $length = count($arr1);
            $arr1[3] = $arr1[3]+1;
            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            if($this->Akun->insertAdmin($id)){ 
                redirect(site_url("Kelola_User/Admin")); 
            }else{
                redirect(site_url("Kelola_User/TambahAdmin")); 
            }
        }
        #endregion

        public function ProsesTambahReseller(){
            $data = $this->Akun->tampil_data_reseller()->last_row();
            $ID = $data->IDReseller;
            $arr1 = str_split($ID);
            $length = count($arr1);
            $arr1[3] = $arr1[3]+1;
            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            $status="aktif";
            if($this->Akun->insertReseller($id,$status)){ 
                redirect(site_url("Kelola_User/Reseller")); 
            }else{
                redirect(site_url("Kelola_User/TambahReseller")); 
            }
        }

        public function ProsesTambahMember(){
            $data = $this->Akun->tampil_data_member()->last_row();
            $ID = $data->IDMember;
            $arr1 = str_split($ID);
            $length = count($arr1);
            $arr1[3] = $arr1[3]+1;
            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            if($this->Akun->insertMember($id)){ 
                redirect(site_url("Kelola_User/Member")); 
            }else{
                redirect(site_url("Kelola_User/TambahMember")); 
            }
        }
        #endregion

        #region Detail Akun
        public function DetailAdmin($ID)
        {
            $data['admin'] = $this->Akun->Detail_Admin($ID)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Detail_Admin.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function DetailReseller($ID)
        {
            $data['reseller'] = $this->Akun->Detail_Reseller($ID)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Detail_Reseller.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function DetailMember($ID)
        {
            $data['member'] = $this->Akun->Detail_Member($ID)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Detail_Member.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion

        #region Halaman Edit Akun
        public function EditAdmin($ID)
        {
            $data['admin'] = $this->Akun->Detail_Admin($ID)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Edit_Admin.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function EditReseller($ID)
        {
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $data['reseller'] = $this->Akun->Detail_Reseller($ID)->result();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Edit_Reseller.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function EditMember($ID)
        {
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $data['member'] = $this->Akun->Detail_Member($ID)->result();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_User/Edit_Member.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion
        #region Proses Edit Akun
        public function ProsesEditAdmin($ID){
            if($this->Akun->Edit_Admin($ID)){ 
                redirect(site_url("Kelola_User/Admin")); 
            }else{
                redirect(site_url("Kelola_User/EditAdmin/".$ID)); 
            }
        }
        
        public function ProsesEditReseller($ID){

            // $status = $this->input->post("status");

            // if($status == "Aktif"){
            //     $notifikasi = array(
            //         "Status" => "seen"
            //     );
            //     $this->Notifikasi->Edit_Notifikasi_Order_Reseller($ID,$notifikasi);

            //     $to = $this->input->post('email');
            //     $nama = $this->input->post('nama');
            //     $username = $this->input->post('usernama');
            //     $subject = 'Konfirmasi Akun Reseller Wearhadid';
            //     $header_message = '<html><head><title>'.$subject.'</title></head><body>';
            //     $footer_message = '</body></html>';
            //     $input_msg = '<h5>Selamat Kepada <b>'.$nama.'</b></h5>  <h5>Akun Telah Diaktifkan dan Anda dinyatakan sebagai Reseller Wearhadid dengan username : <b>'.$username.'</b> dan password yang telah anda tentukan sebelumnya </h5>  <br>   <h5>Silakan Login pada Link Berikut : <a href="http://localhost/Wearhadid/Login">Login </a>';   
            //     $msg = $header_message.$input_msg.$footer_message;
            //     $from = "Wearhadid";

            //     $config['protocol']    = 'smtp';
            //     $config['smtp_host']    = 'ssl://smtp.gmail.com';
            //     $config['smtp_port']    = '465';
            //     $config['smtp_timeout'] = '7';
            //     $config['smtp_user']    = 'satriosetiawan99@gmail.com';
            //     $config['smtp_pass']    = '198237setiawan';
            //     $config['charset']    = 'utf-8';
            //     $config['newline']    = "\r\n";
            //     $config['mailtype'] = 'html'; // or html
            //     $config['validation'] = TRUE; // bool whether to validate email or not      

            //     $this->email->initialize($config);
            //     $this->email->from('satriosetiawan99@gmail.com', 'Admin');
            //     $this->email->to($to); 
            //     $this->email->subject($subject);
            //     $this->email->message($msg);  
            //     $this->email->send();

            //     $this->email->print_debugger();
            // }

            if($this->Akun->Edit_Reseller($ID)){ 
                redirect(site_url("Kelola_User/Reseller")); 
            }else{
                redirect(site_url("Kelola_User/EditAdmin/".$ID)); 
            }
        }

        public function ProsesEditMember($ID){
            if($this->Akun->Edit_Member($ID)){ 
                redirect(site_url("Kelola_User/Member")); 
            }else{
                redirect(site_url("Kelola_User/EditMember/".$ID)); 
            }
        }
        
        #endregion
        #region Proses Hapus Akun
        public function ProsesHapusAdmin($ID){ 
            $this->Akun->Hapus_Admin($ID); 
            redirect(site_url("Kelola_User/Admin")); 
        } 

        public function ProsesHapusReseller($ID){ 
            $this->Akun->Hapus_Reseller($ID); 
            redirect(site_url("Kelola_User/Reseller")); 
        }
        
        public function ProsesHapusMember($ID){ 
            $this->Akun->Hapus_Member($ID); 
            redirect(site_url("Kelola_User/Member")); 
        }
        #endregion
        
        public function Accept_Reseller($ID){
            $data = $this->Akun->Detail_Reseller($ID)->row();

            $to = $data->Email;
            $nama = $data->Name;
            $username = $data->Username;
            $passmd = $data->Password;

            $subject = 'Konfirmasi Akun Reseller Wearhadid';
            $header_message = '<html><head><title>'.$subject.'</title></head><body>';
            $footer_message = '</body></html>';
            $input_msg = '<h5>Konfirmasi Akun Reseller Atas Nama <b>'.$nama.'</b></h5>  <h5>Selamat! Akun ada telah berhasil terkonfirmasi dengan informasi akun sebagai berikut : <br> username : <b>'.$username.'</b></h5>  <br>   <h5>Silakan Login Melalui Link Berikut : <a href="http://localhost/Wearhadid/Login"> Login </a>';   
            $msg = $header_message.$input_msg.$footer_message;
            $from = "Wearhadid";

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'satriosetiawan99@gmail.com';
            $config['smtp_pass']    = '198237setiawan';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'html'; 
            $config['validation']   = TRUE; 

            $this->email->initialize($config);
            $this->email->from('satriosetiawan99@gmail.com', 'Admin');
            $this->email->to($to); 
            $this->email->subject($subject);
            $this->email->message($msg);  
            $this->email->send();
            $this->email->print_debugger();

            $notifikasi = array(
                "Status" => "seen"
            );
            $this->Notifikasi->Edit_Notifikasi_Order_Reseller($ID,$notifikasi);

            $Reseller = array(
                "Status" => "Aktif"
            );
            if($this->Akun->Edit_Reseller_Accept($ID,$Reseller)){ 
                redirect(site_url("Kelola_User/Reseller")); 
            }
        }

        public function Reject_Reseller($ID){
            $data = $this->Akun->Detail_Reseller($ID)->row();
            
            $to = $data->Email;
            $nama = $data->Name;
            $username = $data->Username;
            $subject = 'Konfirmasi Akun Reseller Wearhadid';
            $header_message = '<html><head><title>'.$subject.'</title></head><body>';
            $footer_message = '</body></html>';
            $input_msg = '<h5>Mohon Maaf Kepada <b>'.$nama.'</b></h5>  <h5>Akun Anda Tidak diterima sebagai Reseller Wearhadid dengan <br> username : <b>'.$username.'</b> <br> Karena Alasan Tertentu </h5>  <h5>Mohon Maaf Sebelumnya dan Terima Kasih Telah Mendaftar Menjadi Reseller Wearhadid';   
            $msg = $header_message.$input_msg.$footer_message;
            $from = "Wearhadid";

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'satriosetiawan99@gmail.com';
            $config['smtp_pass']    = '198237setiawan';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'html'; 
            $config['validation']   = TRUE; 

            $this->email->initialize($config);
            $this->email->from('satriosetiawan99@gmail.com', 'Admin');
            $this->email->to($to); 
            $this->email->subject($subject);
            $this->email->message($msg);  
            $this->email->send();
            $this->email->print_debugger();

            $notifikasi = array(
                "Status" => "seen"
            );
            $this->Notifikasi->Edit_Notifikasi_Order_Reseller($ID,$notifikasi);

            $this->Akun->Hapus_Reseller($ID); 
            redirect(site_url("Back_Admin"));
        }
    }
?>