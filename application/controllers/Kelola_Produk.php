<?php
    class Kelola_Produk extends CI_Controller {

        function __construct(){
            parent::__construct();
            //Tidak Dapat mengakses halaman back-end admin jika belum login
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                //jika yang login bukan admin maka akan pindah pada halaman login
                if($arr[0]!='A'){
                    redirect("Login");
                }
            }
            //Model (MVC) yang digunakan dalam Controller ini
            $this->load->model('Produk');
            $this->load->model('Notifikasi');
        }

        #region Halaman Utama 
        //Halaman Produk
        public function Produk(){
            $data['produk'] = $this->Produk->tampil_data_produk()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
		    $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Kelola_Produk.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        //Halaman Kategori
        public function Kategori(){
            $data['category'] = $this->Produk->tampil_data_kategori()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
		    $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Kelola_Kategori.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion

        #region Halaman Tambah
        //Halaman Tambah Produk barus
        public function TambahProduk()
        {
            $data_bantu = $this->Produk->tampil_data_product()->last_row();
            $ID = $data_bantu->IDProduct;
            $arr1 = str_split($ID);
            //echo $arr1[3];
            $length = count($arr1);
            //satuan
            if($arr1[3] == 9){
                $arr1[3] = 0;
                $arr1[2] = $arr1[2]+1;
            }
            else{
                $arr1[3] = $arr1[3]+1;
            }

            if($arr1[2] == 9){
                $arr1[3] = 0;
                $arr1[2] = 0;
                $arr1[1]+1;
            }
            
            
            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
        
            $data["ID"] = $id;
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $data['category'] = $this->Produk->tampil_data_kategori()->result();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Tambah_Produk.php', $data,);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function TambahKategori()
        {
            $data = $this->Produk->tampil_data_Kategori()->last_row();
            $ID = $data->IDCategory;
            $arr1 = str_split($ID);
            $length = count($arr1);
            if($arr1[3] == 9){
                $arr1[3] = 0;
                $arr1[2] = $arr1[2]+1;
            }
            else{
                $arr1[3] = $arr1[3]+1;
            }

            if($arr1[2] == 9){
                $arr1[3] = 0;
                $arr1[2] = 0;
                $arr1[1]+1;
            }
            
            $No_Awal = 0;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            $dt["ID"] = $id;
            $dt['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $dt['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$dt);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Tambah_Kategori.php', $dt);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion
        #region proses tambah
        function ProsesTambahProduk() 
        {
            $foto = $_FILES['gambar'];

    
            $config['upload_path'] = './assets/dist/img'; 
            $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
            
            $this->load->library('upload', $config); 
            if (!$this->upload->do_upload('gambar')) { 
                $foto="";
            } else { 
                $foto = $this->upload->data('file_name');
            } 

            $harga=$this->input->post("harga");
            $y= strlen($harga);
            $arr1 = str_split($harga);
            $hargafix="";
            for($x=0;$x<$y;$x++){
                if($arr1[$x]!="."){
                    $hargafix = $hargafix.$arr1[$x];
                }
            }
            
            $produk = array(
            "IDProduct" =>  $this->input->post("ID"),
            "IDAdmin" => $_SESSION["id"],
            "ProductName" => $this->input->post("nama"),
            "IDCategory" => $this->input->post("kategori"), 
            "Description" => $this->input->post("deskripsi"),
            "ItemStock" => $this->input->post("stok"),
            "ImageSource" => $foto,
            "Price" => $hargafix
            ); 

            if($this->Produk->insertProduk($produk)){ 
                redirect(site_url("Kelola_Produk/Produk")); 
            }else{ 
                redirect(site_url("Kelola_Produk/TambahProduk")); 
            } 
        }


        public function ProsesTambahKategori(){
            $data = $this->Produk->tampil_data_Kategori()->last_row();
            $ID = $data->IDCategory;
            $arr1 = str_split($ID);
            $length = count($arr1);
            $arr1[3] = $arr1[3]+1;
            $id = "";
            for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
                $id = $id.$arr1[$No_Awal];
            }
            if($this->Produk->insertkategori($id)){ 
                redirect(site_url("Kelola_Produk/Kategori")); 
            }else{
                redirect(site_url("Kelola_Produk/TambahKategori")); 
            }
        }
        #endregion
        #region Detail
        public function DetailProduk($ID)
        {
            $data['produk'] = $this->Produk->Detail_Produk($ID)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Detail_produk.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion
        #region Halaman Edit
        public function EditProduk($ID)
        {
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $data['produk'] = $this->Produk->Detail_Produk_Dasar($ID)->result();
            $data['kategori'] = $this->Produk->tampil_data_Kategori($ID)->result();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Edit_Produk.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function EditKategori($ID)
        {
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $data['category'] = $this->Produk->Detail_Kategori($ID)->result();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php', $data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Kelola_Produk/Edit_Kategori.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
        #endregion
        #region proses edit
        public function ProsesEditProduk($ID){
            $foto = $_FILES['gambar'];
    
            $config['upload_path'] = './assets/dist/img'; 
            $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
            
            $this->load->library('upload', $config);
            if(@$_FILES['gambar']['name'] != null){
                if ($this->upload->do_upload('gambar')) { 
                    $post['gambar'] = $this->upload->data('file_name');
                } 
                // else { 
                //     $foto = $this->upload->data('file_name');
                // } 
            } else{
                $post['gambar'] = null;
            }
            
            if($this->Produk->Edit_Produk($ID,$post)){ 
                redirect(site_url("Kelola_Produk/Produk")); 
            }else{
                redirect(site_url("Kelola_Produk/EditProduk/".$ID)); 
            }
        }

        public function ProsesEditKategori($ID){
            if($this->Produk->Edit_Kategori($ID)){ 
                redirect(site_url("Kelola_Produk/Kategori")); 
            }else{
                redirect(site_url("Kelola_Produk/EditKategori/".$ID)); 
            }
        }
        #endregion
        #region proses hapus
        public function ProsesHapusProduk($ID){ 
            $this->Produk->Hapus_Produk($ID); 
            redirect(site_url("Kelola_Produk/Produk")); 
        }

        public function ProsesHapusKategori($ID){ 
            $this->Produk->Hapus_Kategori($ID); 
            redirect(site_url("Kelola_Produk/Kategori")); 
        }
        #endregion
    }
?>