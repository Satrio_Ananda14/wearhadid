<?php
    class Laporan extends CI_Controller {

        function __construct(){
            parent::__construct();
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                if($arr[0]!='A'){
                    redirect("Login");
                }
            }
            $this->load->model('Transaksi');
            $this->load->model('Akun');
            $this->load->model('Notifikasi');
        }

        public function index()
	    {
            $bln = $this->input->post('bulan');
            $bln2 = $this->input->post('bulan2');
            $thn = $this->input->post('tahun');
            $thn2 = $this->input->post('tahun2');
            $hari = $this->input->post('tgl');
            $hari2 = $this->input->post('tgl2');
            $data['hari'] = $hari;
            $data['bulan'] = $bln;
            $data['tahun'] = $thn;
            $data['hari2'] = $hari2;
            $data['bulan2'] = $bln2;
            $data['tahun2'] = $thn2;
            $data['month'] = $this->Transaksi->tampil_data_tgl($bln, $thn)->result();
            $data['day'] = $this->Transaksi->tampil_data_hari($hari)->result();
            $data['month2'] = $this->Transaksi->tampil_data_tgl_admin($bln2, $thn2)->result();
            $data['day2'] = $this->Transaksi->tampil_data_hari_admin($hari2)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Laporan/Laporan.php',$data);
            $this->load->view('Template/Back_End/Footer_Admin.php');
	    }

        public function Cetak_Bulanan($bln,$thn)
	    {
            $data['bulan'] = $bln;
            $data['tahun'] = $thn;
            $data['transaksi'] = $this->Transaksi->tampil_data_tgl($bln, $thn)->result();
            $this->load->view('Laporan/Bulanan_Cetak.php',$data);
	    }

        public function Cetak_Harian($tgl)
	    {
            $data['hari'] = $tgl;
            $data['transaksi'] = $this->Transaksi->tampil_data_hari($tgl)->result();
            $this->load->view('Laporan/Harian_Cetak.php',$data);
	    }

        public function Cetak_Bulanan_Admin($bln,$thn)
	    {
            $data['bulan'] = $bln;
            $data['tahun'] = $thn;
            $data['transaksi'] = $this->Transaksi->tampil_data_tgl_admin($bln, $thn)->result();
            $this->load->view('Laporan/Bulanan_Cetak_Admin.php',$data);
	    }

        public function Cetak_Harian_Admin($tgl)
	    {
            $data['hari'] = $tgl;
            $data['transaksi'] = $this->Transaksi->tampil_data_hari_admin($tgl)->result();
            $this->load->view('Laporan/Harian_Cetak_Admin.php',$data);
	    }
    }
?>