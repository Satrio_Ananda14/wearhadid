<?php
    class Back_Reseller extends CI_Controller {
    
        function __construct(){
            parent::__construct();
            //Tidak Dapat mengakses halaman back-end admin jika belum login
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                //jika yang login bukan admin maka akan pindah pada halaman login
                if($arr[0]!='R'){
                    redirect("Login");
                }
            }
            //Model (MVC) yang digunakan dalam Controller ini
            $this->load->model('Akun');
            $this->load->model('Produk');
            $this->load->model('Transaksi');
        }

        //tampilan dashboard back-end admin
        public function index()
	    {
            $ID=$_SESSION["id"];
            //daftar produk yang dijual oleh reseller 
            $data['produk']=$this->Produk->tampil_data_Daftarproduk($ID)->result();
            //untuk grafik penjualan dalam reseller tersebut
            $data['Transaksi']=$this->Transaksi->tampil_data_Transaksi($ID)->result();

            //Penjualan Tertinggi Perbulan
            $i=1;
            $j=12;
            $tertinggi = 0;
            for($i;$i<=$j;$i++){
                $bulan[] = $this->Transaksi->tampil_data_bulanan($ID,$i)->row();
                $data['Bulan'] = $bulan;
                $uji = $this->Transaksi->tampil_data_bulanan($ID,$i)->row();
                $tampung[$i] = $uji->Total_Akhir;
                if($tertinggi < $tampung[$i]){
                    $tertinggi = $tampung[$i];
                }
                $tampung[$i];
            }
            $data['Penjualan_Tertinggi_Bulan'] = $tertinggi;
            
            //Get Bulan
            $a=0;
            $b=12;
            for($a;$a<=$b;$a++){
                $time=date("F", mktime(0, 0, 0, $a, 12));
                $Nama_Bulan[]=$time;
                $data['Nama_Bulan'] = $Nama_Bulan;
            }

		    $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            //data yang dimasukan adalah data penjualan (persentasi kenaikan harian dan bulanan)
            $this->load->view('Back_Dasboard/dashboard_reseller.php',$data);
            //data yang dimasukan adalah data penjualan (untuk grafik)
            $this->load->view('Template/Back_End/Footer_Reseller.php',$data);
	    }

        public function EditProfil($ID)
        {
            //isi notifikasi
            $data['reseller'] = $this->Akun->Detail_Reseller($ID)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            //data akun reseller yang login
            $this->load->view('Kelola_User/Edit_Profil_Reseller.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //proses Update data Edit data ketika dilakukan perubahan
        public function ProsesEditReseller($ID){
            //jika berhasil kembali halaman dasboad back-end reseller
            if($this->Akun->Edit_Member($ID)){ 
                redirect(site_url("Back_Reseller")); 
            }
            //jika gagal tetap dalam halaman edit profil
            else
            {
                redirect(site_url("Back_Reseller/EditProfil/".$ID)); 
            }
        }
    }
?>