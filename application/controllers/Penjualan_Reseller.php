<?php
    class Penjualan_Reseller extends CI_Controller {

        function __construct(){
            parent::__construct();
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                if($arr[0]!='A'){
                    redirect("Login");
                }
            }
            $this->load->model('Akun');
            $this->load->model('Transaksi');
            $this->load->model('Produk');
            $this->load->model('Notifikasi');
        }

        function index(){
            $ID=$_SESSION["id"];
            $data['reseller'] = $this->Akun->tampil_data_reseller()->result();
            $data['transaksi'] = $this->Transaksi->tampil_data_alltransaksi()->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Penjualan_Reseller/Penjualan_Reseller.php',$data);
            $this->load->view('Template/Back_End/Footer_Admin.php');
        }

        public function DetailPenjualanReseller($ID)
        {
            $session_data = array(
                "reseller" => $ID
            );
            $this->session->set_userdata($session_data); 
            $data['Nama'] = $ID;
            $data['user'] = $this->Akun->Detail_Reseller($ID)->result();
            $data['transaksi'] = $this->Transaksi->tampil_data_transaksi($ID)->result();
            $data['daftarproduk'] = $this->Produk->tampil_data_Daftarproduk($ID)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Penjualan_Reseller/Detail_Penjualan_Reseller.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        public function DetailPenjualanPertanggal($tanggal){
            $ID=$_SESSION["reseller"];
            $data['user'] = $this->Akun->Detail_Reseller($ID)->result();
            $data['transaksi'] = $this->Transaksi->tampil_data_transaksi_tgl($ID,$tanggal)->result();
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            $this->load->view('Template/Back_End/Header_Admin.php');
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            $this->load->view('Penjualan_Reseller/Detail_Penjualan_Reseller_Pertanggal', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }
    }
?>