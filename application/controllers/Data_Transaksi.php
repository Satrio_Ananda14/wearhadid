<?php
    class Data_Transaksi extends CI_Controller {
      
    function __construct(){
        parent::__construct();
        //Tidak Dapat mengakses halaman back-end admin jika belum login
        if(empty($this->session->userdata('login'))){ 
            $ID=$_SESSION["id"];
            $arr = str_split($ID);
            //jika yang login bukan admin maka akan pindah pada halaman login
            if($arr[0]!='A'){
                redirect("Login");
            }
        }
        //Model (MVC) yang digunakan dalam Controller ini
        $this->load->model('Transaksi');
        $this->load->model('Produk');
        $this->load->model('Notifikasi');
        $this->load->model('Akun');
        $this->load->library('email');
        
    } 

    //halaman Order dan Penjualan Admin
    function index(){
        $data['order'] = $this->Transaksi->tampil_data_Order()->result();
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php',$data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Data_Transaksi/Data_Transaksi.php',$data);
        $this->load->view('Template/Back_End/Footer_Admin.php');
    }

    //Detail Order 
    function Detail_Order($ID){
        //$ID= $this->input->post("idorder");
        //$ID= 'TM002202111011';
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $data['order'] = $this->Transaksi->tampil_data_Order_Detail($ID)->result();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php',$data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Data_Transaksi/Tabel_Detail_Order.php',$data);
        $this->load->view('Template/Back_End/Footer_Admin.php');
    }

    function Detail_Confirmation($ID){
        //$ID= $this->input->post("idorder");
        //$ID= 'TM002202111011';
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $data['order'] = $this->Transaksi->tampil_data_Order_Detail($ID)->result();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php',$data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Data_Transaksi/Tabel_Detail_Confirmation.php',$data);
        $this->load->view('Template/Back_End/Footer_Admin.php');
    }

    //Detail Deliver
    function Detail_Deliver($ID){
        //$ID= $this->input->post("idorder");
        //$ID= 'TM001202110191';
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $data['order'] = $this->Transaksi->tampil_data_Order_Detail($ID)->result();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php',$data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Data_Transaksi/Tabel_Detail_Deliver.php',$data);
        $this->load->view('Template/Back_End/Footer_Admin.php');
    }

    //tabel bukti pembayaran
    function tabelbuktipembayaran(){
        $ID= $this->input->post("idorder");
        //$ID= 'TM001202110191';
        $data['order'] = $this->Transaksi->tampil_data_Order_Detail($ID)->result();
        $this->load->view('Data_Transaksi/Tabel_Bukti_Pembayaran.php',$data);
    }

    //proses order to deliver
    function ProsesKirim($ID){
        $ekspedisi = $this->input->post("nama");

        //Mengurangi Stock Barang
        $data=$this->Transaksi->tampil_data_Order_Detail($ID)->result();
        $jumlahbarang=$this->Transaksi->tampil_data_Order_Detail($ID)->num_rows();
        $no=0;
        foreach($data as $u){
            $no++;
            $IDProduct = $u->IDProduct;
            $QTY = $u->Qty;
            $dt = $this->Produk->Detail_Produk($IDProduct)->result();
            foreach($dt as $s){
                $QtyStock = $s->ItemStock;
            }
            $sisastock = $QtyStock-$QTY;
            $Stock = array(
                "ItemStock" => $sisastock
            );
            $this->Produk-> Edit_Stok_Produk($IDProduct,$Stock);
        }

        //Ubah Status
        $penjualan = array(
            "Expedition" => $ekspedisi,
            "Status" => 'Deliver'
        );
        $this->Transaksi->order_to_deliver($ID,$penjualan);
        

        //Kirim Email
        $dt = $this->Transaksi->tampil_data_Order_Detail($ID)->row();
        $to = $dt->Email;
        $nama = $dt->MName;
        $almt = $dt->Address;
        $dtlalmt = $dt->Detail_Address;
        $pc = $dt->Postal_Code;

        $subject = 'Konfirmasi Pengiriman Barang';
        $input_msg = '<html>
                        <body>
                            <h4> Hallo '.$nama.',</h4>
                            <table>
                                <tr width=10px>
                                    <td style="font-size:13px;">
                                        Pesanan Anda telah Kami Kirim Dengan ID : '.$ID.'
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td colspan=4 style="font-size:12px;">
                                        <p>Dengan Kode Pengiriman Sebagai Berikut :'.$ekspedisi.'<p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=4 style="font-size:12px;">
                                        Ringkasan Pembelian
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>Alamat</td>
                                    <td> : </td>
                                    <td>'.$almt.'</td>
                                </tr>
                                <tr>
                                    <td> Detail</td>
                                    <td> : </td>
                                    <td>'.$dtlalmt.'</td>
                                </tr>
                                <tr>
                                    <td>Kodepos</td>
                                    <td> : </td>
                                    <td>'.$pc.'</td>
                                </tr>
                            </table>
                            <table border=1>
                                <tr>
                                    <td><b>Barang</b></td>
                                    <td><b>QTY</b></td>
                                    <td><b>Harga</b></td>
                                    <td><b>Total Harga</b> </td>
                                </tr>'; 
                                    $total_harga_cart=0;
                                    $total_seluruhnya=0;
                                    foreach($data as $u){
                                    $qty = $u->Qty; 
                                    $harga = $u->PriceProduct;
                                    $total_harga = $qty * $harga;
                                    $total_harga_cart = $total_harga_cart + $total_harga;
                                    $total_seluruhnya = $total_seluruhnya + $total_harga_cart;
                                $input_msg .='<tr>
                                    <td class="text-left">'.$u->Name.'</td>
    	                            <td>'.$u->Qty.'</td>
                                    <td>Rp. '.number_format($u->PriceProduct,0,',','.').'</td>
                                    <td>Rp. '.number_format($total_harga ,0,',','.').'</td>
                                </tr>';
                                    }
                                $input_msg.='
                                <tr>
                                    <td colspan=3><b>Total Biaya</b></td>
                                    <td>Rp. '.number_format($total_seluruhnya,0,',','.').'</td>
                                </tr>
                            </table>
                            <div text-align="left">Jika Barang Sudah Sampai Anda Dapat Mengirim Ulasan Melalui</div>
                            <div text-align="left">Link Berikut : <a href="http://localhost/Wearhadid/Cart/upload_ulasan/'.$ID.'"> Link </a></div>            
                        </body>
                        ';   
        //$msg = $header_message.$input_msg.$footer_message;
        $from = "Wearhadid";

        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'satriosetiawan99@gmail.com';
        $config['smtp_pass']    = '198237setiawan';
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $config['mailtype']     = 'html'; 
        $config['validation']   = TRUE; 

        $this->email->initialize($config);
        $this->email->from('satriosetiawan99@gmail.com', 'Admin');
        $this->email->to($to); 
        $this->email->subject($subject);
        $this->email->message($input_msg);  
        $this->email->send();
        $this->email->print_debugger();

        //Ubah Notifikasi Menjadi Hilang
        $notifikasi = array(
            "Status" => "seen"
        );
        $this->Notifikasi->Edit_Notifikasi_Order_Reseller($ID,$notifikasi);
        
        if($no == $jumlahbarang){
            redirect(site_url("Data_Transaksi"));
        }   
    }

    function ProsesKonfirmasi($ID){
        $harga = $this->input->post("ongkir");

        $y= strlen($harga);
        $arr1 = str_split($harga);
        $hargafix="";
        for($x=0;$x<$y;$x++){
            if($arr1[$x]!="."){
                $hargafix = $hargafix.$arr1[$x];
            }
        }

        //Ubah Status
        $penjualan = array(
            "Shipping_Cost" => $hargafix,
            "Status" => 'Confir'
        );
        $this->Transaksi->order_to_deliver($ID,$penjualan);

        $data=$this->Transaksi->tampil_data_Order_Detail($ID)->result();
        foreach($data as $u){
            $Id = $u->IDMember;
            $tgl = $u->Date;
            $province = $u->Province;
            $city = $u->City;
            $district = $u->Districts;
            $almt = $u->Address;
            $dtlalmt = $u->Detail_Address;
            $pc = $u->Postal_Code;
        }


        //Kirim Email Member
            $dt = $this->Akun->Detail_Member($Id)->row();

            $to = $dt->Email;
            $nama = $dt->Name;
            $username = $dt->Username;
            $passmd = $dt->Password;
            $total_harga_cart = 0;
            $total_seluruhnya = 0;

            $subject = 'Konfirmasi Pemesanan Barang';

            //isi email
            $input_msg = '<html>
                            <body>
                                <h4> Hallo '.$nama.',</h4>
                                <table>
                                    <tr width=10px>
                                        <td style="font-size:13px;">
                                           Pesanan Anda telah Kami Terima Dengan ID : '.$ID.'. Pada Tanggal '.$tgl.'
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td colspan=4 style="font-size:12px;">
                                           <p>Segera lakukan pembayaran pesananmu dengan detail sebagai berikut : <p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=4 style="font-size:12px;">
                                            Ringkasan Pembayaran
                                        </td>
                                    </tr>
                                </table>
                                <table border=1>
                                    <tr>
                                        <td><b>Barang</b></td>
                                        <td><b>QTY</b></td>
                                        <td><b>Harga</b></td>
                                        <td><b>Total Harga</b> </td>
                                    </tr>'; foreach($data as $u){
                                        $qty = $u->Qty; 
                                        //$harga = $u->Price;
                                        $IDProduct = $u->IDProduct;
                                        //$total_harga = $qty * $harga;
                                        $total_harga_cart = $total_harga_cart + $u->Total_Price;
                                        $total_seluruhnya = $total_seluruhnya + $total_harga_cart;
                                    $input_msg .='<tr>
                                        <td class="text-left">'.$u->Name.'</td>
    	                                <td>'.$u->Qty.'</td>
                                        <td>Rp. '.number_format($u->PriceProduct,0,',','.').'</td>
                                        <td>Rp. '.number_format($u->Total_Price ,0,',','.').'</td>
                                    </tr>';
                                    }
                                $input_msg.='
                                    <tr>
                                        <td colspan=3><b>Total Ongkos Kirim</b></td>
                                        <td>Rp. '.number_format($hargafix,0,',','.').'</td>
                                    </tr>
                                    <tr>
                                        <td colspan=3><b>Total Biaya</b></td>
                                        <td>Rp. '.number_format($total_seluruhnya+$hargafix,0,',','.').'</td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td> : </td>
                                        <td>'.$province.'</td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td> : </td>
                                        <td>'.$city.'</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td> : </td>
                                        <td>'.$district.'</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td> : </td>
                                        <td>'.$almt.'</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Detail
                                        </td>
                                        <td> : </td>
                                        <td>
                                            '.$dtlalmt.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kodepos
                                        </td>
                                        <td> : </td>
                                        <td>
                                            '.$pc.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=3>
                                            Biaya Ongkos Kirim Akan Ditanggu Pembeli Dengan Sistem COD (Untuk Biaya Pengiriman)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align=center colspan=3>
                                            <br>
                                            <div text-align="center">Pembayaran Dapat Melalui BANK MANDIRI</div>
                                            <div text-align="center">Dengan No. Rekening : 1320005708398</div>
                                            <div text-align="center">A.T SATRIO ANANDA SETIAWAN</div>
                                            <div text-align="center">Bukti Pembayaran Dapat Diunggah Melalui</div>
                                            <div text-align="center">Link Berikut : <a href="http://localhost/Wearhadid/Cart/upload_bukti"> Link </a></div>
                                            <div text-align="center">Invoice Pemesanan</div>
                                            <div text-align="center">Link Berikut : <a href="http://localhost/Wearhadid/Cart/cetak_invoice/'.$ID.'"> Link </a></div>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        ';   
            //$msg = $header_message.$input_msg.$footer_message;
            $from = "Wearhadid";

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'satriosetiawan99@gmail.com';
            $config['smtp_pass']    = '198237setiawan';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'html'; 
            $config['validation']   = TRUE; 

            $this->email->initialize($config);
            $this->email->from('satriosetiawan99@gmail.com', 'Admin');
            $this->email->to($to); 
            $this->email->subject($subject);
            $this->email->message($input_msg);  
            $this->email->send();
            $this->email->print_debugger();

            redirect(site_url("Data_Transaksi"));
    }

    //proses hapus Order (Order Ditolak)
    function ProsesHapusOrder($ID){
        $data = $this->Transaksi->tampil_data_Order_Detail($ID)->result();
        $dt = $this->Transaksi->tampil_data_Order_Detail($ID)->row();
        $to = $dt->Email;
        $nama = $dt->MName;
        $almt = $dt->Address;
        $dtlalmt = $dt->Detail_Address;
        $pc = $dt->Postal_Code;

        $subject = 'Konfirmasi Order Barang';
        $input_msg = '<html>
                        <body>
                            <h4> Hallo '.$nama.',</h4>
                            <table>
                                <tr width=10px>
                                    <td style="font-size:13px;">
                                        Mohon Maaf Pesanan yang Anda Ajukan dengan ID : '.$ID.'. Kami Tolak Karena Tidak Melakukan Pembayaran ataupun stok barang sudah habis. 
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td colspan=4 style="font-size:12px;">
                                        Ringkasan Order
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>Alamat</td>
                                    <td> : </td>
                                    <td>'.$almt.'</td>
                                </tr>
                                <tr>
                                    <td> Detail</td>
                                    <td> : </td>
                                    <td>'.$dtlalmt.'</td>
                                </tr>
                                <tr>
                                    <td>Kodepos</td>
                                    <td> : </td>
                                    <td>'.$pc.'</td>
                                </tr>
                            </table>
                            <table border=1>
                                <tr>
                                    <td><b>Barang</b></td>
                                    <td><b>QTY</b></td>
                                    <td><b>Harga</b></td>
                                    <td><b>Total Harga</b></td>
                                </tr>'; 
                                    $total_harga_cart=0;
                                    $total_seluruhnya=0;
                                    foreach($data as $u){
                                    $qty = $u->Qty; 
                                    $harga = $u->PriceProduct;
                                    $total_harga = $qty * $harga;
                                    $total_harga_cart = $total_harga_cart + $total_harga;
                                    $total_seluruhnya = $total_seluruhnya + $total_harga_cart;
                                $input_msg.='
                                <tr>
                                    <td class="text-left">'.$u->Name.'</td>
    	                            <td>'.$u->Qty.'</td>
                                    <td>Rp. '.number_format($u->PriceProduct,0,',','.').'</td>
                                    <td>Rp. '.number_format($total_harga ,0,',','.').'</td>
                                </tr>';
                                    }
                                $input_msg.='
                                <tr>
                                    <td colspan=3><b>Total Biaya</b></td>
                                    <td>Rp. '.number_format($total_seluruhnya,0,',','.').'</td>
                                </tr>
                            </table>
                        </body>
                    ';   

        $from = "Wearhadid";

        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'satriosetiawan99@gmail.com';
        $config['smtp_pass']    = '198237setiawan';
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $config['mailtype']     = 'html'; 
        $config['validation']   = TRUE; 

        $this->email->initialize($config);
        $this->email->from('satriosetiawan99@gmail.com', 'Admin');
        $this->email->to($to); 
        $this->email->subject($subject);
        $this->email->message($input_msg);  
        $this->email->send();
        $this->email->print_debugger();

        $this->Transaksi->deleteorder($ID);
        redirect(site_url("Data_Transaksi")); 

    }
}
?>