<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller{
    // Controller Front End 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Produk');
        $this->load->model('Transaksi');
        $this->load->model('Notifikasi');
        $this->load->model('Akun');
    }
    
    // Home Page
    public function index(){
        if($this->session->userdata('logins')){
            $ID=$_SESSION["id"];
        }else{
            $ID="";
        }
        
        $data['Slider'] = $this->Produk->tampil_data_slider()->result();
        $data['produk'] = $this->Produk->tampil_data_produk()->result();
        $data['cart'] = $this->Transaksi->tampil_data_cart($ID)->result();
        $this->load->view('Front/themes/header_2',$data);
        $this->load->view('Front/pages/home', $data);
        $this->load->view('Front/pages/product', $data);
        $this->load->view('Front/pages/reseller_copy');
        $this->load->view('Front/pages/contact_us_2');
        $this->load->view('Front/themes/footer_2',$data);
    }
    
    //Halaman Register Reseller baru
    public function Register_Reseller(){
        $data = $this->Akun->tampil_data_reseller()->last_row();
        $ID = $data->IDReseller;
        $arr1 = str_split($ID);
        $length = count($arr1);
        $arr1[3] = $arr1[3]+1;
        $No_Awal = 0;
        $id = "";
        for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
            $id = $id.$arr1[$No_Awal];
        }
        $dt["ID"]=$id;
        $this->load->view('Front/pages/join_reseller_form_2',$dt);
    }

    //proses memasukan data kedalam database
    public function ProsesRegistrasiReseller(){
        $id=$this->input->post("id");
        $status = "Non Aktif";

        $kalimat_notifikasi = "Terdapat Pendaftar Reseller Baru Dengan ID ".$id." Mohon Periksa Account Tersebut";
        echo $tgl = date('Y-m-d');
        $notifikasi = array(
            "No" => "",
            "ID" => $id,
            "Value_Notification_Order" => $kalimat_notifikasi,
            "Status" => "not seen",
            "Date" =>$tgl
        );
        $this->Notifikasi->insertnotifikasi($notifikasi);

        if($this->Akun->insertReseller($id,$status)){ 
            redirect(site_url("Front")); 
        }else{
            redirect(site_url("Front/Register_Reseller")); 
        }
    }

    //halaman Detail Prodcut
    public function Detail_Product($ID){
        $data['produk'] = $this->Produk->Detail_Produk($ID)->result();
        $this->load->view('Front/pages/details_product',$data);
    }

    //Halaman edit Slider
	public function Slider(){
        $data['Slider'] = $this->Produk->tampil_data_slider()->result();
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php',$data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Front/setting/setting_slider.php',$data);
        $this->load->view('Template/Back_End/Footer_admin.php');
    }

    //Tambah Slider
    public function TambahSlider(){
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php',$data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Front/setting/setting_tambah_slider.php');
        $this->load->view('Template/Back_End/Footer_admin.php');
    }

    //Upload Slider
    public function ProsesTambahGambar(){
        $foto = $_FILES['gambar'];
    
        $config['upload_path'] = './assets/dist/img'; 
        $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
            
        $this->load->library('upload', $config); 
        if (!$this->upload->do_upload('gambar')) { 
            echo $this->upload->display_errors(); 
        } else { 
            $foto = $this->upload->data('file_name');
        } 

        $Slider = array(
            "No" => "",
            "ImageSource" => $foto,
            "Status" => $this->input->post("Status")
        ); 

        if($this->Produk->insertSlider($Slider)){ 
            redirect(site_url("Front/Slider")); 
        }else{ 
            redirect(site_url("Front/TambahSlider")); 
        } 
    }

    //Halaman Edit Slider
    public function EditSlider($ID)
    {
        $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
        $data['JN'] = $this->Notifikasi->total_notifikasi();
        $data['Slider'] = $this->Produk->Detail_Slider($ID)->result();
        $this->load->view('Template/Back_End/Header_Admin.php');
        $this->load->view('Template/Back_End/Nav_Admin.php', $data);
        $this->load->view('Template/Back_End/Side_Admin.php');
        $this->load->view('Front/setting/setting_Edit_slider.php', $data);
        $this->load->view('Template/Back_End/Footer_admin.php');
    }

    //Halaman Proses Edit
    public function ProsesEditSlider($ID){
        $foto = $_FILES['gambar'];

        $config['upload_path'] = './assets/dist/img'; 
        $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
        
        $this->load->library('upload', $config);
        if($_FILES['gambar']['name'] != null){
            if ($this->upload->do_upload('gambar')) { 
                $post['gambar'] = $this->upload->data('file_name'); 
            }
        }else{
            $post['gambar'] = null;
        }

        if($this->Produk->Edit_Slider($ID,$post)){ 
            redirect(site_url("Front/Slider")); 
        }else{ 
            redirect(site_url("Front/TambahSlider")); 
        }
    }

    //Proses Hapus
    public function ProsesHapusSlider($ID){ 
        $this->Produk->Hapus_Slider($ID); 
        redirect(site_url("Front/Slider")); 
    }

    public function EditAccount(){
        $ID=$_SESSION["id"];
        $data['member'] = $this->Akun->Detail_Member($ID)->result();
        $this->load->view('Front/pages/edit_account',$data);
    }

    public function ProsesEditMember($ID){ 
        if($this->Akun->Edit_Member($ID)){ 
            redirect(site_url("")); 
        }else{
            redirect(site_url("Front/EditAccount".$ID)); 
        }
    }
    
}
?>