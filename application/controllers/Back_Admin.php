<?php
    class Back_Admin extends CI_Controller {
    
        function __construct(){
            parent::__construct();
            //Tidak Dapat mengakses halaman back-end admin jika belum login
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                //jika yang login bukan admin maka akan pindah pada halaman login
                if($arr[0]!='A'){
                    redirect("Login");
                }
            }
            //Model (MVC) yang digunakan dalam Controller ini
            $this->load->model('Akun');
            $this->load->model('Produk');
            $this->load->model('Transaksi');
            $this->load->model('Notifikasi');
        }

        //tampilan dashboard back-end admin
        public function index()
	    {
            $ID=$_SESSION["id"];
            //untuk grafik produk -> Footer admin (javascript grafik)
            $data['produk'] = $this->Produk->tampil_data_produk()->result();
            //untuk grafik Transaksi -> Footer admin (javascript grafik)
            $data['Transaksi'] = $this->Transaksi->tampil_data_Order()->result();
            //isi notifikasi
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            //jumlah notifikasi
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            //untuk akun yang mendaftar sebagai reseller (belum di acc)
            $data['reseller'] = $this->Akun->tampil_data_reseller()->result();
            
            //Penjualan Tertinggi Perbulan
            $i=1;
            $j=12;
            $tertinggi = 0;
            for($i;$i<=$j;$i++){
                $bulan[] = $this->Transaksi->tampil_data_bulanan_admin($i)->row();
                $data['Bulan'] = $bulan;
                $uji = $this->Transaksi->tampil_data_bulanan_admin($i)->row();
                $tampung[$i] = $uji->Total_Akhir;
                if($tertinggi < $tampung[$i]){
                    $tertinggi = $tampung[$i];
                }
                $tampung[$i];
            }
            $data['Penjualan_Tertinggi_Bulan'] = $tertinggi;

            //Get Bulan
            $a=0;
            $b=12;
            for($a;$a<=$b;$a++){
                $time=date("F", mktime(0, 0, 0, $a, 12));
                $Nama_Bulan[]=$time;
                $data['Nama_Bulan'] = $Nama_Bulan;
            }

            //tampilan view dengan template
		    $this->load->view('Template/Back_End/Header_Admin.php');
            //data yang dimasukan adalah jumlah dan isi notifikasi 
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            //data yang dimasukan adalah data penjualan (persentasi kenaikan harian dan bulanan)
            $this->load->view('Back_Dasboard/dashboard_admin.php',$data);
            //data yang dimasukan adalah data penjualan (untuk grafik harian dan bulanan)
            $this->load->view('Template/Back_End/Footer_admin.php',$data);
	    }

        public function EditProfil($ID)
        {
            //isi notifikasi
            $data['Notifikasi'] = $this->Notifikasi->tampil_data_Notifikasi()->result();
            //jumlah notifikasi
            $data['JN'] = $this->Notifikasi->total_notifikasi();
            //data akun yang login
            $data['admin'] = $this->Akun->Detail_Admin($ID)->result();
            $this->load->view('Template/Back_End/Header_Admin.php');
            //data yang dimasukan adalah jumlah dan isi notifikasi 
            $this->load->view('Template/Back_End/Nav_Admin.php',$data);
            $this->load->view('Template/Back_End/Side_Admin.php');
            //data akun admin yang login
            $this->load->view('Kelola_User/Edit_Profil_Admin.php', $data);
            $this->load->view('Template/Back_End/Footer_admin.php');
        }

        //proses Update data Edit data ketika dilakukan perubahan
        public function ProsesEditAdmin($ID){
            //jika berhasil kembali halaman dasboad back-end admin
            if($this->Akun->Edit_Admin($ID)){ 
                redirect(site_url("Back_Admin")); 
            }
            //jika gagal tetap dalam halaman edit profil
            else
            {
                redirect(site_url("Back_Admin/EditProfil/".$ID)); 
            }
        }

        //method ketika menekan notifikasi Order
        public function NotifikasiOrder($ID){
            $notifikasi = array(
                "Status" => "seen"
            );
            $this->Notifikasi->Edit_Notifikasi($ID,$notifikasi);
            redirect(site_url("Data_Transaksi")); 
        }

        //method ketika menekan notifikasi Register Reseller baru
        public function NotifikasiRegisterReseller($ID){
            $notifikasi = array(
                "Status" => "seen"
            );
            $this->Notifikasi->Edit_Notifikasi($ID,$notifikasi);
            redirect(site_url("Back_Admin")); 
        }
    }
?>