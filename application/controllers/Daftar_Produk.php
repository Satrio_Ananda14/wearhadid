<?php
    class Daftar_Produk extends CI_Controller {

        function __construct(){
            parent::__construct();
            //Tidak Dapat mengakses halaman back-end reseller jika belum login
            if(empty($this->session->userdata('login'))){ 
                $ID=$_SESSION["id"];
                $arr = str_split($ID);
                //jika yang login bukan reseller maka akan pindah pada halaman login
                if($arr[0]!='R'){
                    redirect("Login");
                }
            }
            //model yang digunakan
            $this->load->model('Produk');
        }

        //halaman Daftar Produk yang dijual reseller
        function index(){
            //Akun yang login
            $ID=$_SESSION["id"];
            //mendapatkan data product wearhadid
            $dt = $this->Produk->tampil_data_Daftarproduk($ID)->result();
            $Product = array();
            foreach($dt as $u){
                //daftar product
                $Product[] = $u->IRProduct;
            }
            //menampilkan product yang belum dijual oleh reseller 
            $data['produk'] = $this->Produk->daftar_belum_dijual($ID,$Product)->result();
            //data product yang dijual rreseller
            $data['daftarproduk'] = $this->Produk->tampil_data_Daftarproduk($ID)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            //data merupakan data barang yang sudah dijual dan juga belum dijual oleh reseller
            $this->load->view('Daftar_Produk/Daftar_Produk.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //detail informasi untuk tambah produk
        public function TambahDaftarProduk($ID)
        {
            $data['produk'] = $this->Produk->Detail_Produk($ID)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            $this->load->view('Daftar_Produk/Tambah_Daftar_Produk.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //proses tambah daftar produk
        public function ProsesTambahDaftarProduk($ID){
            $data = $this->Produk->Detail_Produk($ID)->row();
            $Category=$data->IDCategory;

            $harga=$this->input->post("harga");
            $y= strlen($harga);
            $arr1 = str_split($harga);
            $hargafix="";
            for($x=0;$x<$y;$x++){
                if($arr1[$x]!="."){
                    $hargafix = $hargafix.$arr1[$x];
                }
            }

            $daftar_produk = array(
                "No" => " ",
                "IDReseller" => $_SESSION["id"],
                "IRProduct" => $ID,
                "ProductName" => $this->input->post("nama"),
                "IRCategory" =>  $Category,
                "Stock" => $this->input->post("stok"),
                "Price" => $hargafix
            );

            if($this->Produk->insertdaftarproduk($daftar_produk)){ 
                redirect(site_url("Daftar_Produk")); 
            }else{
                redirect(site_url("Daftar_Produk/Tambah_Daftar_Produk")); 
            }
        }

        //edit data daftar produk
        public function EditDaftarProduk($ID){
            $data['daftarproduk'] = $this->Produk->Detail_Daftar_Produk($ID)->result();
            $this->load->view('Template/Back_End/Header_Reseller.php');
            $this->load->view('Template/Back_End/Nav_Reseller.php');
            $this->load->view('Template/Back_End/Side_Reseller.php');
            $this->load->view('Daftar_Produk/Edit_Daftar_Produk.php', $data);
            $this->load->view('Template/Back_End/Footer_Reseller.php');
        }

        //proses edit daftar produk
        public function ProsesEditDaftarProduk($ID){
            $data = $this->Produk->Detail_Daftar_Produk($ID)->row();
            $Category=$data->IRCategory;

            //proses perubahan tipe harga (menghilangkan tanda titik)
            $harga=$this->input->post("harga");
            $y= strlen($harga);
            $arr1 = str_split($harga);
            $hargafix="";
            for($x=0;$x<$y;$x++){
                if($arr1[$x]!="."){
                    $hargafix = $hargafix.$arr1[$x];
                }
            }

            //data product yang ditambahkan
            $daftar_produk = array( 
                "ProductName" => $this->input->post("nama"), 
                "IRCategory" => $Category,
                "Stock" => $this->input->post("stok"),
                "Price" => $hargafix
            );

            //validasi untuk harga
            if($hargafix < 200000){
                if($hargafix > 100000){
                    if($this->Produk->Edit_Daftar_Produk($ID,$daftar_produk)){
                        redirect(site_url("Daftar_Produk"));
                    }else{
                        redirect(site_url("Daftar_Produk/EditDaftarProduk/".$ID));
                    }
                }else{
                    $validasi = "Harga harus diantara Rp. 100.000 sampai Rp. 200.000";
                    $this->session->set_flashdata("error",$validasi);
                    redirect(site_url("Daftar_Produk/EditDaftarProduk/".$ID));
                }
            }else{
                $validasi = "Harga harus diantara Rp. 100.000 sampai Rp. 200.000";
                $this->session->set_flashdata("error",$validasi);
                redirect(site_url("Daftar_Produk/EditDaftarProduk/".$ID));
            }
        }

        //proses hapus daftar product
        public function ProsesHapusDaftarProduk($ID){ 
            $this->Produk->Hapus_Daftar_Produk($ID); 
            redirect(site_url("Daftar_Produk")); 
        }

    }
?>
