<?php
class Login extends CI_Controller{
    public function __construct(){ 
        parent::__construct();
        $this->load->model("Akun","",TRUE);
    }

    public function index(){
        if($this->session->userdata('logins')){
            $ID=$_SESSION["id"];
            $arr1 = str_split($ID);
            $Hak_Akses = $arr1[0];
            if($Hak_Akses == 'A'){
                redirect(site_url("Back_Admin"));
            }elseif($Hak_Akses == 'R'){
                redirect(site_url("Back_Reseller"));
            }else{
                redirect(site_url("/Front"));
            }
        }else{
            $this->load->view("Login");
        }
    }

    function Daftar(){
        $this->load->view("Register");
    }
    
    #region Proses
    function prosesLogin(){
        $this->load->model("Akun");
        if($this->Akun->Admin()->num_rows() > 0){
            $pengguna = $this->Akun->Admin()->row_array();
            $session_data = array(
                "logins" => true,
                "username" => $pengguna['Username'],
                "name" =>  $pengguna['Name'],
                "id" =>  $pengguna['IDAdmin']
            );       
            $this->session->set_userdata($session_data); 
            redirect(site_url("/Front"));
        } else if ($this->Akun->Reseller()->num_rows() > 0){
            $pengguna = $this->Akun->Reseller()->row_array();
            $session_data = array(
                "logins" => true,
                "username" => $pengguna['Username'],
                "name" =>  $pengguna['Name'],
                "id" =>  $pengguna['IDReseller'],
                "bantu" => true
            );       
            $this->session->set_userdata($session_data); 
            redirect(site_url("/Front"));
        }else if ($this->Akun->Member()->num_rows() > 0){
            $pengguna = $this->Akun->Member()->row_array();
            $session_data = array(
                "logins" => true,
                "username" => $pengguna['Username'],
                "name" =>  $pengguna['Name'],
                "id" =>  $pengguna['IDMember'],
                "bantu" => true
            );       
            $this->session->set_userdata($session_data); 
            redirect(site_url("/Front"));
        }else{
            $this->session->set_flashdata("error","Username atau Password Salah");
            redirect(site_url("login"));
        }
    }

    function prosesDaftar(){       
        $data = $this->Akun->tampil_data_member()->last_row();
        $ID = $data->IDMember;
        $arr1 = str_split($ID);
        $length = count($arr1);
        if($arr1[3] == 9){
            $arr1[3] = 0;
            $arr1[2] = $arr1[2]+1;
        }
        else{
            $arr1[3] = $arr1[3]+1;
        }

        if($arr1[2] == 9){
            $arr1[3] = 0;
            $arr1[2] = 0;
            $arr1[1]+1;
        }
        
        $No_Awal = 0;
        $id = "";
        for($No_Awal = 0; $No_Awal < $length; $No_Awal++) {
            $id = $id.$arr1[$No_Awal];
        }
        if($this->Akun->insertmember($id)){
            redirect(site_url("Login"));
        }else{
            redirect(site_url("Login/daftar"));
        }
    }
    #endregion
    
    function logout(){
        $this->session->sess_destroy();
        redirect(site_url("Login"));
    }

}
?>